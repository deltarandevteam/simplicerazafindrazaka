<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<body>
<div class="filazana">
<table width="900" border="0" style="font-size:14px">
  <tr>
    <td><p><br>
</p>
      <div align="center">
        <div>
          <table border="0" width="900">
            <tbody>
              <tr>
                <td>While reading this page, you will hear the famous   Protestant song "Esory re, ry Raiko". You will know the reason for this   further down <strong>.<br>
                    <br>
                </strong>First  of all, just a short lexicon to make your reading easier<br>
                <br>
Atoa = short for Andriamatoa = Mr or Sir.<br>
Rtoa = short for Ramatoa = Mrs or Ms or Lady.<br>
Pejy teo aloha = previous page.<br>
Pejy manaraka = next page.<br>
Tetiarana = family tree.<br>
Taranaka = descendant(s), generation.<br>
Zafikely = grand-children.<br>
Zafiafy = great-grand-children.<br>
Zanak'amanjafy = the descendants or the grand-children and  great-grand-children.<br>
Mpiraitampo = the brothers and sisters.<br>
Taranakin'ny mpiraitampo amin'i ..... = the descendants of the brothers and  sisters of .....<br>
Nodimandry = deceased.<br>
Tsy nanambady = unmarried.<br>
Tsy niteraka / tsy nanan-taranaka = no descendants.<br>
Vady faharoa = spouse of second marriage.<br>
Raha fintinina ou rqha fehezina = briefly, in a nutshell, to sum it up.<strong><br>
<br>
</strong>This family tree is a hymn in memory of our ancestors and other   deceased relatives. It will help us to remember their names better. We   will thus be able to convey them to the future generations. It will also   help us to know the existence of our cousins, nephews, nieces, uncles,   aunts, grandfathers and grandmothers, all over the world. Uniting and   fraternizing, such is also the noble purpose of this family tree.<br>
<rr>
In   the past, Malagasy parents did not systematically give their children   the father's patronymic name nor that of the mother. Within the same   family, every child could have different patronymic names. This may   often sound odd and confusing to Westerners and often leaves them   perplexed and disconcerted, but this custom also had the noble purpose   of perpetuating the memory of a late relative. Further explanations on   this question can be found in the most interesting article "Why are   Malagasy names so long ?" <a href="http://www.slateafrique.com/829/pourquoi-noms-malgaches-si-longs" target="_blank">http://www.slateafrique.com/829/pourquoi-noms-malgaches-si-longs</a>.<br>
<br>
For coherence's sake, we have exceptionally restored the original long   form of some expatriates' shortened or westernized names.<br>
<br>
Data additions can be made at any time here. Those of you, whose   names are not appearing here, can always join the big family, which can   only delight everyone concerned, because it would be a nice token of   brotherhood, togetherness and solidarity.<br>
<br>
Even if some of you do not believe much in the importance of a   family tree, it is worth remembering that one day your children,   grandchildren, great-grandchildren and great-great-grandchildren may   feel the need to know their distant origins. Knowing that we relong to a   particular family, discovering or rediscovering our roots and hang to   them is absolutely necessary to the balance, among other things, of   every human being. Please think for a moment of abandoned children or of   those born anonymously, to understand their suffering and despair, when   they start searching for their origins.<br>
<br>
The setting up of this site has been a time-consuming and arduous   task. One relative initially did his bit and brought a stone to the   pile, but given the complexity and the huge weight of the task, it was   necessary to appeal to further helping hands. Thus, we were able to   benefit from the expertise and know-how of a computer   engineer.<br>
Our deepest gratitude goes to all of them.<br>
<br>
Without the generous and patient contribution of some members of our big family, we would not have had a   number of pictures and information of our deceased loved ones. They managed to dig out   the yellowish, old pictures left by our ancestors. A big wholehearted   thank-you to all of them as well !<br>
Our forefather,   RAZAFINDRAZAKA, also known as" Simplice RAZAFINDRAZAKA " had a   reputation as a composer. His most famous composition is the Protestant   song " Esory re ry Raiko ", interpreted her by Eric MANANA. His own grandson, Justin RAZAKA, later made   the musical arrangement of the song. Justin RAZAKA was one of the first   Malagasy academicians or even the very first Malagasy academician under   the French colony. He used to be one of the contributors to Reverend   Father Malzac's French-Malagasy Dictionary. Justin RAZAKA was born on   March 11th, 1890 and died in 1976 at the age of 86.<br>
RAZAFINDRAZAKA is   the son of ANDRIATSARAFARA, brother of Ms RAFARAVAVY, also known as   "RASALAMA MARTIORA". ANDRIATSARAFARA was the one who had our Manjakaray   ancestral vault built on May 11th, 1837.<br>
Please see a picture of the family vault here <a href="http://www.ronsard-andriamanana.globalvision.mg/fr/photos-de-nos-chers-parents-disparus/galerie/11-lieux-dinhumation/detail/76-tombeauclaricerafarasoamanjakaray.html" target="_blank">http://www.ronsard-andriamanana.globalvision.mg/fr/photos-de-nos-chers-parents-disparus/galerie/11-lieux-dinhumation/detail/76-tombeauclaricerafarasoamanjakaray.html</q><br>
<br>
Here are some further information about Simplice RAZAFINDRAZAKA:<br>
<br>
Madagascar: AMAA:  Several festivities to celebrate AMAA's 75th birthday. <br>
Source: Midi Madagasikara, by Patrice RABE, February 15th, 2011. <a href="http://fr.allafrica.com/stories/201102151338.html" target="_blank">http://fr.allafrica.com/stories/201102151338.html</a><br>
AMAA   is one of those big liturgical groups which have built and still build   the fame of the Malagasy choral singing. It is celebrating its jubilee   this year 2011 with great pomp and ceremony. The Avaratr' Andohalo FJKM   Church has the great figures of the Malagasy Protestantism in its bosom.   Those famous figures used to co~tribute to the huge success and   popularity of their parish, by composing works which have become famed   hymns. They are still sung on a regular basis.<br>
Their names are   Rabengodona Andrianaly, Simplice Razafindrazaka, Tselatra Rajaonah,   Rasamy Gasy, Dr Ratovondrahety, Samuel Randrianandraina (Dada Samy), and   so on. They all used to be part of AMAA, the Church choir. Today, this   liturgical group is still one of the most famed groups of the capital   city.<br>
---------------------------<br>
Apart from "Esory re ry Raiko" , here is a brief sample of Simplice Razafindrazaka's songs:<br>
<br>
" Raozy mavokely "   <br>
"Ny fitondran' Andriamanitra", " Ny fiainana manaitra ", " Ny fialonana " and more <a href="http://www.avmm.org/disco/78.html" target="_blank">http://www.avmm.org/disco/78.html</a> <br>
 
<br>
Also let us add that Justin RAZAKA was the composer of some other songs, among which was "Rahoviana no ho paradisa" <a href="http://www.avmm.org/disco/78.htm" target="_blank">http://www.avmm.org/disco/78.htm</a></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
	<td>&nbsp;</td>    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td></td>
    <td>&nbsp;</td>
  </tr>
</table>
</div>


</body>
