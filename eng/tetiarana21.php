<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<body>
<div class="filazana2" align="left">
<table width="900" border="0" style="font-size:14px;">
  <tr>
    <td>&nbsp;</td>
    <td><p class="style9">&nbsp;</p>
      <p><strong><u>Jaona  RAKOTOVAO (02.07.11/05.02.95) and Jeannelle RAZAIARISOA (26.11.14/07.11.93) had 8 children:</u></strong> </p>
      <br>
      <strong>Michel Jaona  Rakotovao, Lala Monique Ranivoarisoa,   Liliane Holisoa Ravaoarimanga, L&eacute;a Nivo  Rafaratiana, Lydie Ginette   Razaiarisoa, Liva Jeannelle Rasoazanany, Mbolatiana  Rakotovao.</strong> <br>
&nbsp;<br>
<p><strong><u>1. Michel  Jaona Rakotovao and Gis&egrave;le Gilberte Razafimahefa ===&gt; 2 children:</u></strong> </p>
<br>
<strong>Sylvie  Holisoa Rakotovao, Patrick Jaona Rakotovao.</strong><br>
<strong><br>
</strong> <br>
<u><strong>Sylvie  Holisoa Rakotovao and Zefania Randrianarison ===&gt; 1 child:</strong> </u><br>
<strong>Harena Holisoa Rakotovao.</strong><br>
<strong><br>
</strong> <br>
<u><strong>Patrick Jaona Rakotovao ===&gt; 2 children:</strong> </u><br>
<strong>Fitia Olisoa Rakotovao, Aina Rakotovao.</strong> <br>
&nbsp;<br>
<p><strong><u>2. Lala Monique Ranivoarisoa and Seth Razafimpanilo ===&gt; 4 children:</u></strong> </p>
<br>
<strong>Harilala Elia Razafimpanilo, Haritiana Razafimpanilo, Harinivo  Razafimpanilo, Njakaharivony Tantely Razafimpanilo.</strong><br>
<strong><br>
</strong> <br>
<u><strong>Harilala Elia Razafimpanilo ===&gt; 1 child:</strong> </u><br>
<strong>Tantely Harimirana Andrianaly.</strong><br>
<strong><br>
</strong> <br>
<u><strong>Haritiana Razafimpanilo ===&gt; 2 children:</strong> </u><br>
<strong>Miarasoa Micha</strong><strong>&euml;</strong><strong>lla Razafimahefa, Mialitiana Razafimahefa.</strong><br>
<strong><br>
</strong> <br>
<u><strong>Miarasoa Micha</strong><strong>&euml;</strong><strong>lla Razafimahefa married Zo Rakotoarisoa.</strong></u><br>
<u><strong><br>
</strong></u> <br>
<strong><u>Harinivo Razafimpanilo and Haja Ravoajanahary ===&gt; 2 children</u>:</strong> <br>
<strong>Mahalia Ravoajanahary, Andrianina Niloniaina Ravoajanahary.</strong> <br>
&nbsp;<br>
<p><strong><u>3.Liliane Holisoa Ravaoarimanga and Raymond  Rabetsimialona(24.02.32/01.07.2008) ===&gt; 4 children:</u></strong> </p>
<br>
<strong>Jacky Andriantsialonina, Jaona Christian   Andriantsialonina, Olivia  Holisoanirina Andriantsialonina, Lysiane   Fanjasoa Andriantsialonina.</strong><br>
<strong><br>
</strong> <br>
<u><strong>Jacky Andriantsialonina and Hantanirina Andriamanantsoa ===&gt; 2 children:</strong> </u><br>
<strong>Nantenaina Eric Andriantsialonina, &nbsp;Alisoa Elodie Andriantsialonina.</strong><br>
<strong><br>
</strong> <br>
<u><strong>Jaona Christian Andriantsialonina (11.12.1951 / 07.07.1997).</strong></u><br>
<strong><br>
</strong> <br>
<strong><u>Olivia Holisoanirina Andriantsialonina </u>===&gt;</strong> <br>
<strong>Nantenaina Tiana Andriantsialonina.</strong><br>
<strong><br>
</strong> <br>
<u><strong>Lysiane Fanjasoa Andriantsialonina and Francis Rajaofetra ===&gt; 3 children:</strong></u>&nbsp;<br>
<br>
<strong>Liantsoa  Franco Rajaofetra, Nomentsoa Fiankana Rajaofetra, Miotisoa Rajaofetra.</strong> <br>
&nbsp;<br>
<p><strong><u>4. Lala Nivo Rafaratiana (12.11.42/19.11.79) and Rodolphe Razafindratovo  (13.10.34/...2001)</u></strong> <br>
    <strong><u>had 6 children:</u></strong> </p>
<br>
<strong>Bruno Jaona Razafindratovo, Jos</strong><strong>&eacute;</strong><strong> Harizo Razafindratovo, Yves  Michel Razafindratovo, Didier   Razafindratovo, Lanto Yvette Razafindratovo,  Lalaina Josiane   Razafindratovo.</strong><br>
<strong><br>
</strong> <br>
<u><strong>Bruno Razafindratovo: No descendants.</strong></u><br>
<strong><br>
</strong> <br>
<u><strong>Jos&eacute; Harizo Razafindratovo and Harilalasoa Ramahery ===&gt; 1 child:</strong></u> <br>
<strong>Andrianina Cedrick Ratovoherisoa.</strong><br>
<strong><br>
</strong> <br>
<u><strong>Yves Michel Razafindratovo and Zo</strong><strong>&eacute;</strong><strong> Myriame Ravololona ===&gt; 3 children:</strong> </u><br>
<strong>Aina Nampoina, Aina Domoina, Aina Nomena.</strong><br>
<strong><br>
</strong> <br>
<strong><u>Didier Razafindratovo and Vololomalala Rajaonarivelo ===&gt; 2 children</u>:</strong><br>
<strong>Tojosoa Fandresena Razafindratovo, Fitahiana Razafindratovo.</strong><br>
<strong><br>
</strong> <br>
<u><strong>Lanto Yvette Razafindratovo ===&gt; 1 child:</strong> </u><br>
<strong>Harena Randrianjatovo.</strong><br>
<strong><br>
</strong> <br>
<strong><u>Lalaina Razafindratovo and Rija Andrianina Rakotondrazaka ===&gt; 1 child</u>:</strong> <br>
<strong>Mitia Andrianirina.</strong> <br>
&nbsp;<br>
<p><strong>5. Lydia Ginette Razaiarisoa and Claude Ramanantsalama (19.04.1939 /  02.11.2002) ===&gt; 3 children:</strong> </p>
<br>
<strong>Hanta Ramanantsalama, Harinirina Ramanantsalama, Herinirina Ramanantsalama.</strong><br>
<strong><br>
</strong> <br>
<u><strong>Hanta Ramanantsalama and Herv</strong><strong>&eacute;</strong></u><strong><u> Razafindramiarana (11.08.1959  / 12.01.2010) ===&gt; 3 children</u>:</strong> <br>
<strong>Irina Aina Razafindramiarana, Niry Aina Razafindramiarana, Lanto  Razafindramiarana.</strong><br>
<strong><br>
</strong> <br>
<strong><u>Harinirina  Ramanantsalama and Dina Ranaivoson (01.09.1972 / 23.07.2005) ===&gt; 1 child</u>:&nbsp;</strong> <br>
<strong>Miora  Ramanantsalama.</strong><br>
<strong><br>
</strong> <br>
<strong><u>Herinirina  Ramanantsalama and Clara Razafindrakoto===&gt; 2 children</u>:</strong> <br>
<strong>Maeva  Ramanantsalama, Aro Salomon Ramanantsalama.</strong> <br>
&nbsp;<br>
<p><strong><u>6. Liva  Jeannelle Rasoazanany (20.12.1946 / 08.09.2011) and Nirina Andrianarijaona ===&gt; 3 children:</u></strong> </p>
<br>
<strong>Tiana Andrianarijaona, Nirina Andrianarijaona, Andy Andrianarijaona.</strong><br>
<strong><br>
</strong> <br>
<u><strong>Tiana Andrianarijaona ===&gt; 2 children:</strong> </u><br>
<strong>Menja Rajoelisolo, Vonondahy Rajoelisolo.</strong><br>
<strong><br>
</strong> <br>
<u><strong>Nirina Andrianarijaona and Miora Tiana Rajoelisolo ===&gt; 2 children:</strong> </u><br>
<strong>Mialy Fitia Andrianarijaona, Philippe Mandresy Andrianarijaona.</strong><br>
<strong><br>
</strong> <br>
<u><strong>Andy Andrianarijaona: No descendants.</strong></u> <br>
&nbsp;<br>
<p><strong><u>7. Mbolatiana Rakotovao and Olivier Duval ===&gt; 1 child:</u></strong> </p>
<br>
<strong>Tefy  Rakotovao Duval.</strong><p class="style9">&nbsp;</p>
      </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#784f3b"><a href="page20.php">Previous page</a></td>
    <td>&nbsp;</td>
	<td bgcolor="#784f3b"><a href="page22.php">Next page</a></td>   
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td>&nbsp;</td>
  </tr>
</table>
</div>


</body>
