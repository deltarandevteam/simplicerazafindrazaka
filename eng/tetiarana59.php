<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
<!--
p.MsoNormal {
margin-top:0cm;
margin-right:0cm;
margin-bottom:10.0pt;
margin-left:0cm;
line-height:115%;
font-size:11.0pt;
font-family:"Calibri",sans-serif;
}
-->
</style>
<body>
<div class="filazana2" style="text-align:center">

<div style="display:table; background-color:#FFFFFF; width:100%; font-size:14px;">
<div style="width:100%; display:table; text-align:center;">
<div style="width:19%; display:table; float:left;margin:5px; height:180px;"><img src="photos/page45_clip_image002.jpg" width="233" height="202" style="margin-bottom:15px;"><br/>
</div>
<div style="width:19%; display:table; float:left;margin:5px;height:180px;"><img src="photos/page45_clip_image004.jpg" width="139" height="199" style="margin-bottom:15px;"><br/>
</div>
<div style="width:18%; display:table; float:left;margin:5px; height:180px;"><img src="photos/page45_clip_image006.jpg" width="174" height="214" style="margin-bottom:15px;"><br/>
</div>
<div style="width:18%; display:table; float:left;margin:2px; height:180px;"><img src="photos/page45_clip_image009.jpg" width="165" height="218" style="margin-bottom:15px;"></div>
<div style="width:18%; display:table; float:left;margin:5px; height:180px;"><img src="photos/page45_clip_image010.jpg" width="163" height="210" style="margin-bottom:15px;"><br/>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div style="width:100%; display:table; float:left; margin:0px; border:1px solid #000000; padding:10px;"><p class="MsoNormal" align="center" style="text-align: center; font-style: normal; font-weight: bold; font-size: 14pt;">Rodin (Joujou)</p>
</div>
<div style="width:100%; display:table;">

<div style="width:42%; display:table; float:left; margin:20px; border:1px solid #000000; padding:10px;">
  <p><strong>Rehefa maty aho </strong></p> <br>
  <p>Raha ho avy, Ralala, ny taona  hodiako <br>
    Ao ambanin'ny tany  tsindrian-drangolahy, <br>
    Ka ny vato no solo kidoro  hatoriako <br>
    Izao re ny zavatra ataonao ho  ahy, <br>
    Voleo voninkazo eo ambonin'ny  fasako, <br>
    Fotsy sy mavo ary manga sy  mena, <br>
    Aleo haniry eo  mandram-pifohako, <br>
    Mba hanofisako ilay fanekena. <br>
    Ny fotsy fahadiovana,<br>
    Misy aminao tsy mba azo  lotoina, <br>
    Toy ny orim-bato izay  nifanaovana, <br>
    Soratra velona tsy azo  vonoina,<br>
    Ny endriny mavo, ny fofony  manitra, <br>
    Dia fivavahana ataoko any an-danitra, <br>
    Mba hitsinjovany anao tsy ho  irery, <br>
    Omb&agrave;ny raha sendra tsy misy  mpijery, <br>
    Dia ny manga no marikin'ny  fihafiana <br>
    Niaraha-niharitra ny  zava-mangidy <br>
    Dia ny andro namoizana ilay  fifaliana <br>
    Niaraha-niaritra zava-tsy  fidiny, <br/>
    Ny mena ho taratrin'ny afo tsy maty <br/>
    Mirehitra lava ao anatin' ny foko, <br/>
    io ilay fitia, fitia tena anaty, <br/>
    Fitia voakas&eacute; sy voatomboka loko. <br/>
    Ny fasako ataovy eo andrefan-tan&agrave;na <br/>
    Ka ataovy mikasika ny ilan-kamory, <br/>
    Ka soraty eo ny tenin'ny fanantenana: <br/>
    "Tsy maty aho akory fa mbola matory". <br/>
    Toy ny moron'ny rano voleo volotara <br/>
    Ka jerijereo aza avela ho maina, <br/>
    Tarafo eo aho ho tsinjinao tsara,<br/>
    Fa tsy miala eo aho raha tsy efa maraina. <br/>
    Na dia ny maty aza ny tenako be, <br/>
    Ka hiraraka an-tany ny r&agrave; ao am-poko, <br/>
    Ny fitiavako anao ka mimpetraka anie,<br/>
    Tsy mba ho maty ary tsy mba ho lo.<br/><br>
</p>
</div>


<div style="width:45%; display:table; float:left; margin:20px; border:1px solid #000000; padding:10px;">
  <p><strong>Lorsque sonnera l'heure de ma mort</strong></p><br>
  <p>Quand viendra, Ralala, le jour o&ugrave; je m&prime;en irai <br>
    sous terre, parmi les pierres, <br>
    Et que la pierre sera le matelas de mes sommeils,<br>
    Voici ce que je voudrais bien que tu fasses pour moi,<br>
    Plante des fleurs sur ma tombe,<br>
    Blanches et jaunes et bleues et rouges,<br>
    Laisse-les pousser l&agrave; jusqu&prime;&agrave; mon r&eacute;veil,<br>
    Pour que je puisse r&ecirc;ver &agrave; notre pacte,<br>
    Le blanc, incarnant l'immacul&eacute; en toi,<br>
    Souill&eacute;, ne doit pas l'&ecirc;tre,<br>
    Tel la st&egrave;le que nous avons &eacute;rig&eacute;e ensemble,<br>
    C'est une &eacute;criture vivante qui ne doit pas &ecirc;tre effac&eacute;e,<br>
    Son aspect jaune, sa fragrance exquise,<br>
    Une pri&egrave;re que je ferai l&agrave;-haut,<br>
    Pour veiller &agrave; ce que tu ne sois pas seul(e),<br>
    Que tu sois prot&eacute;g&eacute;(e), si personne ne veille sur toi,<br>
    Le bleu comme marque de nos privations,<br>
    Quand nous endurions ensemble l&prime;&acirc;cre et l'amer,<br>
    Le jour o&ugrave; nous ne chantions plus la joie,<br>
    Endurant des choses ensemble contre notre gr&eacute;,<br>
    Le rouge comme marque du feu non &eacute;teint,<br>
    Toujours allum&eacute; dans mon coeur,<br>
    C'est l'amour, l'amour du fond de mon &ecirc;tre,<br>
    Amour bien cachet&eacute; et estampill&eacute; en couleur,<br>
    Mon tombeau, b&acirc;tis-le &agrave; l'ouest du village,<br>
    Fais-le, le long du petit lac,<br>
    Inscris dessus les mots de l'espoir<br>
    &quot;Je ne suis pas mort, je dors encore&quot;.<br>
    Comme au bord de l'eau, fais-y pousser des roseaux,<br>
    Prends-en soin, ne les laisse pas se d&eacute;ss&eacute;cher,<br>
    Cherches-y mon reflet, pour que tu voies bien,<br>
    Que je ne m'en irai de l&agrave; que lorsqu'il fera jour,<br>
    M&ecirc;me s'il est mort ce vieux corps qu'est le mien,<br>
    Et que, par terre, se versera le sang de mon coeur,<br>
    Mon amour pour toi, il est bien l&agrave;,<br>
    il ne sera ni mort ni d&eacute;p&eacute;ri.</p>
  <p>Traduction libre de Rodin (Joujou).<br>
  </p>
</div>

</div>
</div>

<table width="900" border="0" style="font-size:14px;">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#784f3b"><a href="page58.php" style="color:#FFF; text-decoration:underline; height:30px; line-height:30px;">Page pr&eacute;c&eacute;dente</a></td>
    <td>&nbsp;</td>
	<td bgcolor="#784f3b"><a href="page1.php" style="color:#FFF; text-decoration:underline; height:30px; line-height:30px;">Premi&egrave;re page</a>--></td>   
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
  </tr>
</table>
</div>


</body>
