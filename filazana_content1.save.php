<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<body>
<div class="filazana">
<table width="900" border="0" style="font-size:14px">
  <tr>
    <td><p><br>
</p>
      <div align="center">
        <div>
          <table border="0" width="900">
            <tbody>
              <tr>
                <td>Eo am-pamakiana an'ity pejy ity ianareo, dia ahaheno ilay   hira protestanta  malaza "Esory re, ry Raiko". Ho fantatrareo ery ambany   ny antony. <br>
                  <br>
                  * Ity tetiarana ity dia karazana antema na faneva ho    fahatsiarovana ireo razam-bentsika sy ireo havantsika efa nodimandry ary    hanampy antsika koa hahatsiaro ny anaran'izy ireo, mba hahafahantsika   mampita  an'izany amin'ireo taranaka ho avy. Hanampy antsika koa izy   hahafantarantsika  ny fisian'ireo  zanaky ny mpiray tampo amin'ny ray   aman-drenintsika ary ny  raibe sy ny renibentsika, izay miely eran'izao   tontolo izao. Hampiray sy  hampifankatia : izay koa no tanjona mihaja   kendren'ity tetiarana ity. <br>
                  <br>
                  Mety foana, raha tianareo, ny manampy anarana na  tari-dalana   samihafa eto amin&prime;ity tetiarana ity. Ny sasany aminareo izay tsy  naniry   ny anoratana ny anarany eto, dia afaka miray mandrakariva amin&prime;ny    fianakaviam-be. Izany zavatra izany dia ho fifaliana tokoa ho an'ny   havana  rehetra, satria ho fihetsika kanto izay mampiray sy maneho   fifankatiavana sy  firahalahiana izany. <br>
                  <br>
                  Ny sasany aminareo izay tsy mino loatra fa zava-dehibe  ny   tetiarana, dia tokony hahatsiaro fa, indray andro any, ny zanakareo, ny    zafi-kelinareo na ny zafiafinareo dia mety mila hahafantatra ny   fiavian&prime;izy  ireo. Ny fahafantarana hoe avy amin&prime;ny fianakavian-dRanona   sy Ranona isika, ny  fahazoana mahita ny fiaviana tsy fantatra na very   tadidy dia zava-dehibe tokoa  ho any ny filaminan-tsaina ho antsika   olombelona. Mba jereo kely fotsiny ny  manjo ny zaza narian-dreniny mba   hahafahanareo hahatsapa tsara ny fahoriany sy  ny   fahadisoam-panantenan&prime;izy ireo, rehefa manomboka mitady ny fiaviany izy    ireo. <br>
                  <br>
                  Asa goavam-be sy lava tokoa ny fanaovana ity tetiarana  ity. Efa   nisy havana sy namana vitsivitsy ihany izay nanomboka nanome    amim-pahatsorana sy maimaimpoana ny fanampiany teo aloha. Kanefa noho ny    fahasarotana sy ny fahamaroan' ny andraikitra sahanina, dia voatery   niantso  ing&eacute;nieur-informaticien taty aoriana. Atolotra ho azy eto ny   fisaorana sy  fankatelemana avy amin&prime;ny fianakaviana manontolo.  <br>
                  <br>
                  Raha tsy nisy koa ny fanampiana feno fitiavana avy  amin&prime;ny   havana sasany, dia nety tsy nahazo ireo filazana sy sary maromaron&prime;ny    havana izay nodimandry isika. Niara-nitady hatraiza izahay, mba hahita   ireo  taratasy sy sary tena tranainy izay navelan&prime;ny razambentsika.   Fisaorana sy  fankasitrahana tanteraka no hatolotra ho an&prime;ireo havana   rehetra ireo. <br>
                  <br>
                  * Fahiny, dia tsy nahazatra ny ray aman-dreny malagasy  ny   nanome avy hatrany ny zanany ny anaran'ny ray na ny anaran'ny reny. Ao    amin'ny fianakaviana iray dia mety ho nanana anarana samihafa ny ankizy    nateraka.  Izany toe-javatra izany dia somary hafahafa ihany ho an-dry    zareo Tandrefana ary mazana mampirohonahana sy mahalasa saina azy ireo,   kanefa  ny tanjona kanton&prime;io fomba io dia natao mba tsy hahavery tadidy   an&prime;ireo razana  nodimandry. Mety hahitanao fanazavana sy taridalana   bebe kokoa momba io fomba  io ao anatin&prime;ny lahatsoratra tena mahaliana   ity: <strong>"Maninona ny anarana Malagasy no lava toy  izao?" </strong><a href="http://www.slateafrique.com/829/pourquoi-noms-malgaches-si-longs" target="_blank"><strong>http://www.slateafrique.com/829/pourquoi-noms-malgaches-si-longs</strong>. </a><br>
                  <br>
                  * Itkl Razafindrazaka, raibentsika, izay fantatra koa amin'ilay   anarana hoe  "Simplice Razafindrazaka" dia mpamoronkira malaza. Ny hira   malaza indrindra noforony dia ilay hira  protestanta "Esory re ry   Raiko" <!--ary i Justin Razaka-->,Izay hirain'i Eric MANANA etoana.I Justin RAZAKA zafinkeliny, no  nanao ny atao hoe   "arrangement musical" taty aoriana. <br>
                  <br>
                  <br>
                  <br>
                  Itkl Justin RAZAKA dia anisan'ireo akademisiana malagasy voalohany   tamin'ny andron'ny fanjanahatany ary toa hoe izy mihintsy no   akademisiana malagasy voalohany indrindra tamin'izany fotoana izany.   Teraka tamin'ny 11 martsa 1890 Itompokolahy ary nodimandry tamin'ny   toana 1976.<br>
                  Simplice RAZAFINDRAZAKA dia zanak'i ANDRIATSARAFARA,   anadahin'Itkv RAFARAVAVY RASALAMA atao hoe "RASALAMA MARTIORA". Itkl   ANDRIATSARAFARA no namorona ny fasandrazatsika ao Manjakaray tamin'ny   11 mai 1837. <br>
                  Tsindrio ity lien ity <a href="http://www.ronsard-andriamanana.globalvision.mg/fr/photos-de-nos-chers-parents-disparus/galerie/11-lieux-dinhumation/detail/76-tombeauclaricerafarasoamanjakaray.html" target="_blank">http://www.ronsard-andriamanana.globalvision.mg/fr/photos-de-nos-chers-parents-disparus/galerie/11-lieux-dinhumation/detail/76-tombeauclaricerafarasoamanjakaray.html</a>  mba afahanareo mijery an'io fasana io.<br>
                  <br>
                  <br>
                  Ireto misy filazana samy  hafa   miresaka an'Itkl Simplice Razafindrazaka.  <br>
                  <br>
                  <br>
                  Madagascar: AMAA - Plusieurs manifestations pour la c&eacute;l&eacute;bration   de son 75e  anniversaire. Source: Midi Madagasikara, Par Patrice RABE,   15 F&eacute;vrier  2011. <a href="http://fr.allafrica.com/stories/201102151338.html" target="_blank">http://fr.allafrica.com/stories/201102151338.html</a> <br>
                  <em>C'est l'une de ces  grandes formations liturgiques qui font le   renom du chant choral malgache. Elle  c&eacute;l&egrave;bre cette ann&eacute;e son jubil&eacute;   avec &eacute;clat. Le temple FJKM Avaratr'Andohalo a en  son sein de grandes   figures du protestantisme malgache. Ces illustres  personnages ont   particip&eacute; au rayonnement de leur paroisse, en composant des  oeuvres qui   sont devenues des cantiques interpr&eacute;t&eacute;s r&eacute;guli&egrave;rement.</em> <br>
                  Ils s'appellent Rabengodona Andrianaly, Simplice  Razafindrazaka,   Rajaonah Tselatra, Rasamy Gasy, Dr Ratovondrahety,  Randrianandraina   Samuel (Dada Samy), etc. Ils faisaient tous partie de l'AMAA,  la   chorale du temple et aujourd'hui, cet ensemble liturgique est une des    formations les plus connues de la capitale. <br>
                  Ankoatra "Esory re ry Raiko" dia ireto ny sasany amin'ireo hira  noforon'i Simplice Razafindrazaka : <br>
                  "Esory re ry Raiko"  <br>
                  "Raozy mavokely"  <br>
                  "Ny fitondran'Andriamanitra", "Ny  fiainana manaitra", "Ny fialonana" sy ny sisa sy ny sisa <a href="http://www.avmm.org/disco/78.html" target="_blank">http://www.avmm.org/disco/78.html</a> <br>
                  <br>
                  Tsara ho marihana eto koa fa i Justin Razaka kosa no  namorona ny tonony sy ny feon'ilay hira "Rahoviana no ho  paradisa" <a href="http://www.avmm.org/disco/78.html" target="_blank">http://www.avmm.org/disco/78.html</a> </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
	<td>&nbsp;</td>    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td></td>
    <td>&nbsp;</td>
  </tr>
</table>
</div>


</body>
