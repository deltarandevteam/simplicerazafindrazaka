<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<body>
<div class="filazana">
<table width="900" border="0" style="font-size:14px">
  <tr>
    <td><p><br>
</p>
      <div align="center">
        <div>
          <table border="0" width="900">
            <tbody>
              <tr>
                <td><p>Tout en lisant cette page, vous entendrez le c&eacute;l&egrave;bre cantique protestant <strong>"Esory re, ry Raiko"</strong>. Vous en saurez les raisons, plus bas.<br>
                    <br>
                    Tout d'abord, juste un bref lexique pour faciliter votre  lecture:<br>
                    <br>
Atoa = abr&eacute;viation de Andriamatoa = Monsieur.<br>
Rtoa = abr&eacute;viation de Ramatoa = Madame.<br>
Page pr&eacute;c&eacute;dente = pejy teo aloha.<br>
 Page suivante = pejy manaraka.<br>
Tetiarana = arbre g&eacute;n&eacute;alogique.<br>
Taranaka = descendant(s), g&eacute;n&eacute;ration.<br>
Zafikely = petits-enfants.<br>
Zafiafy = arri&egrave;re-petits-enfants.<br>
Zanak'amanjafy = les descendants ou les petits-enfants et arri&egrave;re-petits-enfants.<br>
Mpiraitampo = les fr&egrave;res et soeurs.<br>
Taranakin'ny mpiraitampo amin'i ..... = les descendants des fr&egrave;res et soeurs de  .....<br>
Nodimandry = d&eacute;c&eacute;d&eacute;(e).<br>
Tsy nanambady = c&eacute;libataire.<br>
Tsy niteraka / tsy nanan-taranaka = pas de descendants.<br>
Vady faharoa = &eacute;poux ou &eacute;pouse en secondes noces.<br>
Raha fintinina ou raha fehezina = en bref, en deux mots, pour r&eacute;sumer.<br>
<br>
Cet arbre g&eacute;n&eacute;alogique est un hymne &agrave; la m&eacute;moire de nos a&icirc;eux et   autres parents d&eacute;funts. Il nous aidera &agrave; mieux nous rappeler leurs noms,   pour pouvoir les transmettre aux futures g&eacute;n&eacute;rations. Il nous aidera   aussi &agrave; conna&icirc;tre l'existence de nos cousins, cousines, neveux, ni&egrave;ces,   oncles, tantes, grands-p&egrave;res et grands-m&egrave;res, dispers&eacute;s aux quatre coins   du monde. R&eacute;unir et fraterniser, tel est &eacute;galement le noble but de cet   arbre g&eacute;n&eacute;alogique.<br>
<br>
Autrefois, les parents malagasy ne donnaient pas syst&eacute;matiquement &agrave;   leurs enfants le nom patronymique du p&egrave;re ni celui de la m&egrave;re. Au sein   d'une m&ecirc;me famille, chaque enfant pouvait avoir des noms patronymiques   diff&eacute;rents. Ceci d&eacute;concerte souvent les Occidentaux et les laisse   r&ecirc;veurs et perplexes, mais cette coutume avait pour but, entre autres,   de perp&eacute;tuer la m&eacute;moire d'un parent d&eacute;funt. Plus d'explications et   d'informations sur cette question, dans l'article fort int&eacute;ressant <strong>"Pourquoi les noms malgaches sont-ils si longs?" </strong><a href="http://www.slateafrique.com/829/pourquoi-noms-malgaches-si-longs" target="_blank">http://www.slateafrique.com/829/pourquoi-noms-malgaches-si-longs</a>.<br>
Dans le but de maintenir une certaine coh&eacute;rence, nous avons   exceptionnellement restitu&eacute; leur forme enti&egrave;re originelle aux noms   raccourcis de certains descendants expatri&eacute;s.<br>
<br>
Des ajouts de donn&eacute;es peuvent, &agrave; tout moment, &ecirc;tre effectu&eacute;s dans ce   site. Ceux d'entre vous, dont les noms n'y figurent pas, peuvent   toujours se joindre &agrave; la grande famille, ce qui ne peut que r&eacute;jouir tout   le monde, car ce serait un beau geste qui vient r&eacute;unir, f&eacute;d&eacute;rer,   fraterniser et solidariser. <br>
M&ecirc;me si certain d'entre vous ne croient pas trop en l'importance   d'un arbre g&eacute;n&eacute;alogique, il est utile de se rappeler qu'un jour, vos   enfants, petits-enfants et arri&egrave;re-petits-enfants pourraient &eacute;prouver le   besoin de conna&icirc;tre leurs origines lointaines. Savoir qu'on appartient &agrave;   telle ou telle famille, pouvoir trouver ou retrouver ses racines et s'y   raccrocher est absolument n&eacute;cessaire pour l'&eacute;quilibre, entre autres, de   tout &ecirc;tre humain. Pensez, un instant, aux enfants abandonn&eacute;s ou n&eacute;s   sous X, pour comprendre leur souffrance et leur d&eacute;sespoir, d&egrave;s lors   qu'ils sont en qu&ecirc;te de leurs origines.<br>
La mise en place de ce site a &eacute;t&eacute; un travail de longue haleine. Un   parent a, dans un premier temps, apport&eacute; b&eacute;n&eacute;volement sa pierre &agrave;   l'&eacute;difice, mais face &agrave; la complexit&eacute; et &agrave; la lourdeur de la t&acirc;che, il a   fallu faire appel &agrave; d'autres bras. A chacun d'entre eux revient   notre plus profonde gratitude.<br>
Sans la g&eacute;n&eacute;reuse et patiente   participation de certains membres de la grande famille, nous n'aurions   pas pu avoir un certain nombre d'informations et de   photos de nos chers parents disparus. Un grand merci du fond du coeur, &agrave;   eux aussi !<br>
<br>
Notre grand a&icirc;eul, RAZAFINDRAZAKA, connu aussi sous son   nom complet "Simplice RAZAFINDRAZAKA" &eacute;tait un illustre compositeur. Il   s'est fait surtout conna&icirc;tre par le c&eacute;l&egrave;bre cantique protestant "Esory   re ry Raiko" interpr&eacute;t&eacute; ici par Fafah du Groupe Mahaleo.Son propre neveu, Justin RAZAKA, en a fait, plus   tard, ce qu'on appelle "l'arrangement musical".<br>
Voici quelques informations sur Simplice RAZAFINDRAZAKA:</p>
                  <table border="0" width="900">
                    <tbody>
                      <tr>
                        <td><br>
                          Madagascar: AMAA - Plusieurs manifestations pour la c&eacute;l&eacute;bration   de son 75e  anniversaire. Source: Midi Madagasikara, Par Patrice RABE,   15 F&eacute;vrier  2011. <a href="http://fr.allafrica.com/stories/201102151338.html" target="_blank">http://fr.allafrica.com/stories/201102151338.html</a> <br>
                          <em>C'est l'une de ces  grandes formations liturgiques qui font le   renom du chant choral malgache. Elle  c&eacute;l&egrave;bre cette ann&eacute;e son jubil&eacute;   avec &eacute;clat. Le temple FJKM Avaratr'Andohalo a en  son sein de grandes   figures du protestantisme malgache. Ces illustres  personnages ont   particip&eacute; au rayonnement de leur paroisse, en composant des  oeuvres qui   sont devenues des cantiques interpr&eacute;t&eacute;s r&eacute;guli&egrave;rement.</em> <br>
                          Ils s'appellent Rabengodona Andrianaly, Simplice  Razafindrazaka,   Rajaonah Tselatra, Rasamy Gasy, Dr Ratovondrahety,  Randrianandraina   Samuel (Dada Samy), etc. Ils faisaient tous partie de l'AMAA,  la   chorale du temple et aujourd'hui, cet ensemble liturgique est une des    formations les plus connues de la capitale. <br>
                          Mis &agrave; part "Esory re ry Raiko" , voici un bref &eacute;chantillon des compositions de Simplice Razafindrazaka : <br>
                          <br>
                          "Raozy mavokely"	<br>
                          "Ny fitondran'Andriamanitra", "Ny  fiainana manaitra", "Ny fialonana" sy ny sisa sy ny sisa <a href="http://www.avmm.org/disco/78.html" target="_blank">http://www.avmm.org/disco/78.html</a> <br>
                          <br>
                          Pr&eacute;cisons &eacute;galement que Justin RAZAKA a &eacute;t&eacute; le   compositeur de quelques autres chansons, parmi lesquelles "Rahoviana no   ho  paradisa" <a href="http://www.avmm.org/disco/78.html" target="_blank">http://www.avmm.org/disco/78.html</a> 
                          <br>
                          <br>
                          <strong>Nos plus vifs remerciements reviennent, entre autres, &agrave; <a href="http://radama.free.fr/desseins_de_la_semaine/?p=7020">M.Claude Razanajao</a> de l'AVMM (Archives Virtuelles de la Musique Malgache) pour ses conseils &eacute;clair&eacute;s et pertinents, &agrave; William Arthur pour  son assistance finale et son expertise informatique, et &agrave; tous ceux qui ont, d'une mani&egrave;re ou d'une autre, apport&eacute; leur pierre &agrave; l'&eacute;difice. </strong>

						  <div style="margin-top:25px;"><?php //include('../liste.php'); ?>
						  <a href="page38.php">Cliquer ici pour voir la liste des chansons et morceaux de musique entendus en fond sonore dans les diff&eacute;rentes pages</a>.</div>
					  </td>
					  </tr>
                    </tbody>
                  </table>
                  </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
	<td>&nbsp;</td>    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td></td>
    <td>&nbsp;</td>
  </tr>
</table>

<!--<div style="text-align: left;font-size: 14px;margin-left: 28px;margin-bottom:12px;"
><h3>Chansons et morceaux de musique entendus dans cet arbre g&eacute;n&eacute;alogique: </h3></br> 
<p>
<b>Page 'Accueil' :</b> " Madagasikara " compos&eacute; par Naly RAKOTOFIRINGA </br>
<b>Page 'La Famille' :</b> " Mama o! " </br>
<b>Page 'Photos' :</b> " Veloma re ry dada havako " </br>
<b>Page 'Notes & Biographie' : </b>" Esory re ry raiko " compos&eacute; par Simplice RAZAFINDRAZAKA, Interpr&eacute;t&eacute;  par Eric MANANA </br>
<b>Page 'Les 7 G&eacute;n&eacute;rations' :</b> " My lanitra mangamanga " compos&eacute; et interpr&eacute;t&eacute; par Ny Antsaly </br>
<b>Page 'Veloma e!' : </b>" Aiza ho aiza re izay dadako izay " interpr&eacute;t&eacute; par Ritsoka</p></div>

</div>-->



</body>
