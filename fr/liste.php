<?php



<p>- Inty ny lisitrin'ny hira sy mozika-lafika malagasy henonareo ao amin'inty tetiarana inty. Ny ankabehazan'ny hira sy mozika eto dia ireo atao hoe "kalon'ny fahiny" sy "hiran'omaly"ary koa ireo izay ao anatin'ny vakodrazana eto anivon'Imerina fa misy ihany koa hira vitsivitsy izay tsy ao anatin'izany.</p>

<p>- Voici la liste des chansons et morceaux de musique malagasy entendus en fond sonore, dans cet arbre g&eacute;n&eacute;alogique. A quelques exceptions pr&egrave;s, il s'agit principalement des vieilles chansons du pass&eacute; bien lointain, appel&eacute;es "kalon'ny fahiny", des m&eacute;lodies de jadis et nagu&egrave;re, les "hiran'omaly"et d'airs folkloriques et populaires des Hauts Plateaux de Madagascar. </p>
 
<p>- Here is the list of the Malagasy background music and songs you may hear in this family tree. With a few exceptions, they are mainly the very old songs of the far-away past, known as "kalon'ny fahiny", the old songs of yesteryear and of not so long ago, aka "hiran'omaly" and the folk and popular tunes of the Highlands of Madagascar.</p>

 

<p>Page Accueil: Madagasikara: Auteur-Compositeur-Interpr&egrave;te: Naly Rakotofiringa.</p>
<p>Page La Famille: Mama , Auteur-Compositeur-Interpr&egrave;te: Ramahafadrahona.<a href=" http://video-streaming.orange.fr/musique/ramahafadrahona-mama-VID0000001rjUP.html">
ramahafadrahona-mama-VID0000001rjUP </a></p>

<p>Page Photos: Veloma re, ry dada havako: Compositeur: Therack, Interpr&egrave;te: Solika. <a href="http://gasy.net/video/clip/22283/veloma-re-ry-dada-havako-solika.html">veloma-re-ry-dada-havako-solika</a></p>
<p>Page Notes diverses et biographie: Esory re, ry Raiko: Auteur-Compositeur: Simplice Razafindrazaka, Interpr&egrave;te: Fafah. 
<a href="https://www.youtube.com/watch?v=GmR4Mocceiw">www.youtube.comwatch?v=GmR4Mocceiw</a></p>
<p>Page Les 7 G&eacute;n&eacute;rations: Ny lanitra mangamanga, Auteur-Compositeur: Sylvestre Randafison, Interpr&egrave;te: Ny Antsaly. 
<a href="http://gasy.net/video/clip/49097/lanitra-mangamanga-randafison-sylvestre-ny-antsaly.html">lanitra-mangamanga-randafison-sylvestre-ny-a</a></p>
<p>Page "Veloma e!" : Aiza ho aiza re izay dadako izay, Auteur-Compositeur: Therack, Interpr&egrave;te: Ritsoka.</p>

-----------------------------------------------

<p>Page1: Afindrafindrao, chant traditionnel et danse des Hautes Terres Malagasy, interpr&eacute;t&eacute; ici par Monique Rabaraona (Les Surfs) <a href="http://www.masombahiny.com/afindra.mp3">afindra</a></p>
<p>Page2: Ombintsaretin-dRaleva, Auteur-Compositeur: Ernest Randria, Interpr&egrave;tes: Patsy <a href="https://www.youtube.com/watch?v=3fOx_THLSJc">www.youtube.com/watch?v=3fOx_THLSJc</a> ainsi que Lova Mamisoa <a href="https://www.youtube.com/watch?v=FdjvJP3tIJs"></a>www.youtube.com/watch?v=FdjvJP3tIJs</p>
<p>Page3: Arosoy ny lakan-tsika, razandry: Interpr&egrave;te: Sakelindalana. <a href="https://www.youtube.com/watch?v=zvxiKy8ICN4">www.youtube.com/watch?v=zvxiKy</a></p>
<p>Page4: Iny hono izy, Ravorombazaha: Berceuse traditionnelle, Interpr&egrave;te: Patsy. <a href="https://www.youtube.com/watch?v=IYDJzBsdKzE">www.youtube.com/watch?v=IYDJzBsdKzE</a></p>
<p>Page5: Arotsay dieny izao: Interpr&egrave;te: Feo Gasy. <a href="http://www.youtube.com/watch?v=5zGtGKK01Wk">www.youtube.com/watch?v=5zGtGKK01Wk</a> </p>
<p>Page6: Ny vady: Auteur: Georges Andriamanantena, Compositeur: Naly Rakotofiringa, Interpr&egrave;tes: Jeanne et Naly Rakotofiringa. <a href="https://www.youtube.com/watch?v=qsjeVuYOB_E">www.youtube.com/watch?v=qsjeVuYOB_E</a></p>
<p>Page7: Vorompotsy miaradia: Auteur-Compositeur: Therack, Interpr&egrave;te: Solika. <a href="https://www.youtube.com/watch?v=exj8pN9JbO8">www.youtube.com/watch?v=exj8pN9JbO8</a></p>
<p>Page8: Sompatra: chanson traditionnelle interpr&eacute;t&eacute;e par Barijaona et Odette Suzannah <a href="https://www.youtube.com/watch?v=o__YDZdF8XQ">www.youtube.com/watch?v=o__YDZdF8XQ</a></p>
<p>Page9: Revin-gadrako: Auteur-Compositeur-Interpr&egrave;te: Mamih Bastah <a href="https://www.youtube.com/watch?v=UX5o6rGpPFA">www.youtube.com/watch?v=UX5o6rGpPFA</a></p>
<p>Page10: Bako: Auteur-Compositeur: Barijaona. Interpr&egrave;tes: Barijaona et Odette Suzannah. <a href="https://www.youtube.com/watch?v=H44B_Z0pQSk">www.youtube.com/watch?v=H44B_Z0pQSk</a></p>
<p>Page11: Mba tsiditsidio: Auteur-Compositeur: Naly Rakotofiringa, Interpr&egrave;te: Salomon. <a href="https://www.youtube.com/watch?v=LAMfCpsOGAg">www.youtube.com/watch?v=LAMfCpsOGAg</a></p>
<p>Page12: Mazava antsinana: chant traditionnel des Hauts Plateaux interpr&eacute;t&eacute; ici par les Mpilalao de Fenoarivo <a href="https://www.youtube.com/watch?v=UimH5t359TU">www.youtube.com/watch?v=UimH5t359TU</a></p>
<p>Page13: Diavolana fenomanana takariva: musique traditionnelle des Hauts Plateaux, interpr&eacute;t&eacute; par Laka <a href="https://www.youtube.com./watch?v=g9-NcYHlo54"></a> </p>
<p>Page14: Ilay takariva kely: Compositeur: Justin Rajoro, Interpr&egrave;te: Sakelidalana.
 <a href="http://www.dailymotion.com/video/xcm3au_ilay-takariva-kely-sakelidalana_music">www.dailymotion.com/video/xcm3au_ilay-takariva-kely-sakelidalana_music</a></p>
<p>Page15: Ianao: Dama, Dadah "Mahaleo". <a href="http://2junky.com/video/2392465/mahaleo-ianao-a-rakotobe-c-andrianaivo-r-razafindranoa.html">2junky.com/video/2392465/mahaleo-ianao-a-rakotobe-c-andrianaivo-r-razafindranoa.html</a></p>
<p>Page16: Veloma ry fahazazana: Mahaleo. <a href="http://gasy.net/video/clip/43647/veloma-ry-fahazazana-mahaleo.html">gasy.net/video/clip/43647/veloma-ry-fahazazana-mahaleo.html</a></p>
<p>Page17: Ekeko satria Interpr&egrave;tes: Gaby et Faniry <a href="https://www.youtube.com/watch?v=B9VMwVvsdEg">www.youtube.com/watch?v=B9VMwVvsdEg</a></p>
<p>Page18: Falifaly (valiha), Ny Antsaly <a href="https://www.youtube.com/watch?v=Uc3paSgEBMU">www.youtube.com/watch?v=Uc3paSgEBMU</a></p>

<p>Page19: Gasikara, Auteur-Compositeur-interpr&egrave;te: Rakoto Frah TeloFangady <a href="http://gasy.net/video/clip/20518/telofangady-rakoto-frah-gasikara.html">gasy.net/video/clip/20518/telofangady-rakoto-frah-gasikara.html</a></p>
<p>Page20: Ikala manja: Feo Gasy. <a href="http://mp3freefree.com/manja,nspeh4S0ba,download.html">mp3freefree.com/manja,nspeh4S0ba,download.html</a></p>
<p>Page21: Ny lambanao mikopakopaka: Auteur-Compositeur: Henri Ratsimbazafy, Interpr&egrave;tes: Voahirana et Henri Ratsimbazafy. <a href="http://www.musicvideos.the-real-africa.com/madagascar/henri_ratsimbazafy_voahirana_ny_lambanao_mikopakopa">www.musicvideos.the-real-africa.com/madagascar/henri_ratsimbazafy_voahirana_ny_lambanao_mikopakopa</a></p>
<p>Page22: Izahay sy i Malala (valiha) Ny Antsaly, <a href="https://www.youtube.com/watch?v=K46u0u9_wLs">www.youtube.com/watch?v=K46u0u9_wLs</a></p>
<p>Page23: Raha ny rano natsakaina: Auteur-Compositeur: Andrianary Ratianarivo, Interpr&egrave;te: Solika. <a href="http://gasy.net/video/clip/67608/raha-ny-rano-natsakaina-solika.html"> </a> ou <a href="https://www.youtube.com/watch?v=wOzeNsklVsY">www.youtube.com/watch?v=wOzeNsklVsY</a></p>
<p>Page24: Ilay vohitra manga, Auteur: Bernard, Compositeur: Naly Rakotofiringa, Interpr&egrave;te: Ludger Andrianjaka <a href="https://www.youtube.com/watch?v=ndiWhBGSmNk">www.youtube.com/watch?v=ndiWhBGSmNk</a></p>
<p>Page25 &agrave; Page31: Gasikara, Auteur-Compositeur-Interpr&egrave;te: Rakoto Frah TeloFangady <a href="http://gasy.net/video/clip/20518/telofangady-rakoto-frah-gasikara.html"></a></p>
<p>Page32: Ry toera-manirery, Auteur-Compositeur: Naly Rakotofiringa, Interpr&egrave;te: Lilie. <a href="https://www.youtube.com/watch?v=_0qd9b2R7T8">www.youtube.com/watch?v=_0qd9b2R7T8</a></p>
<p>Page33: Ry Iarivo tsy foiko: Eric Manana, Bodo, Poopy, Aina Quach, Fafah <a href="http://gasy.net/video/clip/8741/erick-manana-bodo-poopy-bebey-aina-quach-fafah.html">erick-manana-bodo-poopy-bebey-aina-quach-fafah.html</a></p>
Page34: Aiza ho aiza re izay dadako izay, Auteur-Compositeur Therack, Interpr&egrave;te: Ritsoka</p>
------------------------------------
/*
Page com/rakotobert.html : Voahirana o, Auteur-Compositeur-Interpr&egrave;te: Henri Ratsimbazafy. http://www.anathoth.fr/index2.php?option=com_content&do_pdf=1&id=219
Page com/Joseph_Ravalison.php : Nahaona no mitomany, Auteur-Compositeur-Interpr&egrave;te: Z&eacute;z&eacute; Mahanoro. https://www.youtube.com/watch?v=SEX2EDnTXRA
Page com/izy_7_mianadahy_ANDRIAMANANA.php : Raozy manambaka raozy, Compositeur: Freddy Ranarison https://www.youtube.com/watch?v=xlbG8dzDzkU
Page com/sary_justin_razaka.php : Tsy mba misy tsy ho diso, Auteur-Compositeur: Justin Rajoro, Interpr&egrave;te: Solika. https://www.youtube.com/watch?v=DP_FLCUlswM
Page com/sary_justine_ratsarabe.php : Babako roa, Auteur-Compositeur: Andrianary Ratianarivo, Interpr&egrave;te: Solika. http://www.dailymotion.com/video/x806bz_solika-bakobako-ro-andrianary-ratia_people
Page com/sary_henriette.php : Fikasana, Auteur-Compositeur: Naly Rakotofiringa, Interpr&egrave;te: Ando Rakotoarisoa. https://www.youtube.com/watch?v=iFeqnX_Y05E
Page com/Taranaka_Juliette_Ratsiorimisa.php : Indreto izahay auteur-Compositeur: Justin Rajoro, interpr&eacute;t&eacute; ici au piano par Sammy Rakotoarimalala https://www.youtube.com/watch?v=FiW7Xawe0ns
Page com/FamilleRavalinisaRakotondravohitra.php : Ho tsarovantsika eto, Interpr&egrave;te: Nada Miangola https://www.youtube.com/watch?v=oHAtEFsWVSE
Page com/sary_gabriel_andriantseheno.php : Injany feom-boro-mikalo: Compositeur: Naka Rabemanantsoa, Interpr&egrave;te: ATAUM AssociationTh&eacute;&acirc;trale & Artistique de l'Universit&eacute; de Madagascar https://www.youtube.com/watch?v=X3eqB9Mc-eM
Page com/sary_juliette_ratsiorimisa_mianakavy.php : Indro tazako,Interpr&egrave;te: Sakelindalana  https://www.youtube.com/watch?v=uAqaIDq53sU	
Page com/LOUIS_RAKOTOVAO_INGE_LORE_URBACH.php : Ny lanitra mangamanga, Ny Antsaly, http://www.ronsard-andriamanana.globalvision.mg/en/home.html
page com/sary_guy.php : Nandao anao ela Raiko, Cantique protestant chant&eacute; par la Chorale du Groupe de Jeunes de FPMA Lille. https://www.youtube.com/watch?v=tPcNsia6Gn8 

------------------------------------------------------

TOUTES LES CHANSONS / ALL THE SONGS
Manaraka ny laharan'ny abidia.
Par ordre alphab&eacute;tique.
In alphabetical order 

Afindrafindrao, chant traditionnel et danse des Hautes Terres Malagasy, interpr&eacute;t&eacute; ici par Monique Rabaraona (Les Surfs)  http://www.masombahiny.com/afindra.mp3 .
Aiza ho aiza re izay dadako izay Auteur-Compositeur: Therack, Interpr&egrave;te: Ritsoka. (Aucune vid&eacute;o trouv&eacute;e sur internet).
Arosoy ny lakan-tsika, Razandry: Sakelindalana https://www.youtube.com/watch?v=zvxiKy8ICN4
Arotsahy dieny izao : Feo Gasy https://www.youtube.com/watch?v=5zGtGKK01Wk 
Auld Lang Syne, vieille m&eacute;lodie &eacute;cossaise interpr&eacute;t&eacute;e par Susan Boyle. https://www.youtube.com/watch?v=Voq8OIU9kTM  
-------------------------------------------------------
Bako Auteur-Compositeur: Barijaona, Interpr&egrave;tes: Barijaona et Odette Suzannah. https://www.youtube.com/watch?v=H44B_Z0pQSk
Bakobako roa, Auteur-Compositeur: Andrianary Ratianarivo, Interpr&egrave;te: Solika. http://www.dailymotion.com/video/x806bz_solika-bakobako-ro-andrianary-ratia_people
--------------------------------------------------------- 
Diavolana fenomanana takariva: chanson traditionnelle des Hauts Plateaux, interpr&eacute;t&eacute; par Freddy Randria, http://gasy.net/video/clip/52123/diavolana-fenomanana-takariva-cover-didia-reps-live-2013.html 
et aussi par l'Ensemble Laka https://www.youtube.com/watch?v=g9-NcYHlo54 
----------------------------------------------------------
Ekeko satria: Gaby et Faniry http://www.dailymotion.com/video/x7x36d_ekeko-satria-gabhy-faniry_music
Eo antananao, ry Raiko: Cantique protestant N�462, Version malagasy de J. Rajamaria https://www.youtube.com/watch?v=o-qe5yQBiPg 
Esory re, ry Raiko: Auteur-Compositeur: Simplice Razafindrazaka, Interpr&egrave;te: Fafah. https://www.youtube.com/watch?v=GmR4Mocceiw 
----------------------------------------------------------
Falifaly: Ny Antsaly https://www.youtube.com/watch?v=Uc3paSgEBMU 
Fa tsarotsaroko: Feo Gasy https://www.youtube.com/watch?v=iNjEyl-sUIw
Fihavanana malagasy: Feo Gasy. https://www.youtube.com/watch?v=cxgHQrA4R2U
Fikasana: Auteur-Compositeur: Naly Rakotofiringa, Interpr&egrave;te: Ando Rakotoarisoa. https://www.youtube.com/watch?v=iFeqnX_Y05E 
----------------------------------------------------------
Gasikara: Auteur-Compositeur-Interpr&egrave;te: Rakoto Frah TeloFangady http://gasy.net/video/clip/20518/telofangady-rakoto-frah-gasikara.html 
----------------------------------------------------------
Handao ity tany mahazatra: Compositeur: Naka Rabemanantsoa http://gasy.net/video/clip/41118/handao-ity-tana-mahazatra-par-naka-rabemanantsoa.html 
Ho tsarovantsika eto, Auteur: Samuel Rahamefy, Compositeur: Justin Rajoro, Interpr&egrave;te: Nada Miangola https://www.youtube.com/watch?v=oHAtEFsWVSE 
Hody Aho: Groupe Laurent Rakotomamonjy (GLR) http://parolyric.blogspot.com/2012/09/hody-aho-laurent-rakotomamonjy-glr.html
----------------------------------------------------------
Ianao no nofidiko: Dama, Dadah "Mahaleo".http://2junky.com/video/2392465/mahaleo-ianao-a-rakotobe-c-andrianaivo-r-razafindranoa.html 
Iarivoko: Telo Fangady https://www.youtube.com/watch?v=82gWNSdA8Zg&list=PL87BCEE236D0AF97D&index=47
Ikala manja: Feo Gasy. http://mp3freefree.com/manja,nspeh4S0ba,download.html
Ilay takariva kely: Compositeur: Justin Rajoro, Interpr&egrave;te: Sakelidalana.
http://www.dailymotion.com/video/xcm3au_ilay-takariva-kely-sakelidalana_music 
Ilay tany niaviko: Feo Gasy https://www.youtube.com/watch?v=00pbuHepiyI  + https://www.youtube.com/watch?v=HBdx1UcImSU
Ilay vohitra manga: Auteur: Bernard, Compositeur: Naly Rakotofiringa, Interpr&egrave;te: Ludger Andrianjaka https://www.youtube.com/watch?v=ndiWhBGSmNk
Indreto izahay: Auteur-Compositeur: Justin Rajoro, Interp&eacute;t&eacute; au piano pas Sammy Rakotoarimalala. https://www.youtube.com/watch?v=FiW7Xawe0ns
Indro tazako, indro tazako manangasanga, Sakelindalana https://www.youtube.com/watch?v=uAqaIDq53sU
Injany feom-boro-mikalo: Compositeur: Naka Rabemanantsoa, Interpr&egrave;te: ATAUM AssociationTh&eacute;&acirc;trale & Artistique de l'Universit&eacute; de Madagascar https://www.youtube.com/watch?v=X3eqB9Mc-eM
Iny hono izy, Ravorombazaha: Berceuse traditionnelle, Interpr&egrave;te: Patsy. https://www.youtube.com/watch?v=IYDJzBsdKzE 
Izahay sy i Malala (valiha), Ny Antsaly https://www.youtube.com/watch?v=K46u0u9_wLs
--------------------------------------------------- 
Lalatiana: interpr&eacute;t&eacute; par Ny Voninavoko https://www.youtube.com/watch?v=ADqeXWppFWc
---------------------------------------------------
Madagasikara, Auteur-Compositeur-interpr&egrave;te: Naly Rakotofiringa, 
Mahereza: Zandry Gasy https://www.youtube.com/watch?v=-mSrZXio1UM
Mama , Auteur-Compositeur-interpr&egrave;te: Ramahafadrahona. http://video-streaming.orange.fr/musique/ramahafadrahona-mama-VID0000001rjUP.html 
Mandihiza Ra-hitsikitsika, chant traditionnel des Hauts Plateaux interpr&eacute;t&eacute; par un groupe d'enfants https://www.youtube.com/watch?v=eNIGqOsseX0
Manimanina Ny Antsaly https://www.youtube.com/watch?v=piu1SZxH5Rk
Mazava antsinana: chant traditionnel des Hauts Plateaux interpr&eacute;t&eacute; ici par les Mpilalao de Fenoarivo https://www.youtube.com/watch?v=UimH5t359TU
Mba tsiditsidio: Auteur-Compositeur: Naly Rakotofiringa, Interpr&egrave;te: Salomon. https://www.youtube.com/watch?v=LAMfCpsOGAg 
Mpivarodamba sy mpivarokena:  Auteur-compositeur: Ramilson Besigara, Interpr&egrave;tes: Randratelo. https://www.youtube.com/watch?v=h_wR2NIlVIY 
----------------------------------------------------
Nahaona no mitomany, Auteur-Compositeur-Interpr&egrave;te: Z&eacute;z&eacute; Mahanoro. https://www.youtube.com/watch?v=SEX2EDnTXRA 
Nandao anao ela Raiko, Cantique protestant N�417, Auteur-Compositeur: Rajoely J&eacute;r&ocirc;me, Interpr&egrave;te: la Chorale du Groupe de Jeunes de FPMA Lille. https://www.youtube.com/watch?v=tPcNsia6Gn8 
Niraraka teto ny vonim-boasary: Solika https://www.youtube.com/watch?v=qYuL0A0LoWQ
Nosy Madagasikara, Gilbert Ramiarison http://www.allvideogasy.com/ramiarison-gilbert/nosy-madagasikara-video_9952e02a6.html
Ny eo akaikinao, sombinaina Auteur-Compositeur: Therack https://www.youtube.com/watch?v=g77RzU7M47M  
Ny lambanao mikopakopaka: Auteur-Compositeur: Henri Ratsimbazafy, Interpr&egrave;tes: Voahirana et Henri Ratsimbazafy. https://www.youtube.com/watch?v=wA8Q6JwJYlA
Ny lanitra mangamanga, Auteur-Compositeur: Sylvestre Randafison, Interpr&egrave;te: Ny Antsaly. http://gasy.net/video/clip/49097/lanitra-mangamanga-randafison-sylvestre-ny-antsaly.html 
Ny vady: Auteur: Georges Andriamanantena, Compositeur: Naly Rakotofiringa, Interpr&egrave;tes: Jeanne et Naly Rakotofiringa.https://www.youtube.com/watch?v=qsjeVuYOB_E 
Ny voamaintilany: Auteur-Compositeur: Justin Rajoro, interpr&eacute;t&eacute; au piano par Sammy Rakotoarimalala https://www.youtube.com/watch?v=Q2VZfjTdrJk et http://www.dailymotion.com/video/xbm51z_kalf-retro-ny-voamaintilany-old_people
-------------------------------------------------------
Ombintsaretin-dRaleva, Auteur-Compositeur: Ernest Randria, Interpr&egrave;tes: Patsy https://www.youtube.com/watch?v=3fOx_THLSJc ainsi que Lova Mamisoa https://www.youtube.com/watch?v=FdjvJP3tIJs 
O, ry Fody an'ala: Feo Gasy https://www.youtube.com/watch?v=aGNT2EHdLK8
------------------------------------------------------
Raha ny rano nantsakaina: Auteur-Compositeur: Andrianary Ratianarivo, Interpr&egrave;te: Solika. http://gasy.net/video/clip/67608/raha-ny-rano-natsakaina-solika.html  ou https://www.youtube.com/watch?v=wOzeNsklVsY
Raozy manambaka raozy, Compositeur: Freddy Ranarison https://www.youtube.com/watch?v=xlbG8dzDzkU
Rehefa maty aho: Auteur: Jean Verdi Salomon Razakandraina 'Dox", Compositeur: Jasmine Ratsimisata, Interpr&egrave;te: Rakoto Frah https://www.youtube.com/watch?v=cxES50jVjEg
Revin-gadrako: Auteur-Compositeur-interpr&egrave;te: Mamih Basta. https://www.youtube.com/watch?v=UX5o6rGpPFA
R'ilay otrikafon'ny foko, Compositeur: Therack, Interpr&egrave;te: ?
Ry Iarivo tsy foiko: Eric Manana, Bodo, Poopy, Aina Quach, Fafah http://gasy.net/video/clip/8741/erick-manana-bodo-poopy-bebey-aina-quach-fafah.html 
Ry toera-manirery, Auteur-Compositeur: Naly Rakotofiringa, Interpr&egrave;te: Lilie. https://www.youtube.com/watch?v=_0qd9b2R7T8
------------------------------------------------------------
Sompatra: chanson traditionnelle interpr&eacute;t&eacute;e par Barijaona et Odette Suzannah https://www.youtube.com/watch?v=o__YDZdF8XQ 
------------------------------------------------------------
Tambazako: Eric Manana https://www.youtube.com/watch?v=AIhNGxODaTA 
Tonga vahiny ianareo : Interpr&egrave;tes: Solika https://www.youtube.com/watch?v=67D5XOzu5iw
Totoy tsara ny varinareo: Ny Antsaly https://www.youtube.com/watch?v=IHaB475-Fm 
Tsy maty de Barijaona, Interpr&egrave;te: Johary http://gasy.net/video/clip/14090/video-johary-tsy-maty-barijaona.html
Tsy mba misy tsy ho diso, Auteur-Compositeur: Justin Rajoro, Interpr&egrave;te: Solika. https://www.youtube.com/watch?v=DP_FLCUlswM 
Tsy resy tsy akory ny olo-mangina Auteur-Compositeur: Barijaona, Interpr&egrave;tes: Barijaona et Odette Suzannah https://www.youtube.com/watch?v=CpjWNMncRM8&index=1&list=PLUl-6xZnZvOZsKHC9uh9aShhHKOJSR0CW
 et aussi http://www.anathoth.fr/index.php?option=com_content&view=article&id=177:tsy-resy-tsy-akory&catid=43:hira&Itemid=75
Tsiriry ahitra hono e, tsiriry vorona: Troupe Voninavoko https://www.youtube.com/watch?v=El6OqViH_QU
-----------------------------------------------------------
Veloma re, ry dada havako: Compositeur: Therack, Interpr&egrave;te: Solika. http://gasy.net/video/clip/22283/veloma-re-ry-dada-havako-solika.html
Veloma re, ry Saidomara: Interpr&egrave;tes: Marguerite et Razanatsoa http://musictonic.com/music/Marguerite+and+Razanatsoa#v=EhjCmi24LHw
Veloma ry fahazazana: Mahaleo. http://gasy.net/video/clip/43647/veloma-ry-fahazazana-mahaleo.html
Voahirana o, Auteur-Compositeur-Interpr&egrave;te: Henri Ratsimbazafy. http://www.anathoth.fr/index2.php?option=com_content&do_pdf=1&id=219
Volan'ny kamboty: Ny Railovy https://www.youtube.com/watch?v=CJgOTCwgpC4
Voninkazo adaladala: Eric Manana et Aina Quach http://www.dailymotion.com/video/x8e4bb_voninkazo-adaladala_music
Vorombeo tsaradia: Eric Manana https://www.youtube.com/watch?v=7nNQFgOQjuM
Vorompotsy miaradia: Auteur-Compositeur: Therack, Interpr&egrave;te: Solika. https://www.youtube.com/watch?v=exj8pN9JbO8 
Vorona &ocirc; : Barijaona et Odette Suzannah, https://www.youtube.com/watch?v=MUBKCtHvd8o*/
 ?>