<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<body>
<div style="background-color:#FFFFFF; text-align:left; padding:30px;">
  <div> Inty ny lisitrin'ny hira sy mozika-lafika malagasy henonareo ao&nbsp;amin'inty tetiarana inty. Ny ankabehazan'ny hira sy mozika eto dia&nbsp;ireo atao hoe "kalon'ny fahiny" sy "hiran'omaly"ary koa ireo izay ao&nbsp;anatin'ny vakodrazana eto anivon'Imerina fa misy ihany koa hira&nbsp;vitsivitsy izay tsy ao anatin'izany.<br>
    <br>
  </div>
  <div>- Voici la liste des chansons et morceaux de musique malagasy entendus&nbsp;en fond sonore, dans cet arbre g&eacute;n&eacute;alogique. A quelques exceptions&nbsp;pr&egrave;s, il s'agit principalement des vieilles chansons du pass&eacute; bien&nbsp;lointain, appel&eacute;es "kalon'ny fahiny", des m&eacute;lodies de jadis et&nbsp;nagu&egrave;re, les "hiran'omaly"et d'airs folkloriques et populaires des&nbsp;Hauts Plateaux de Madagascar.<br>
    <br>
  </div>
  <div>- Here is the list of the Malagasy background music and songs you may&nbsp;hear in this family tree. With a few exceptions, they are mainly the&nbsp;very old songs of the far-away past, known as "kalon'ny fahiny", the&nbsp;old songs of yesteryear and of not so long ago, aka "hiran'omaly" and&nbsp;the folk and popular tunes of the Highlands of Madagascar.<br>
    <br>
  </div>
  <div>Page Accueil: Madagasikara: Auteur-Compositeur-Interpr&egrave;te: Naly Rakotofiringa.<br>
    Page La Famille: Nahoana no mitomany , Auteur-Compositeur-Interpr&egrave;te:&nbsp;Z&eacute;z&eacute; Mahanoro..<br>
    Page Photos: Veloma re, ry dada havako: Compositeur: Therack Ramamonjisoa,&nbsp;Interpr&egrave;te: Solika.<br>
    Page Notes diverses et biographie: Esory re, ry Raiko:&nbsp;Auteur-Compositeur: Simplice Razafindrazaka, Interpr&egrave;te: Fafah.<br>
    Page Les 7 G&eacute;n&eacute;rations: Ny lanitra mangamanga, Auteur-Compositeur:&nbsp;Sylvestre Randafison, Interpr&egrave;te: Ny Antsaly.<br>
    Page "Veloma e!" : Aiza ho aiza re izay dadako izay,&nbsp;Auteur-Compositeur: Therack Ramamonjisoa, Interpr&egrave;te: Ritsoka.<br>
  <br>
  </div>
  <div>-----------------------------------------------<br>
    <br>
  </div>
  <div>Page1: Afindrafindrao, chant traditionnel et danse des Hautes Terres&nbsp;Malagasy, interpr&eacute;t&eacute; ici par Monique Rabaraona (Les Surfs).<br>
    Page2: Ombintsaretin-dRaleva, Auteur-Compositeur: Ernest Randria,&nbsp;Interpr&egrave;te: Patsy.<br>
    Page3: Ikala Manja de Feo Gasy.<br>
    Page4: Iny hono izy, Ravorombazaha: Berceuse traditionnelle,&nbsp;Interpr&egrave;te: Patsy.<br>
    Page5: Ry toera-manirery: Auteur-compositeur: Naly Rakotofiringa.&nbsp;Interpr&egrave;te:Lilie.<br>
    Page6: Ny vady: Auteur: Georges Andriamanantena, Compositeur: Naly&nbsp;Rakotofiringa, Interpr&egrave;tes: Jeanne et Naly Rakotofiringa.<br>
    Page7: Vorompotsy miaradia: Auteur-Compositeur: Therack, Interpr&egrave;te: Solika.<br>
    Page8: Sompatra: chanson traditionnelle interpr&eacute;t&eacute;e par Barijaona et&nbsp;Odette Suzannah.<br>
    Page9: Revin-gadrako: Auteur-Compositeur-Interpr&egrave;te: Mamih Bastah.<br>
    Page10: Voahirana Auteur-compositeur: Henri Ratsimbazafy Interpr&eacute;t&eacute; au&nbsp;piano par Sammy Rakotoarimalala.<br>
    Page11: Mba tsiditsidio: Auteur-Compositeur: Naly Rakotofiringa,&nbsp;Interpr&egrave;te: Salomon.<br>
    Page12: Mazava antsinana: chant traditionnel des Hauts Plateaux,&nbsp;version instrumentale.<br>
    Page13: Diavolana fenomanana takariva: chant traditionnel des Hauts&nbsp;Plateaux, version instrumentale.<br>
    Page14: Ilay takariva kely: Compositeur: Justin Rajoro, Interpr&egrave;te:&nbsp;Sakelindalana.<br>
    Page15: Ianao par Dama, Dadah "Mahaleo".<br>
    Page16: Veloma ry fahazazana: Mahaleo.<br>
    Page17: Arosoy ny lakantsika, razandry par Sakelindalana.<br>
    Page18: Mandihiza ra-hitsikitsika, chant traditionnel interpr&eacute;t&eacute; par&nbsp;l'Ensemble Laka.<br>
    Page19: Gasikara, Auteur-Compositeur-Interpr&egrave;te: TeloFangady.<br>
    Page20: Ombintsaretin-dRaleva: Auteur-Compositeur: Ernest Randria,&nbsp;Interpr&egrave;te: Lova Mamisoa.<br>
    Page21: Ny lambanao mikopakopaka: Auteur-Compositeur: Henri&nbsp;Ratsimbazafy, Interpr&egrave;tes: Voahirana et Henri Ratsimbazafy.<br>
    Page22: Izahay sy Malala (valiha) Auteur-Compositeur: Sylvestre&nbsp;Randafison, Interpr&egrave;te: Ny Antsaly,<br>
    Page23: Raha ny rano nantsakaina: Auteur-Compositeur: Rasamy Gitara,&nbsp;Interpr&egrave;te: Solika.<br>
    Page24: 'Ndreto izahay, Auteur-Compositeur: Justin Rajoro, Interpr&eacute;t&eacute;&nbsp;au piano par Sammy Rakotoarimalala.<br>
    Page25 Fa tsarotsaroko ny andro taloha: Feo Gasy.<br>
    Page26: Diavolana Fenomanana takariva, Ensemble Laka.<br>
    Page27: Mpivarodamba, mpivarokena: Auteur-Compositeur: Ramilson&nbsp;Besigara, Interpr&egrave;tes: Randratelo.<br>
    Page28: Ilay vohitra manga: Compositeur: Naly Rakotofiringa,&nbsp;Interpr&egrave;te: Ludger Andrianjaka.<br>
    Page29: Lalatiana, Interpr&egrave;te: Ny Voninavoko.<br>
    Page30: Fihavanana Malagasy,: Feo Gasy.<br>
    Page31: Vorombe tsaradia, Interpr&egrave;te: Eric Manana.<br>
    Page32: Manimanina, Compositeur: Sylvestre Randafison, Interpr&egrave;te: Ny Antsaly.<br>
    Page33: Ny voamaintilany, Auteur-Compositeur: Justin Rajoro,&nbsp;interpr&eacute;t&eacute; au piano par Sammy Rakotoarimalala.<br>
    Page34: Aiza ho aiza re izay dadako izay, Auteur-Compositeur Therack Ramamonjisoa,&nbsp;Interpr&egrave;te: Ritsoka.<br>
    Page35: Veloma re, ry Said Omara, Interpr&egrave;tes: Marguerite et Razanatsoa.<br>
    Page36: Dobla sento, Auteur-Compositeur: Naka Rabemanantsoa,&nbsp;Interpr&egrave;te: R'Imbosa.<br>
    Page37: Hody Aho, Compos&eacute; et interpr&eacute;t&eacute; par GLR (Groupe Laurent Rakotomamonjy).<br>
    Page38: Ekeko satria, interpr&eacute;t&eacute; par Gaby et Faniry.<br>
    Page39: Nosy Madagasikarako, interpr&eacute;t&eacute; par le Groupe de Gilbert Ramiarison.<br>
    Page40: O, ry fody an'ala &ocirc;: Feo Gasy.<br>
    Page41: Ny eo akaikinao, sombin'aiko, de Th&eacute;rack Ramamonjisoa, interpr&eacute;t&eacute; par Ranja et Riri Ramandiamanana.<br>
    Page42: Ombintsaretin-dRaleva, Auteur-Compositeur: Ernest Randria,&nbsp;Interpr&egrave;te: Lova Mamisoa.<br>
    Page43: Tsiriry ahitra, tsiriry vorona par Ny Voninavoko.<br>
    Page44: Totoy tsara ny varinareo: chant traditionnel interpr&eacute;t&eacute; par Ny Antsaly.<br>
    Page45: Havako anie izy izay, Auteur-Compositeur: Andrianary&nbsp;Ratianarivo. Interpr&egrave;te: Nada Miangola.<br>
    Page46: Tiako ianao: Salomon.<br>
    Page47: Raha maninanay ianareo.<br>
    Page48: Iarivo fonenana, Auteur-Compositeur: Naka Rabemanantsoa, Interpr&egrave;te: R'Imbosa.<br>
    Page49: Haody ry Analamangako, Auteur-Compositeur: Jean&nbsp;Ratsaramiafara, Interpr&egrave;te: R'imbosa.<br>
    Page50: Mba tsiditsidio, Auteur-Compositeur: Naly Rakotofiringa,&nbsp;Interpr&egrave;te: Salomon.<br>
    Page51: Ny fitia vita voady, Auteur-Compositeur: Romule, Interpr&egrave;tes:&nbsp;Lilie &amp; Luk.<br>
    Page52: Tsofiko rano, Auteur-Compositeur: Justin Rajoro, Interpr&egrave;tes:&nbsp;Rossy &amp; Solika.<br>
    Page53: Ity hirako ity, Thiera Bruno.<br>
    Page54: Ka tianao va, Thiera Bruno.<br>
    Page55: Tanisa, tanisa (version instrumentale), Auteur-Compositeur:&nbsp;Naly Rakotofiringa. Au piano: Sammy Rakotoarimalala.<br>
    Page56: Ho tsarovantsiko eto, Auteur: Samuel Rahamefy, Compositeur:&nbsp;Justin Rajoro, Interpr&egrave;te: Salomon.<br>
    Page57: Midira, midira zana-boromanga, Chant traditionnel, Chorale des&nbsp;jeunes de la FPMA Lille.<br>
    Page58: Trano mbongo, Version malagasy (Scott Davis, Roger Rajaonary)&nbsp;de "In the ghetto" d'Elvis Presley interpr&eacute;t&eacute;e par Ny Akoro du Sud de&nbsp;Madagascar.<br>
    Page59: Rehefa maty aho, Auteur: Jean Verdi Salomon Razakandraina&nbsp;"Dox", Compositeur: Jasmine Ratsimisata, Interpr&egrave;tes: Rakoto Frah et&nbsp;Feo Gasy.</div>
  <div><br>
    -------------------------------------------</div>
  <div><br>
    Page com/rakotobert.html : Voahirana o, Auteur-Compositeur-Interpr&egrave;te:&nbsp;Henri Ratsimbazafy.<br>
    Page com/sary_clarice_rafarasoa.php : Ny vady, Naly et Jeanne Rakotofiringa.<br>
    Page com/Joseph_Ravalison.php : Nahoana no mitomany,&nbsp;Auteur-Compositeur-Interpr&egrave;te: Z&eacute;z&eacute; Mahanoro.<br>
    Page com/izy_7_mianadahy_Andriamanana.php : Raozy manambaka raozy,&nbsp;Compositeur: Freddy Ranarison<br>
    Page com/sary_justin_razaka.php : Tsy mba misy tsy ho diso,&nbsp;Auteur-Compositeur: Justin Rajoro, Interpr&egrave;te: Solika.<br>
    Page com/sary_justine_ratsarabe.php : Babako roa, Auteur-Compositeur:&nbsp;Andrianary Ratianarivo, Interpr&egrave;te: Solika.<br>
    Page com/sary_henriette.php : Fikasana, Auteur-Compositeur: Naly&nbsp;Rakotofiringa, Interpr&egrave;te: Ando Rakotoarisoa.<br>
    Page com/Taranaka_Juliette_Ratsiorimisa.php : Indreto izahay:&nbsp;Auteur-Compositeur: Justin Rajoro, interpr&eacute;t&eacute; ici au piano par Sammy&nbsp;Rakotoarimalala.<br>
    Page com/FamilleRavalinisaRakotondravohitra.php : Ho tsarovantsika&nbsp;eto, de Samuel Rahamefy et Justin Rajoro, Interpr&egrave;te: Nada Miangola.<br>
    Page com/sary_gabriel_andriantseheno.php : Izany ny fomban'ny tany.<br>
    Page com/sary_juliette_ratsiorimisa_mianakavy.php : Gasikarako,&nbsp;Interpr&egrave;te: TeloFangady.<br>
    Page com/bert_rakoto_sy_marguerite_razafindratavy.php: Aiza ho aiza&nbsp;izay dadako izay. Auteur Compositeur: Th&eacute;rack Ramamonjisoa, Interpr&egrave;te: Ritsoka<br>
    Page com/fianakaviana_paul_ranaivo.php, Aiza ho aiza izay dadako&nbsp;izay. Auteur Compositeur: Th&eacute;rack Ramammonjisoa. Interpr&egrave;te: Ritsoka<br>
    Page com/Louis_Rakotovao_Inge_Lore_urbach.php : Ny lanitra mangamanga,&nbsp;Ny Antsaly,<br>
    Page com/sary_guy.php : Nandao anao ela Raiko, Cantique protestant&nbsp;chant&eacute; par la Chorale du Groupe de Jeunes de FPMA Lille.<br>
  <br>
  </div>
  <div>------------------------------------------------------------<br>
    <br>
  </div>
  <div><strong>Toutes les chansons par ordre alphab&eacute;tique / All the songs in alphabetical order</strong><br>
    <br>
    Afindrafindrao, chant traditionnel et danse des Hautes Terres&nbsp;Malagasy, interpr&eacute;t&eacute; ici par Monique Rabaraona (Les Surfs).<br>
    Aiza ho aiza re izay dadako izay Auteur-Compositeur: Therack Ramamonjisoa,&nbsp;Interpr&egrave;te: Ritsoka.<br>
    Arosoy ny lakan-tsika, Razandry Sakelindalana.<br>
    Arotsahy dieny izao Feo Gasy<br>
    -------------------------------------------------------<br>
    Bako Auteur-Compositeur: Barijaona, Interpr&egrave;tes: Barijaona et Odette Suzannah.<br>
    Bakobako roa, Auteur-Compositeur: Andrianary Ratianarivo, Interpr&egrave;te: Solika.<br>
    ---------------------------------------------------------<br>
    Diavolana fenomanana takariva: chanson traditionnelle des Hauts&nbsp;Plateaux, interpr&eacute;t&eacute; par Freddy Randria + par l'Ensemble Laka .<br>
    Dobla sento: Compositeur: Naka Rabemanantsoa, Interpr&egrave;te: R'Imbosa.<br>
    ----------------------------------------------------------<br>
    Ekeko satria: Gaby et Faniry<br>
    Esory re, ry Raiko: Auteur-Compositeur: Simplice Razafindrazaka,&nbsp;Interpr&egrave;te: Fafah.<br>
    ----------------------------------------------------------<br>
    Falifaly: Ny Antsaly.<br>
    Fa tsarotsaroko Feo Gasy.<br>
    Fihavanana Malagasy: Feo Gasy.<br>
    Fikasana: Auteur-Compositeur: Naly Rakotofiringa, Interpr&egrave;te: Ando&nbsp;Rakotoarisoa.<br>
    ----------------------------------------------------------<br>
    Gasikarako: Auteur-Compositeur-Interpr&egrave;te: TeloFangady.<br>
    ----------------------------------------------------------<br>
    Handao ity tany mahazatra: Compositeur: Naka Rabemanantsoa.<br>
    Haody ry Analamangako, Auteur-Compositeur: Jean Ratsaramiafara,&nbsp;Andrianary Ratianarivo, Interpr&egrave;te: R'imbosa.</div>
  <div>Havako anie izy izay: Auteur-Compositeur: Andrianary Ratianarivo,&nbsp;Interpr&egrave;te:Nada Miangola.<br>
    Ho tsarovantsika eto, Auteur: Samuel Rahamefy, Compositeur: Justin&nbsp;Rajoro, Interpr&egrave;te: Nada Miangola .<br>
    Hody Aho: Groupe Laurent Rakotomamonjy (GLR) .<br>
    ----------------------------------------------------------<br>
    Ianao no nofidiko: Dama, Dadah "Mahaleo".<br>
    Iarivo fonenana: Auteur-Compositeur: Naka Rabemanantsoa, Interpr&egrave;te: R'Imbosa<br>
    Ikala manja: Feo Gasy.<br>
    Ilay takariva kely: Compositeur: Justin Rajoro, Interpr&egrave;te: Sakelidalana.<br>
    Ilay vohitra manga: Auteur: Bernard, Compositeur: Naly Rakotofiringa,&nbsp;Interpr&egrave;te: Ludger Andrianjaka .<br>
    Indreto izahay: Auteur-Compositeur: Justin Rajoro, Interpr&eacute;t&eacute; au piano&nbsp;par Sammy Rakotoarimalala.<br>
    Indro tazako: Sakelindalana .<br>
    Injany feom-boro-mikalo: Compositeur: Naka Rabemanantsoa, Interpr&egrave;te:&nbsp;ATAUM Association Th&eacute;&acirc;trale &amp; Artistique de l'Universit&eacute; de&nbsp;Madagascar.<br>
    Iny hono izy, Ravorombazaha: Berceuse traditionnelle, Interpr&egrave;te: Patsy.<br>
    Ity hirako ity: Thiera Bruno.<br>
    Izahay sy i Malala (valiha), Ny Antsaly .<br>
    ---------------------------------------------------<br>
    Ka tianao va: Thiera Bruno.<br>
    ------------------------------------------------<br>
    Lalatiana: interpr&eacute;t&eacute; par Ny Voninavoko.<br>
    ---------------------------------------------------<br>
    Madagasikara, Auteur-Compositeur-interpr&egrave;te: Naly Rakotofiringa.<br>
    Mandihiza Ra-hitsikitsika, chant traditionnel des Hauts Plateaux,&nbsp;interpr&eacute;t&eacute; par l'Ensemble Laka.<br>
    Manimanina: Ny Antsaly.<br>
    Mazava antsinana: chant traditionnel des Hauts Plateaux .<br>
    Mba tsiditsidio: Auteur-Compositeur: Naly Rakotofiringa, Interpr&egrave;te: Salomon.<br>
    Midira, midira, zana-boromanga: Chant folklorique, Ensemble Laka,&nbsp;Salomon Ratianarinaivo.<br>
    Mpivarodamba sy mpivarokena: Auteur-compositeur: Ramilson Besigara, Interpr&egrave;te: Randratelo.<br>
    ----------------------------------------------------<br>
    Nahoana no mitomany, Auteur-Compositeur-Interpr&egrave;te: Z&eacute;z&eacute; Mahanoro.<br>
    Nandao anao ela Raiko, Cantique protestant N&deg;417, Auteur-Compositeur:&nbsp;Rajoely J&eacute;r&ocirc;me, Interpr&egrave;te: la Chorale du Groupe de Jeunes de la FPMA,&nbsp;Lille.<br>
    Nosy Madagasikara, Gilbert Ramiarison.<br>
    Ny eo akaikinao, sombinaina Auteur-Compositeur: Therack. Interpr&egrave;te: Solika.<br>
    Ny fitia vita voady, Auteur-Compositeur: Romule, Interpr&egrave;tes: Lilie &amp; Luk.<br>
    Ny lambanao mikopakopaka: Auteur-Compositeur: Henri Ratsimbazafy,&nbsp;Interpr&egrave;tes: Voahirana et Henri Ratsimbazafy.<br>
    Ny lanitra mangamanga, Auteur-Compositeur: Sylvestre Randafison,&nbsp;Interpr&egrave;te: Ny Antsaly.<br>
    Ny vady: Auteur: Georges Andriamanantena, Compositeur: Naly&nbsp;Rakotofiringa, Interpr&egrave;tes: Jeanne et Naly Rakotofiringa.<br>
    Ny voamaintilany: Auteur-Compositeur: Justin Rajoro, interpr&eacute;t&eacute; au&nbsp;piano par Sammy Rakotoarimalala .<br>
    -------------------------------------------------------<br>
    Ombintsaretin-dRaleva, Auteur-Compositeur: Ernest Randria,&nbsp;Interpr&egrave;tes: Patsy ainsi que Lova Mamisoa .<br>
    O, ry Fody an'ala: Feo Gasy .<br>
    -----------------------------------------------------<br>
    Raha ny rano nantsakaina: Auteur-Compositeur: Rasamy Gitara,&nbsp;Interpr&egrave;te: Solika.<br>
    Raozy manambaka raozy, Compositeur Fredy Ranaeison.<br>
    Rehefa maty aho Auteur: Dox Razakandraina,Compositeur:Jasmine&nbsp;Ratsimisata, Interpr&egrave;tes: Rakoto Frah et Feo Gasy.<br>
    Revin-gadrako: Auteur-Compositeur-interpr&egrave;te: Mamih Basta.<br>
    R'ilay otrikafon'ny foko, Compositeur: Therack Ramamonjisoa.<br>
    Ry toera-manirery, Auteur-Compositeur: Naly Rakotofiringa, Interpr&egrave;te: Lilie<br>
    ------------------------------------------------------------<br>
    Sompatra: chanson traditionnelle interpr&eacute;t&eacute;e par Barijaona et Odette Suzannah.<br>
    ------------------------------------------------------------<br>
    Tambazako: Eric Manana.<br>
    Tanisa, tanisa (version instrumentale), Auteur-Compositeur: Naly&nbsp;Rakotofiringa. Au piano: Sammy Rakotoarimalala.<br>
    Tonga vahiny ianareo : Interpr&egrave;tes: Solika.<br>
    Totoy tsara ny varinareo: Ny Antsaly.<br>
    Trano mbongo, Version malagasy de "In the ghetto" d'Elvis Presley&nbsp;interpr&eacute;t&eacute;e par Ny Akoro du Sud de Madagascar.<br>
    Tsofiko rano, Auteur-Compositeur: Justin Rajoro, Interpr&egrave;tes: Rossy &amp; Solika.<br>
    Tsy mba misy tsy ho diso, Auteur-Compositeur: Justin Rajoro,&nbsp;Interpr&egrave;te: Solika.<br>
    Tsiriry ahitra hono e, tsiriry vorona: Troupe Voninavoko.<br>
    -----------------------------------------------------------<br>
    Veloma re, ry dada havako: Compositeur: Therack Ramamonjisoa, Interpr&egrave;te: Solika.<br>
    Veloma re, ry Saidomara: interpr&egrave;tes: Marguerite et Razanatsoa.<br>
    Veloma ry fahazazana: Mahaleo.<br>
    Voahirana &ocirc;, Auteur-Compositeur-Interpr&egrave;te: Henri Ratsimbazafy.<br>
    Vorombe tsaradia: Eric Manana.<br>
    Vorompotsy miaradia: Auteur-Compositeur: Therack Ramamonjisoa, Interpr&egrave;te: Solika.<br>
    Vorona &ocirc; : Auteur-Compositeur: Barijaona, Interpr&egrave;tes: Barijaona et&nbsp;Odette Suzannah.</div>
</div>
<div class="filazana2" style="text-align:center;">
<table width="900" border="0" style="font-size:14px;">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#784f3b"><a href="page37.php" style="color:#FFF; text-decoration:underline; height:30px; line-height:30px;">Page pr&eacute;c&eacute;dente</a></td>
    <td>&nbsp;</td>
	<td bgcolor="#784f3b"><a href="page39.php" style="color:#FFF; text-decoration:underline; height:30px; line-height:30px;">Page suivante</a></td>   
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
  </tr>
</table>
</div>


</body>
