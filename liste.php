﻿<div style="background-color:#FFF; padding: 0px 30px 30px 30px; font-size:14px;">

<p>- Inty ny lisitrin'ny hira sy mozika-lafika malagasy  henonareo ao amin'inty tetiarana inty. Ny ankabehazan'ny hira sy mozika eto dia  ireo atao hoe &quot;kalon'ny fahiny&quot; sy &quot;hiran'omaly&quot;ary koa  ireo izay ao anatin'ny vakodrazana eto anivon'Imerina fa misy ihany koa hira  vitsivitsy izay tsy ao anatin'izany. </p><p>&nbsp;</p>
<p>-&nbsp;Voici  la liste des chansons et morceaux de musique malagasy entendus en fond sonore,  dans cet arbre g&eacute;n&eacute;alogique. A quelques exceptions pr&egrave;s, il s'agit  principalement des vieilles chansons du pass&eacute; bien lointain, appel&eacute;es  &quot;kalon'ny fahiny&quot;, des m&eacute;lodies de jadis et nagu&egrave;re, les  &quot;hiran'omaly&quot;et d'airs folkloriques et populaires des Hauts Plateaux  de Madagascar. </p><p>&nbsp;</p>
<p>- Here is the list of the Malagasy background music and songs you  may hear in this family tree. With a few exceptions, they are mainly the very  old songs of the far-away past, known as &quot;kalon'ny fahiny&quot;, the old  songs of yesteryear and of not so long ago, aka &quot;hiran'omaly&quot; and the  folk and popular tunes of the Highlands of Madagascar. </p><p>&nbsp;</p>
<p>Page Accueil: Madagasikara: Auteur-Compositeur-Interpr&egrave;te: Naly Rakotofiringa. <br>
  Page La Famille: Mama , Auteur-Compositeur-Interpr&egrave;te:  Ramahafadrahona. <br>
  Page Photos: Veloma re, ry dada havako: Compositeur: Therack,  Interpr&egrave;te: Solika.&nbsp; <br>
  Page Notes diverses et biographie: Esory re, ry Raiko:  Auteur-Compositeur: Simplice Razafindrazaka, Interpr&egrave;te: Fafah.&nbsp; <br>
  Page Les 7 G&eacute;n&eacute;rations: Ny lanitra mangamanga, Auteur-Compositeur:  Sylvestre Randafison, Interpr&egrave;te: Ny Antsaly.&nbsp; <br>
  Page &quot;Veloma e!&quot; : Aiza ho aiza re izay dadako izay,  Auteur-Compositeur: Therack, Interpr&egrave;te: Ritsoka. <br>
  ----------------------------------------------- <br>
  Page1: Afindrafindrao, chant traditionnel et danse des Hautes  Terres Malagasy, interpr&eacute;t&eacute; ici par Monique Rabaraona (Les Surfs)&nbsp; <br>
  Page2: Ombintsaretin-dRaleva, Auteur-Compositeur: Ernest Randria,  Interpr&egrave;te: Patsy. <br>
  Page3: Ikala Manja de Feo Gasy.&nbsp; <br>
  Page4: Iny hono izy, Ravorombazaha: Berceuse traditionnelle,  Interpr&egrave;te: Patsy.&nbsp; <br>
  Page5: &nbsp;Ry toera-manirery: Auteur-compositeur: Naly  Rakotofiringa. Interpr&egrave;te:Lilie. <br>
  Page6: Ny vady: Auteur: Georges Andriamanantena, Compositeur: Naly  Rakotofiringa, Interpr&egrave;tes: Jeanne et Naly Rakotofiringa. <br>
  Page7: Vorompotsy miaradia: Auteur-Compositeur: Therack,  Interpr&egrave;te: Solika.&nbsp; <br>
  Page8: Sompatra: chanson traditionnelle interpr&eacute;t&eacute;e par Barijaona  et Odette Suzannah&nbsp; <br>
  Page9: Revin-gadrako: Auteur-Compositeur-Interpr&egrave;te: Mamih  Bastah&nbsp; <br>
  Page10: Voahirana Auteur-compositeur: Henri Ratsimbazafy  Interpr&eacute;t&eacute; au piano par Sammy Rakotoarimalala.&nbsp; <br>
  Page11: Mba tsiditsidio: Auteur-Compositeur: Naly Rakotofiringa,  Interpr&egrave;te: Salomon. <br>
  Page12: Mazava antsinana: chant traditionnel des Hauts Plateaux,  version instrumentale. <br>
  Page13: Diavolana fenomanana takariva: chant traditionnel des  Hauts Plateaux, version instrumentale. <br>
  Page14: Ilay takariva kely: Compositeur: Justin Rajoro,  Interpr&egrave;te: Sakelindalana.&nbsp; <br>
  Page15: Ianao par Dama, Dadah &quot;Mahaleo&quot;.&nbsp; <br>
  Page16: Veloma ry fahazazana: Mahaleo.&nbsp; <br>
  Page17: Arosoy ny lakantsika, razandry par Sakelindalana <br>
  Page18: Mandihiza ra-hitsikitsika, chant traditionnel interpr&eacute;t&eacute;  par l'Ensemble Laka. <br>
  Page19: Gasikarako, Auteur-Compositeur-Interpr&egrave;te:  TeloFangady&nbsp; <br>
  Page20: Ombintsaretin-dRaleva: Auteur-Compositeur: Ernest Randria,  Interpr&egrave;te: Lova Mamisoa.&nbsp; <br>
  Page21: Ny lambanao mikopakopaka: Auteur-Compositeur: Henri  Ratsimbazafy, Interpr&egrave;tes: Voahirana et Henri Ratsimbazafy.&nbsp; <br>
  Page22: Izahay sy i Malala (valiha) Auteur-Compositeur: Sylvestre  Randafison, Interpr&egrave;te: Ny Antsaly,&nbsp; <br>
  Page23: Raha ny rano nantsakaina: Auteur-Compositeur: Rasamy  Gitara, Interpr&egrave;te: Solika.&nbsp; <br>
  Page24: 'Ndreto izahay, Auteur-Compositeur: Justin Rajoro,  Interpr&eacute;t&eacute; au piano par Sammy Rakotoarimalala.&nbsp; <br>
  Page25 Fa tsarotsaroko ny andro taloha: Feo Gasy. <br>
  Page26: Diavolana Fenomanana takariva, Ensemble Laka. <br>
  Page27: Mpivarodamba, mpivarokena: Auteur-Compositeur: Ramilson  Besigara, Interpr&egrave;te: Randratelo. <br>
  Page28: Ilay vohitra manga: Compositeur: Naly Rakotofiringa,  Interpr&egrave;te: Ludger Andrianjaka. <br>
  Page29: Lalatiana, Interpr&egrave;te: Ny Voninavoko. <br>
  Page30: Fihavanana Malagasy,: Feo Gasy. <br>
  Page31: Vorombe tsaradia, Interpr&egrave;te: Eric Manana. <br>
  Page32: Manimanina, Compositeur: Sylvestre Randafison, Interpr&egrave;te:  Ny Antsaly. <br>
  Page33: Ny voamaintilany, Auteur-Compositeur: Justin Rajoro,  interpr&eacute;t&eacute; au piano par Sammy Rakotoarimalala. <br>
  Page34: Aiza ho aiza re izay dadako izay, Auteur-Compositeur  Therack, Interpr&egrave;te: Ritsoka. <br>
  Page35: Veloma re, ry Said Omara, Interpr&egrave;tes: Marguerite et  Razanatsoa. <br>
  Page36: Nahaona no mitomany, Auteur-Compositeur-Interpr&egrave;te: Z&eacute;z&eacute;  Mahanoro. <br>
  Page37: Hody Aho, Compos&eacute; et interpr&eacute;t&eacute; par GLR (Groupe Laurent  Rakotomamonjy). <br>
  Page38: Ekeko satria, interpr&eacute;t&eacute; par Gaby et Faniry. <br>
  Page39: Nosy Madagasikarako, interpr&eacute;t&eacute; par le Groupe de Gilbert  Ramiarison. <br>
  Page40: O, ry fody an'ala &ocirc;: Feo Gasy. <br>
  Page41: Ny eo akaikinao, sombin'aiko, interpr&eacute;t&eacute; par Solika. <br>
  Page42: Ombintsaretin-dRaleva, Auteur-Compositeur: Ernest Randria,  Interpr&egrave;te: Lova Mamisoa. <br>
  Page43: Tsiriry ahitra, tsiriry vorona par Ny Voninavoko. <br>
  Page44: Totoy tsara ny varinareo: chant traditionnel interpr&eacute;t&eacute;  par Ny Antsaly. <br>
  Page45: Rehefa maty aho, Auteur: Jean Verdi Salomon Razakandraina  &quot;Dox&quot;, Compositeur: Jasmine Ratsimisata, Interpr&egrave;tes: Rakoto Frah et  Feo Gasy. </p>
<p>-------------------------------------------------- </p>
<p>Page com/rakotobert.html : Voahirana o,  Auteur-Compositeur-Interpr&egrave;te: Henri Ratsimbazafy.&nbsp; <br>
  Page com/sary_clarice_rafarasoa.php : Ny vady, Naly et Jeanne  Rakotofiringa. <br>
  Page com/Joseph_Ravalison.php : Nahaona no mitomany,  Auteur-Compositeur-Interpr&egrave;te: Z&eacute;z&eacute; Mahanoro.&nbsp; <br>
  Page com/izy_7_mianadahy_Andriamanana.php : Raozy manambaka raozy,  Compositeur: Freddy Ranarison&nbsp; <br>
  Page com/sary_justin_razaka.php : Tsy mba misy tsy ho diso,  Auteur-Compositeur: Justin Rajoro, Interpr&egrave;te: Solika.&nbsp; <br>
  Page com/sary_justine_ratsarabe.php : Babako roa,  Auteur-Compositeur: Andrianary Ratianarivo, Interpr&egrave;te: Solika. <br>
  Page com/sary_henriette.php : Fikasana, Auteur-Compositeur: Naly  Rakotofiringa, Interpr&egrave;te: Ando Rakotoarisoa.&nbsp; <br>
  Page com/Taranaka_Juliette_Ratsiorimisa.php : Indreto izahay:  Auteur-Compositeur: Justin Rajoro, interpr&eacute;t&eacute; ici au piano par Sammy  Rakotoarimalala <br>
  Page com/FamilleRavalinisaRakotondravohitra.php : Ho tsarovantsika  eto, Interpr&egrave;te: Nada Miangola.&nbsp; <br>
  Page com/sary_gabriel_andriantseheno.php : Izany ny fomban'ny  tany. <br>
  Page com/sary_juliette_ratsiorimisa_mianakavy.php : Gasikarako,  Interpr&egrave;te: TeloFangady. <br>
  Page com/bert_rakoto_sy_marguerite_razafindratavy.php: Aiza ho  aiza izay dadako izay. <br>
  Page com/fianakaviana_paul_ranaivo.php, Aiza ho aiza izay dadako  izay, Ritsoka. <br>
  Page com/Louis_Rakotovao_Inge_Lore_urbach.php : Ny lanitra  mangamanga, Ny Antsaly,&nbsp; <br>
  Page com/sary_guy.php : Nandao anao ela Raiko, Cantique protestant  chant&eacute; par la Chorale du Groupe de Jeunes de FPMA Lille. </p>
<p>------------------------------------------------------------ </p>
<p><strong><u>Toutes les chansons par  ordre alphab&eacute;tique / All the songs in alphabetical order</u></strong></p>
<p><strong>Afindrafindrao,&nbsp;</strong>chant traditionnel et  danse des Hautes Terres Malagasy, interpr&eacute;t&eacute; ici par Monique Rabaraona (Les  Surfs)&nbsp; <br>
  <strong>Aiza ho aiza re izay  dadako izay&nbsp;</strong>Auteur-Compositeur: Therack, Interpr&egrave;te: Ritsoka. (Aucune vid&eacute;o  trouv&eacute;e sur internet). <br>
  <strong>Arosoy ny lakan-tsika,  Razandry </strong>Sakelindalana <br>
  <strong>Arotsahy dieny izao </strong>Feo Gasy <br>
  <strong><br>
  </strong>------------------------------------------------------- <br>
  <strong>Bako&nbsp;</strong>Auteur-Compositeur:  Barijaona, Interpr&egrave;tes: Barijaona et Odette Suzannah<strong>.&nbsp;</strong> <br>
  <strong>Bakobako  roa</strong>, Auteur-Compositeur: Andrianary Ratianarivo, Interpr&egrave;te:  Solika.&nbsp; <br>
  ---------------------------------------------------------&nbsp; <br>
  <strong>Diavolana  fenomanana takariva</strong>: chanson traditionnelle des Hauts Plateaux, interpr&eacute;t&eacute; par  Freddy Randria + par l'Ensemble Laka <br>
  ---------------------------------------------------------- <br>
  <strong>Ekeko satria: </strong>Gaby et Faniry<strong>&nbsp;</strong> <br>
  <strong>Esory  re, ry Raiko:</strong>&nbsp;Auteur-Compositeur: Simplice Razafindrazaka, Interpr&egrave;te:  Fafah.&nbsp; <br>
  ---------------------------------------------------------- <br>
  <strong>Falifaly:</strong>&nbsp;Ny  Antsaly&nbsp; <br>
  <strong>Fa  tsarotsaroko </strong>Feo Gasy <br>
  <strong>Fihavanana  Malagasy</strong>: Feo Gasy&nbsp; <br>
  <strong>Fikasana:</strong>&nbsp;Auteur-Compositeur:  Naly Rakotofiringa, Interpr&egrave;te: Ando Rakotoarisoa.&nbsp; <br>
  ---------------------------------------------------------- <br>
  <strong>Gasikarako:&nbsp;</strong>Auteur-Compositeur-Interpr&egrave;te:  TeloFangady&nbsp; <br>
  ---------------------------------------------------------- <br>
  <strong>Handao  ity tany mahazatra</strong>: Compositeur: Naka Rabemanantsoa. <br>
  <strong>Ho  tsarovantsika eto</strong>, Auteur: Samuel Rahamefy, Compositeur: Justin Rajoro,  Interpr&egrave;te: Nada Miangola . <br>
  <strong>Hody  Aho</strong>: Groupe Laurent Rakotomamonjy (GLR) . <br>
  ---------------------------------------------------------- <br>
  <strong>Ianao  no nofidiko</strong>: Dama, Dadah &quot;Mahaleo&quot;. <br>
  <strong>Ikala  manja</strong>: Feo Gasy.&nbsp; <br>
  <strong>Ilay  takariva kely</strong>: Compositeur: Justin Rajoro,&nbsp;Interpr&egrave;te: Sakelidalana.<br>
  <strong>Ilay  vohitra manga</strong>: Auteur: Bernard, Compositeur: Naly Rakotofiringa, Interpr&egrave;te:  Ludger Andrianjaka . <br>
  <strong>Indreto izahay:</strong>&nbsp;Auteur-Compositeur:  Justin Rajoro, Interpr&eacute;t&eacute; au piano pas Sammy Rakotoarimalala.&nbsp; <br>
  <strong>Indro tazako</strong>, Sakelindalana . <br>
  <strong>Injany  feom-boro-mikalo:</strong>&nbsp;Compositeur: Naka Rabemanantsoa, Interpr&egrave;te: ATAUM  Association Th&eacute;&acirc;trale &amp; Artistique de l'Universit&eacute; de Madagascar. <br>
  <strong>Iny  hono izy, Ravorombazaha:</strong>&nbsp;Berceuse traditionnelle, Interpr&egrave;te:  Patsy.&nbsp; <br>
  <strong>Izahay  sy i Malala (valiha)</strong>, Ny Antsaly . <br>
  ---------------------------------------------------&nbsp; <br>
  <strong>Lalatiana:</strong>&nbsp;interpr&eacute;t&eacute; par Ny  Voninavoko. <br>
  --------------------------------------------------- <br>
  <strong>Madagasikara</strong>,  Auteur-Compositeur-interpr&egrave;te: Naly Rakotofiringa,&nbsp; <br>
  <strong>Mama&nbsp;</strong>,  Auteur-Compositeur-interpr&egrave;te: Ramahafadrahona.&nbsp; <br>
  <strong>Mandihiza  Ra-hitsikitsika</strong>, chant traditionnel des Hauts Plateaux interpr&eacute;t&eacute; par  l'Ensemble Laka. <br>
  <strong>Manimanina</strong>&nbsp;Ny  Antsaly. <br>
  <strong>Mazava  antsinana</strong>: chant traditionnel des Hauts Plateaux . <br>
  <strong>Mba  tsiditsidio</strong>: Auteur-Compositeur: Naly Rakotofiringa, Interpr&egrave;te:  Salomon.&nbsp; <br>
  <strong>Mpivarodamba sy  mpivarokena</strong>:  &nbsp;Auteur-compositeur: Ramilson Besigara, Interpr&egrave;te: Randratelo.&nbsp; <br>
  ---------------------------------------------------- <br>
  <strong>Nahaona  no mitomany</strong>, Auteur-Compositeur-Interpr&egrave;te: Z&eacute;z&eacute; Mahanoro.&nbsp;&nbsp; <br>
  <strong>Nandao  anao ela Raiko</strong>, Cantique protestant N°417, Auteur-Compositeur: Rajoely J&eacute;r&ocirc;me,  Interpr&egrave;te: la Chorale du Groupe de Jeunes de FPMA Lille.&nbsp; <br>
  <strong>Nosy  Madagasikara</strong>, Gilbert Ramiarison. <br>
  <strong>Ny eo  akaikinao, sombinaina&nbsp;</strong>Auteur-Compositeur: Therack<strong>.</strong>&nbsp; <br>
  <strong>Ny  lambanao mikopakopaka</strong>: Auteur-Compositeur: Henri Ratsimbazafy, Interpr&egrave;tes: Voahirana  et Henri Ratsimbazafy.&nbsp; <br>
  <strong>Ny  lanitra mangamanga</strong>, Auteur-Compositeur: Sylvestre Randafison, Interpr&egrave;te: Ny  Antsaly.&nbsp; <br>
  <strong>Ny vady</strong>:  Auteur: Georges Andriamanantena, Compositeur: Naly Rakotofiringa, Interpr&egrave;tes:  Jeanne et Naly Rakotofiringa. <br>
  <strong>Ny  voamaintilany:</strong>&nbsp;Auteur-Compositeur: Justin Rajoro, interpr&eacute;t&eacute; au piano par  Sammy Rakotoarimalala . <br>
  ------------------------------------------------------- <br>
  <strong>Ombintsaretin-dRaleva</strong>,  Auteur-Compositeur: Ernest Randria, Interpr&egrave;tes: Patsy&nbsp;&nbsp;ainsi que  Lova Mamisoa . <br>
  <strong>O, ry  Fody an'ala</strong>: Feo Gasy . <br>
  ------------------------------------------------------ <br>
  <strong>Raha ny  rano nantsakaina</strong>: Auteur-Compositeur: Rasamy Gitara, Interpr&egrave;te: Solika. <br>
  <strong>Raozy  manambaka raozy, Compositeur Fredy Ranaeison.</strong> <br>
  <strong>Rehefa  maty aho Auteur:</strong> Dox Razakandraina,Compositeur:Jasmine Ratsimisata, Interpr&egrave;tes:  Rakoto Frah et Feo Gasy. <br>
  <strong>Revin-gadrako</strong>:  Auteur-Compositeur-interpr&egrave;te: Mamih Basta.&nbsp; <br>
  <strong>R'ilay otrikafon'ny foko</strong>, Compositeur: Therack, <br>
  <strong>Ry toera-manirery</strong>, Auteur-Compositeur:  Naly Rakotofiringa, Interpr&egrave;te: Lilie&nbsp; <br>
  ------------------------------------------------------------ <br>
  <strong>Sompatra</strong>:  chanson traditionnelle interpr&eacute;t&eacute;e par Barijaona et Odette Suzannah. <br>
  ------------------------------------------------------------ <br>
  <strong>Tambazako:&nbsp;</strong>Eric  Manana.&nbsp; <br>
  <strong>Tonga vahiny ianareo</strong>&nbsp;:  Interpr&egrave;tes: Solika. <br>
  <strong>Totoy tsara ny varinareo</strong>: Ny  Antsaly.&nbsp; <br>
  <strong>Tsy mba misy tsy ho diso</strong>,  Auteur-Compositeur: Justin Rajoro, Interpr&egrave;te: Solika.&nbsp;&nbsp; <br>
  <strong>Tsiriry ahitra hono e, tsiriry  vorona</strong>: Troupe Voninavoko. <br>
  ----------------------------------------------------------- <br>
  <strong>Veloma  re, ry dada havako</strong>: Compositeur: Therack, Interpr&egrave;te: Solika.&nbsp; <br>
  <strong>Veloma re, ry Saidomara: </strong>interpr&egrave;tes: Marguerite et Razanatsoa. <br>
  <strong>Veloma ry fahazazana</strong>:  Mahaleo.&nbsp; <br>
  <strong>Voahirana o</strong>,  Auteur-Compositeur-Interpr&egrave;te: Henri Ratsimbazafy.&nbsp; <br>
  <strong>Vorombe tsaradia</strong>: Eric Manana. <br>
  <strong>Vorompotsy  miaradia</strong>: Auteur-Compositeur: Therack, Interpr&egrave;te: Solika.&nbsp; <br>
  <strong>Vorona &ocirc;&nbsp;</strong>: Auteur-Compositeur:  Barijaona, Interpr&egrave;tes: Barijaona et Odette Suzannah. </p>
  
  </div>
