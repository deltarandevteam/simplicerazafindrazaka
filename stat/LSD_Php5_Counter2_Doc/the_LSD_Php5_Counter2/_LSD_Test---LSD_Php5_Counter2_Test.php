<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<!-- template designed by Marco Von Ballmoos -->
			<title>Docs for page LSD_Php5_Counter2_Test.php</title>
			<link rel="stylesheet" href="../media/stylesheet.css" />
			<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'/>
		</head>
		<body>
			<div class="page-body">			
<h2 class="file-name">/LSD_Test/LSD_Php5_Counter2_Test.php</h2>

<a name="sec-description"></a>
<div class="info-box">
	<div class="info-box-title">Description</div>
	<div class="nav-bar">
					<span class="disabled">Description</span> |
									<a href="#sec-includes">Includes</a>
												</div>
	<div class="info-box-body">	
		<!-- ========== Info from phpDoc block ========= -->
<p class="short-description">Page PHP de test de production du compteur de visites LSD_Php5_Counter2.
 PHP version 5
 LICENSE: Ce script vous est gracieusement offert par The Liberated Seven Dwarfs et est libre de tout droit d'utilisation priv�e et non commerciale sous la restriction de conserver le pr�sent ent�te et de respecter la licence Creative Commons : By-Nc-Sa.</p>
<p class="description"><p>Page PHP de test de production du compteur de visites LSD_Php5_Counter2.php
 PHP version 5
 LICENSE: Ce script vous est gracieusement offert par The Liberated Seven Dwarfs et est libre de tout droit d'utilisation priv�e et non commerciale sous la restriction de conserver le pr�sent ent�te et de respecter la licence Creative Commons : By-Nc-Sa.</p></p>
	<ul class="tags">
				<li><span class="field">author:</span> The Liberated Seven Dwarfs</li>
				<li><span class="field">version:</span> 2.0.0.a - February 8, 2009</li>
				<li><span class="field">copyright:</span> 2008-2009 Advanced Software Solutions Inc.</li>
				<li><span class="field">link:</span> <a href="http://theliberated7dwarfs.as2.com">http://theliberated7dwarfs.as2.com</a></li>
				<li><span class="field">filesource:</span> <a href="../__filesource/fsource_the_LSD_Php5_Counter2__LSD_TestLSD_Php5_Counter2_Test.php.html">Source Code for this file</a></li>
				<li><span class="field">license:</span> Creative-Commons_By-Nc-Sa</li>
			</ul>
		
			</div>
</div>
		

	<a name="sec-includes"></a>	
	<div class="info-box">
		<div class="info-box-title">Includes</div>
		<div class="nav-bar">
			<a href="#sec-description">Description</a> |
						<span class="disabled">Includes</span>
														</div>
		<div class="info-box-body">	
			<a name="___/scripts/php/LSD_Php5_Counter2/LSD_Php5_Counter2_php"><!-- --></a>
<div class="evenrow">
	
	<div>
		<span class="include-title">
			<span class="include-type">require_once</span>
			(<span class="include-name"><a href="../the_LSD_Php5_Counter2/_scripts---php---LSD_Php5_Counter2---LSD_Php5_Counter2.php.html">"../scripts/php/LSD_Php5_Counter2/LSD_Php5_Counter2.php"</a></span>)
			(line <span class="line-number">21</span>)
		</span>
	</div>

	<!-- ========== Info from phpDoc block ========= -->
	
</div>
		</div>
	</div>
	
	
	
	
	<p class="notes" id="credit">
		Documentation generated on Thu, 12 Feb 2009 17:27:05 +0100 by <a href="http://www.phpdoc.org" target="_blank">phpDocumentor 1.4.0</a>
	</p>
	</div></body>
</html>