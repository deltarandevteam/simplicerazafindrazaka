<?php
   /**
    * Page PHP de test de production du compteur de visites LSD_Php5_Counter2.php
    *
    * PHP version 5
    *
    * LICENSE: Ce script vous est gracieusement offert par The Liberated Seven Dwarfs et est libre de tout droit
    * d'utilisation priv�e et non commerciale sous la restriction de conserver le pr�sent ent�te et de respecter
    * la licence Creative Commons : By-Nc-Sa.
    *
    * @category Web utilities
    * @package the_LSD_Php5_Counter2
    * @author The Liberated Seven Dwarfs
    * @copyright 2008-2009 Advanced Software Solutions Inc.
    * @license Creative-Commons_By-Nc-Sa - http://creativecommons.org/licenses/by-nc-sa/2.0/fr/
    * @link http://theliberated7dwarfs.as2.com
    * @version 2.0.0.a - February 8, 2009
    * @filesource LSD_Php5_Counter2_Demo.php
    */
   session_start();
   $over_Root="../";
   $script_Path="scripts/php/LSD_Php5_Counter2/";
   $script_Name="LSD_Php5_Counter2";
   $script_Ext="php";
   $script=$over_Root.$script_Path.$script_Name.'.'.$script_Ext;
   require_once ($script);
   $my_Counter=new LSD_Php5_Counter2($over_Root,$script_Path,$config_Name);
   $my_Counter->set_LSD_Php5_Counter2_Debug_Mode(true);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv = "Content-Type" content = "text/html; charset = iso-8859-1" />
      <meta name="Author" content="The Liberated Seven Dwarfs" />
      <meta name="keywords" content = "AS2, php, mysql, javascript, free, download" />
      <title>LSD Php5 Counter2 Demo Page</title>
      <link href="../css/LSD_Php5_Counter2_Demo.css" rel="stylesheet" type="text/css"/>
</head>
   <body class="Fixed_Center">
      <div id="container" align="center">
         <br/>
         <table cellspacing="0" border="1px" bgcolor="#CCCCCC" width="1100">
            <tr>
               <td align="center">
                  &nbsp;
                  <span class="Style1">
                     Compteur de visites (hit) multi-occurences param&eacute;trable
                  </span>
               </td>
               <td align="center">
                  <img src="../pictures/gif/my_php.gif" title="Script PHP" alt="Script PHP" />
               </td>
            </tr>
            <tr>
               <td align="left">
               &nbsp;1 ) Avec le fichier de configuration default_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("default_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;2 ) Avec le fichier de configuration simple_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("simple_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;3 ) Avec le fichier de configuration simpleborder_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("simpleborder_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;4 ) Avec le fichier de configuration simplepaded_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("simplepaded_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;5 ) Avec le fichier de configuration simplepadedborder_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("simplepadedborder_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;6 ) Avec le fichier de configuration small_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("small_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;7 ) Avec le fichier de configuration large_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("large_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;8 ) Avec le fichier de configuration value_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("value_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;9 ) Avec le fichier de configuration string_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("string_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;10 ) Avec le fichier de configuration verbose_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("verbose_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;11 ) Avec le fichier de configuration script1_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("script1_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;12 ) Avec le fichier de configuration script2_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("script2_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;13 ) Avec le fichier de configuration hidden_Counter2, <strong>le compteur n'est pas affich&eacute;</strong> mais il compte bien les visites!               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("hidden_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;14 ) Avec le fichier de configuration blue_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("blue_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;15 ) Avec le fichier de configuration green_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("green_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;16 ) Avec le fichier de configuration yellow_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("yellow_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;17 ) Avec le fichier de configuration red_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("red_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;18 ) Avec le fichier de configuration white_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("white_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;19 ) Avec le fichier de configuration pool_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("pool_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
            <tr>
               <td align="left">
                  &nbsp;20 ) Avec le fichier de configuration tarot_Counter2, le compteur est affich&eacute; comme &ccedil;a :
               </td>
               <td align="center">
                  <?php
                     $my_Counter->set_LSD_Php5_Counter2_Config_Name("tarot_Counter2");
                     $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                  ?>
               </td>
            </tr>
         </table>
      </div>
   </body>
</html>
