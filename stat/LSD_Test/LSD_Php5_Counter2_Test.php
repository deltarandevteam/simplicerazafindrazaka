<?php
   /**
    * Page PHP de test de production du compteur de visites LSD_Php5_Counter2.php
    *
    * PHP version 5
    *
    * LICENSE: Ce script vous est gracieusement offert par The Liberated Seven Dwarfs et est libre de tout droit
    * d'utilisation priv�e et non commerciale sous la restriction de conserver le pr�sent ent�te et de respecter
    * la licence Creative Commons : By-Nc-Sa.
    *
    * @category Web utilities
    * @package the_LSD_Php5_Counter2
    * @author The Liberated Seven Dwarfs
    * @copyright 2008-2009 Advanced Software Solutions Inc.
    * @license Creative-Commons_By-Nc-Sa - http://creativecommons.org/licenses/by-nc-sa/2.0/fr/
    * @link http://theliberated7dwarfs.as2.com
    * @version 2.0.0.a - February 8, 2009
    * @filesource LSD_Php5_Counter2_Test.php
    */
   session_start();
   @require_once("../scripts/php/LSD_Php5_Counter2/LSD_Php5_Counter2.php");
   @$my_Counter2=new LSD_Php5_Counter2("../","scripts/php/LSD_Php5_Counter2/","default_Counter2");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1"/>
      <meta name="Author" content="The Liberated Seven Dwarfs"/>
      <meta name="keywords" content="AS2, Liberated, Seven, Dwarfs, script, php, mysql, javascript, ajax, free, download"/>
      <title>LSD Php5 Counter2 Test Page</title>
      <link href="../css/LSD_Php5_Counter2_Test.css" rel="stylesheet" type="text/css"/>
   </head>
   <body class="Fixed_Centered">
      <div id="container">
         <br/>
         &nbsp;Le compteur par d&eacute;faut est affich&eacute; comme cela :&nbsp;
         <?php 
            @$my_Counter2->get_LSD_Php5_Counter2_Hit_Result();
         ?>
      </div>
   </body>
</html>
