<?php
   /**
    * Page PHP de test document�e du compteur de visites LSD_Php5_Counter2.php
    *
    * PHP version 5
    *
    * LICENSE: Ce script vous est gracieusement offert par The Liberated Seven Dwarfs et est libre de tout droit
    * d'utilisation priv�e et non commerciale sous la restriction de conserver le pr�sent ent�te et de respecter
    * la licence Creative Commons : By-Nc-Sa.
    *
    * @category Web utilities
    * @package the_LSD_Php5_Counter2
    * @author The Liberated Seven Dwarfs
    * @copyright 2008-2009 Advanced Software Solutions Inc.
    * @license Creative-Commons_By-Nc-Sa - http://creativecommons.org/licenses/by-nc-sa/2.0/fr/
    * @link http://theliberated7dwarfs.as2.com
    * @version 2.0.0.a - February 8, 2009
   * @filesource documented_LSD_Php5_Counter2_Test.php
   */

   /**
    * Cette page doit �tre plac�e dans un sous-r�pertoire de premier niveau par rapport � la racine du site.
    * Elle affiche un compteur en fonction des param�tres d�finis dans le fichier de configuration default_Counter2.inc
    */

   /**
    * La fonction "session_start()" d�marre une session n�cessaire au script pour ne pas comptabiliser plus d'une
    * fois une m�me page qui serait rafraichie plusieurs fois lors de la m�me session.
    */
   session_start();

   /**
    * La variable $error est un message qui sera affich� si l'objet compteur rencontre une erreur lors de
    * son ex�cution. Elle est initialis�e � vide pour �viter un message de type Warning dans Zend Studio for Eclipse.
    * @var string $error
    */
   $error="";

   /**
    * La variable $over_Root permet d'indiquer une surcharge � appliquer aux diff�rents chemins d�finis dans le
    * fichier de configuration.
    * Cette variable est utilis�e lorsque le script est appel� dans une page plac�e d'un sous-r�pertoire de la racine
    * du site.
    * REMARQUE : Il n'est pas obligatoire de d�finir cette variable si la page qui app�le le script est situ�e � la
    * racine du site, la valeur assum�e par d�faut par l'objet compteur �tant vide ("").
    * Cepndant, si cette page est plac�e dans un sous-r�pertoire de premier niveau par rapport � la racine du site,
    * la variable $over_Root doit �tre d�finie de la mani�re suivante : ($over_Root="../";) et si cette page est
    * plac�e dans un sous-r�pertoire de second niveau par rapport � la racine du site, la variable $over_Root doit
    * �tre d�finie de la mani�re suivante : ($over_Root="../../";), et ainsi de suite.
    * @var string $over_Root
    */
   $over_Root="../";

   /**
    * La variable $script_Path indique le chemin absolu du r�pertoire contenant le fichier script par rapport � la
    * racine du site.
    * Par d�faut, ce r�pertoire est "scripts/php/LSD_Counter/".
    * REMARQUE : Ne pas oublier le signe "/" � la fin du chemin.
    * @var string $script_Path
    */
   $script_Path="scripts/php/LSD_Php5_Counter2/";

   /**
    * la variable $script_Name indique le nom du fichier script. Par d�faut, ce nom est LSD_Php5_Counter2
    * @var string $script_Name
    */
   $script_Name="LSD_Php5_Counter2";

   /**
    * la variable $script_Ext indique l'extension du fichier script. Par d�faut, cette extension est "php".
    * @var string $script_Ext
    */
   $script_Ext="php";

   /**
    * La variable $config_Name indique le nom du fichier de configuration.
    * L'extention du nom du fichier de configuration ".inc" est assum�e par le script.
    * Si le fichier de configuration n'est pas trouv�, le script retournera soit un compteur par d�faut en mode texte
    * minimal dont les visites seront comptabilis�es dans le fichier "the_Default_LSD_Counter.nbr", soit un message
    * d'erreur si la m�thode $my_Counter->set_LSD_Php_Counter2_Debug_Mode(true) a �t� utilis�e juste avant la m�thode
    * $my_Counter->get_LSD_Php_Counter()
    * @var string $config_Name_Result
    */
   $config_Name="default_Counter2";

   /**
    * La variable $script indique le nom complet (chemin + nom + extension) du fichier script.
    * Elle est initialis�e � vide pour �viter un message de type Warning dans Zend Studio for Eclipse.
    * @var string $script
    */
   $script="";
   

   /**
    * Fonction qui instancie l'objet compteur et qui renvoie le r�sultat de cet objet.
    *
    * @param string $over_Root
    * @param string $script_Path
    * @param string $script_Name
    * @param string $script_Ext
    * @param string $config_Name
    * @global string $script
    * @var string $error
    * @return - soit un compteur, soit un message d'erreur si la m�thode set_LSD_Php5_Counter_Debug_Mode()
    * a �t� initialis�e avec la valeur "true" avant l'appel de la m�thode get_LSD_Php5_Counter_Result().
    */
   function start_the_LSD_Php5_Counter2( $over_Root, 
                                         $script_Path,
                                         $script_Name,
                                         $script_Ext,
                                         $config_Name
                                        )
      {
         /**
          * La variable $script est d�clar�e globale pour �tre accessible � l'ext�rieur de la fonction
          * @global string $script
          */
         global $script;

         /**
          * La variable $not_Found est un message d'erreur local � la fonction start_the_LSD_Php5_Counter2(...)
          * qui sera renvoy�e par la fonction si le fichier script n'est pas trouv�.
          * Elle est initialis�e � vide.
          * @var string $not_Found
          */
         $not_Found="";
   
         /**
          * La variable globale $script est la concat�nation du chemin et du nom du fichier script
          * @global string $script
          */
         $script=$over_Root.$script_Path.$script_Name.'.'.$script_Ext;
         
         /**
          * Si le fichier script est trouv�,
          */
         if(file_exists($script))
            {
               /**
                * alors charger le fichier script,
                */
               require_once($script);

               /**
                * puis instancier l'objet compteur avec les trois param�tres indispensables suivants :
                * - $s_Root,
                * - $script_Path
                * - et $config_Name
                * @var object $my_LSD_Counter
                * @method LSD_Php_Counter($over_Root,$script_Path,$config_Name)
                * @param string $over_Root
                * @param string $script_Path
                * @param string $config_Name
                */
               $my_LSD_Php5_Counter2=new LSD_Php5_Counter2( $over_Root,
                                                            $script_Path,
                                                            $config_Name
                                                          );

               /**
                * et renvoyer de r�sultat de l'objet compteur.
                */
               return($my_LSD_Php5_Counter2);
            }
         /**
          * sinon (donc le fichier de script n'a pas �t� trouv�),
          */
         else
            {
               /**
                * alors, construire le message suivant :
                */
               $not_Found="Erreur : le fichier script : ".$script." est introuvable!";
               
               /**
                * puis renvoyer le message d'erreur.
                */
               return($not_Found);
            }
      }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
      <meta name="Author" content="The Liberated Seven Dwarfs"/>
      <meta name="keywords" content="AS2, Liberated, Seven, Dwarfs, script, php, mysql, javascript, ajax, free, download"/>
      <title>LSD Php5 Counter2 Documented Test Page</title>
      <link href="../css/documented_LSD_Php5_Counter2_Test.css" rel="stylesheet" type="text/css"/>
   </head>
   <body class="Fixed_Center">
      <div id="container">
         <br />
         <table align="center" border="1px">
            <tr>
		         <td style="text-align: center" align="left"  width="600px">
                  <span class="Style1">
                     Page de test document&eacute;e du compteur de visites
                     <br />
		               LSD_Php5_Counter2.php
                  </span>
                </td>
		         <td style="text-align: center" align="right" width="400px">
                  <img src="../pictures/gif/my_php.gif" title="Script PHP" alt="Script PHP"/>
               </td>
	        </tr>
	        <tr>
		        <td>
                 <?php
                    /**
                     * La variable $my_Counter re�oit le r�sultat de l'objet compteur renvoy� par la fonction
                     * start_the_LSD_Counter2($s_Root,$script_Path,$script_Name,$script_Ext,$config_Name).
                     * @var string $my_Counter2
                     */
                    $my_Counter=start_the_LSD_Php5_Counter2( $over_Root,
                                                             $script_Path,
                                                             $script_Name,
                                                             $script_Ext,
                                                             $config_Name
                                                           );
                    /**
                     * Si la variable $my_Counter a re�u le message d'erreur de la fonction
                     * start_the_LSD_Counter1($over_Root,$script_Path,$script_Name,$script_Ext,$config_Name),
                     */
                    if($my_Counter == "Erreur : le fichier script : ".$script." est introuvable!")
                        {
                            /**
                             * alors, afficher le message d'erreur de la fonction "start_the_LSD_Counter2(...).
                             */
                            echo $my_Counter;
                        }
                    /**
                     * sinon (donc la variable $my_Counter n'a pas re�u le message d'erreur de la fonction
                     * start_the_LSD_Php5_Counter2(...))
                     */
                    else
                        {
                        /**
                             * La m�thode set_LSD_Php5_Counter_Debug_Mode() initialise le mode mise au point.
                             * REMARQUE : Le param�tre "true" active le mode mise au point de l'objet compteur et
                             * le para�tre "false" le d�sactive.
                             * @method $my_Counter->set_LSD_Counter_Debug_Mode()
                             * @param boolean
                             * @return boolean
                             */
                            $my_Counter->set_LSD_Php5_Counter2_Debug_Mode(true);

                            /**
                             * La variable $error re�oit le r�sultat de la m�thode get_LSD_Php5_Counter2_Error().
                             * @var string $error
                             * @method $my_Counter->get_LSD_Counter_Error()
                             * @return string
                             */
                            $error=$my_Counter->get_LSD_Php5_Counter2_Error_Status();

                            /**
                             * Si la variable $error re�oit le mot "None" de la m�thode get_LSD_Php5_Counter2_Error(),
                             * (donc il n'y a pas eu d'erreur lors de l'ex�cution du script),
                             */
                            if($error == "None")
                                {
                                    /**
                                     * alors, afficher le message suivant :
                                     */
                                    echo "Avec le fichier de configuration : \"<B>".$script_Path."config/".$config_Name.
                                         ".inc</B>\",<br /> le compteur sera affich� comme cela :";
                                }
                        }
                 ?>
              </td>
              <td style="text-align: center">
                 <?php
                    /**
                     * Si la variable $my_Counter n'a pas re�u le message d'erreur de la fonction
                     * start_the_LSD_Php5_Counter2(...)
                     * et n'a pas la valeur "null",
                     */
                    if(($my_Counter != "Erreur : le fichier script : ".$script." est introuvable!") && ($my_Counter != null))
                        {
                            /**
                             * alors lancer la m�thode du compteur $my_Counter->get_LSD_Php5_Counter2().
                             * @method $my_Counter->get_LSD_Php5_Counter2()
                             * @return string HTML
                             */
                            $my_Counter->get_LSD_Php5_Counter2_Hit_Result();
                        }
                    /**
                     * sinon (donc la variable $my_Counter a re�u le message d'erreur de la fonction
                     * start_the_LSD_Php5_Counter2(...),
                     */
                    else
                        {
                            /**
                             * alors, afficher le message suivant:
                             */
                            echo "Affichage d'un compteur impossible!";
                        }
                 ?>
              </td>
	        </tr>
        </table>
      </div>
   </body>
</html>
