<!-- // JavaScript Document

/**
 * Compteur de visites (hit) param�trable utilisable soit depuis une page PHP5 soit depuis une page HTML via les
 * deux scripts LSD_Mouse_Events2.js et LSD_Stuff2.js
 *
 * JavaScript
 *
 * LICENSE: Ce script vous est gracieusement offert par The Liberated Seven Dwarfs et est libre de tout droit
 * d'utilisation priv�e et non commerciale sous la restriction de conserver le pr�sent ent�te et de respecter
 * la licence Creative Commons : By-Nc-Sa.
 *
 * @category Web utilities
 * @package the_LSD_Php5_Counter2
 * @author The Liberated Seven Dwarfs
 * @copyright 2008-2009 Advanced Software Solutions Inc.
 * @license Creative-Commons_By-Nc-Sa - http://creativecommons.org/licenses/by-nc-sa/2.0/fr/
 * @link http://theliberated7dwarfs.as2.com
 * @version 2.0.0.a - January 10, 2009
 * @filesource LSD_Ajax_Stuff2.js
 */
 
/**
 * Ajax connector
 */
var net=new Object();
/**
 * Constants
 */
net.READY_STATE_UNINITIALIZED=0;
net.READY_STATE_LOADED=1;
net.READY_STATE_INTERACTIVE=2;
net.READY_STATE_COMPLETE=4;
/**
 * 
 */
net.ParamOnload=function(param)
   {
      if(param)
         {
            this.paramOnload=(param=='NULL')?null:param;
         }
      else
         {
            return this.paramOnload;
         }
   }
/**
 * Constructors
 */
net.ContentLoader=function(url,onload,onerror,method,params,contentType)
   {
      this.url=url;
      this.req=null;
      this.onload=onload;
      this.onerror=(onerror)?onerror:this.defaultError;
      this.paramOnLoad=net.ParamOnload();
      this.loadXMLdoc(url,method,params,contentType,this.paramOnload);
   }
/**
 *
 */
net.ContentLoader.prototype.loadXMLdoc=function(url,method,params,contentType)
   {
      if(!method)
         {
            method="GET";
         }
      if(!contentType&&method=="POST")
         {
            contentType="application/x-www-form-urlencoded";
         }
      if(window.XMLHttpRequest)
         {
            this.req=new XMLHttpRequest();
         }
      else
         if(window.ActiveObject)
            {
               this.req=new ActiveObject("Microsoft.XMLHTTP");
            }
         if(this.req)
            {
               try
                  {
                     var loader=this;
                     this.req.onreadystatechange=function()
                        {
                           loader.onReadyState.call(loader);
                        }
                     this.req.open(method,url,true);
                     if(contentType)
                        {
                           this.req.setRequestHeader("content-Type",contentType);
                        }
                     this.req.send(params);
                  }
               catch(err)
                  {
                     this.onerror.call(this);
                  }
            }
   }
/**
 *
 */
net.ContentLoader.prototype.onReadyState=function()
   {
      var req=this.req;
      var ready=req.readyState;
      if(ready==net.READY_STATE_COMPLETE)
         {
            var httpStatus=req.status;
            if(httpStatus==200||httpStatus==0)
               {
                  this.onload.call(this,this.paramOnLoad);
               }
            else
               {
                  this.onerror.call(this);
               }
         }
   }
/**
 *
 */
net.ContentLoader.prototype.defaultError=function()
   {
      alert("Erreur pour obtenir les donn�es : "+"\n readystate:"+this.req.readyState+"\n status :"+this.req.status+"\n headers:"+this.req.getAllResponseHeaders());
   }
/**
 * Short_cut Function
 * 
 * @param element
 * @return document.getElementById(element)
 */
function LSD_$(element)
   {
      var my_Element="";
      if(element)
         {
            my_Element=element;
         }
      return document.getElementById(my_Element);
   }
//
-->