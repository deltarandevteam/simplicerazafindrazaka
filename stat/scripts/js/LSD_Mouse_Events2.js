<!-- // JavaScript Document

/**
 * Compteur de visites (hit) param�trable utilisable soit depuis une page PHP5 soit depuis une page HTML via les
 * deux scripts LSD_Mouse_Events2.js et LSD_Stuff2.js
 *
 * JavaScript
 *
 * LICENSE: Ce script vous est gracieusement offert par The Liberated Seven Dwarfs et est libre de tout droit
 * d'utilisation priv�e et non commerciale sous la restriction de conserver le pr�sent ent�te et de respecter
 * la licence Creative Commons : By-Nc-Sa.
 *
 * @category Web utilities
 * @package the_LSD_Php5_Counter2
 * @author The Liberated Seven Dwarfs
 * @copyright 2008-2009 Advanced Software Solutions Inc.
 * @license Creative-Commons_By-Nc-Sa - http://creativecommons.org/licenses/by-nc-sa/2.0/fr/
 * @link http://theliberated7dwarfs.as2.com
 * @version 2.0.0.a - January 10, 2009
 * @filesource LSD_Mouse_Events2.js
 */

// LSD_Mouse_Events2.js
/**
 * @param string -
 *         url_Id
 * @param string -
 *         php5_Root
 * @param array -
 *         counter2_Params [Config] [Debug] [Mode] [Pad] [Read] [Hidden]
 *         [Session] [Call]
 * @param array -
 *         ajax_Params [Html_Id] [Form_Id] [Msg_Id] [Html_El]
 */
function LSD_Load_Php_Script(url_Id, rough_Response, php5_Root, counter2_Params, ajax_Params)
    {
//					alert("Coucou");
        var my_Url_Id = '';
        var js_Fct = "LSD_Download_File";
        var my_Php5_Load = 'AJAX';
        var my_Rough_Response = '';
        var my_Php5_Root = '';
        var my_Param = '';
        var my_Url_Post = '';
        var my_Url_Get = '';
        
        if(url_Id)
            {
                my_Url_Id = url_Id;
            }
        if(my_Url_Id)
            {
                if(ajax_Params && ajax_Params['html_Id'] || ajax_Params['html_El'] )
                    {
                        if(rough_Response)
                            {
                                my_Rough_Response = rough_Response;
                            }
                        if(php5_Root)
                            {
                                my_Php5_Root = php5_Root;
                            }
                        if(counter2_Params)
                            {
                                var my_Counter2_Config = '';
                                var my_Counter2_Debug = '';
                                var my_Counter2_Mode = '';
                                var my_Counter2_Pad = '';
                                var my_Counter2_Read = '';
                                var my_Counter2_Hidden = '';
                                var my_Counter2_Session = '';
                                var my_Js_Call = 'POST';
                                for (var key in counter2_Params)
                                    {
                                        switch(key)
                                            {
                                                case 'Config':
                                                    var counter2_Config = counter2_Params[key];
                                                    my_Counter2_Config = counter2_Config;
                                                    break;
                                               case 'Debug':
                                                    var counter2_Debug = counter2_Params[key];
                                                    if (counter2_Debug == 'DEBUG'.toUpperCase())
                                                        {
                                                            my_Counter2_Debug = counter2_Debug;
                                                        }
                                                    break;
                                                case 'Mode':
                                                    var counter2_Mode = counter2_Params[key];
                                                    my_Counter2_Mode = counter2_Mode;
                                                    break;
                                                case 'Pad':
                                                    var counter2_Pad = counter2_Params[key];
                                                    my_Counter2_Pad = counter2_Pad;
                                                    break;
                                                case 'Read':
                                                    var read_Mode = counter2_Params[key];
                                                    my_Counter2_Read = read_Mode;
                                                    break;
                                                case 'Hidden':
                                                    var counter2_Hidden = counter2_Params[key];
                                                    my_Counter2_Hidden = counter2_Hidden;
                                                    break;
                                                case 'Session':
                                                    var session_Id = counter2_Params[key];
                                                    my_Counter2_Session = session_Id;
                                                    break;
                                                case 'Call':
                                                    var Js_Call = counter2_Params[key].toUpperCase();
                                                    if (Js_Call == 'GET')
                                                        {
                                                             my_Js_Call = 'GET';
                                                        }
                                                    break;
                                                default:
                                                    break;
                                            }
                                    }
                            }
                        my_Param =  'LSD_Load='+my_Php5_Load+
                                    '&LSD_Rough='+my_Rough_Response+
                                    '&LSD_Root='+my_Php5_Root+
                                    '&LSD_Debug='+my_Counter2_Debug+
                                    '&LSD_Config='+my_Counter2_Config+
                                    '&LSD_Mode='+my_Counter2_Mode+
                                    '&LSD_Pad='+my_Counter2_Pad+
                                    '&LSD_Read='+my_Counter2_Read+
                                    '&LSD_Hidden='+my_Counter2_Hidden+
                                    '&LSD_Session='+my_Counter2_Session;
                        my_Url_Get = my_Url_Id + '?'+ my_Param;
                        my_Url_Post = my_Url_Id; 
                                var my_Js_Fct = eval(js_Fct);
                                if (my_Js_Call != 'POST')
                                    {
                                        LSD_Get_Ajax_Data(my_Url_Get,ajax_Params,my_Js_Fct);
                                    }
                                else
                                    {
                                        LSD_Post_Data(my_Url_Post,ajax_Params,my_Param,null,my_Js_Fct);
                                    }
                        if(ajax_Params['html_Id'] && LSD_$(ajax_Params['html_Id']))
                            {
                                LSD_$(ajax_Params['html_Id']).innerHTML='';
                                LSD_$(ajax_Params['html_Id']).style.display='block';
                            }
                    }
            }
    }
/**
 * @param array -
 *         params
 */
function LSD_Download_File(params)
    {
        if(params)
            {
                for( var key in params)
                    {
                        switch(key)
                            {
                                case 'html_Id':
                                    var my_Html_Id = params[key];
                                    break;
                                case 'form_Id':
                                    var my_Form_Id = params[key];
                                    break;
                                case 'msg_Id':
                                    var my_Msg_Id = params[key];
                                    break;
                                case 'html_El':
                                    var my_Html_El = params[key];
                                    break;
                                default:
                                    break;
                            }
                    }
                if(my_Form_Id && LSD_$(my_Form_Id))
                    {
                        if(confirm(my_Msg_Id))
                            {
                                LSD_$(my_Form_Id).submit();
                            }
                    }
                if(my_Html_Id)
                    {
                        if(!my_Form_Id)
                            {
                                if(my_Msg_Id)
                                    {
                                        alert(my_Msg_Id);
                                    }
                            }
                     if (LSD_$(my_Html_Id)) LSD_$(my_Html_Id).innerHTML = this.req.responseText;
                    }
                if(my_Html_El)
                    {
                        my_Html_El.title = this.req.responseText;
                    }
            }
    }
/**
 * @param string -
 *         url_Id est la page � appeller (php, asp, txt avec les param�tres)
 * @param string -
 *         element_Id est l'id de l'�l�ment HTML o� �crire la r�ponse
 * @param function -
 *         js_Fct est la fonction JS � ex�cuter
 */
function LSD_Get_Ajax_Data(url_Id, element_Id, js_Fct)
    {
        var my_Url_Id = "";
        var my_Element_Id = "";
        var my_Js_Fct = "";
        if(url_Id)
            {
                my_Url_Id = url_Id;
                if(element_Id)
                    {
                        my_Element_Id = element_Id;
                        net.ParamOnload(my_Element_Id);
                    }
                else
                    {
                        net.ParamOnload('NULL');
                    }
                if(js_Fct)
                    {
                        my_Js_Fct = js_Fct;
                    }
                new net.ContentLoader(my_Url_Id, my_Js_Fct, null, 'GET');
            }
    }
/**
 * @param string -
 *         url_Id est l'id de la page � appeler
 * @param string_ou_object -
 *         element_Id est soit l'Id d'un �l�ment HTML soit un objet {html_Id :
 *         xxx, html_El:xxx}
 * @param string -
 *         param est la valeur � envoyer par le POST. Exemple :
 *         nom=valeur&xxx=yyy etc ...
 * @param string -
 *         content est le header � envoyer par d�faut text/html
 * @param function -
 *         js_Fct est la fonction JS � executer pour traiter les donn�es
 *         retourn�es
 */
function LSD_Post_Data(url_Id, element_Id, param, content, js_Fct)
    {
        var my_Url = "";
        var my_Param = "";
        var my_Content = "";
        var my_Js_Fct = "";
        if(url_Id)
            {
                my_Url = url_Id;
                if(element_Id)
                    {
                        var my_Element_Id = element_Id;
                        net.ParamOnload(my_Element_Id);
                    }
                else
                    {
                        net.ParamOnload('NULL');
                    }
                if(param)
                    {
                        my_Param = param;
                    }
                if(content)
                    {
                        my_Content = content;
                    }
                
                if(js_Fct)
                    {
                        my_Js_Fct = js_Fct;
                    }
                new net.ContentLoader(my_Url, my_Js_Fct, null, 'POST', my_Param, my_Content);
            }
    }
//
-->