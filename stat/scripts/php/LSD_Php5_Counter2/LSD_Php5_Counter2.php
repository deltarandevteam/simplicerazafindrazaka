<?php
/**
 * Compteur de visites (hit) param�trable utilisable soit depuis une page PHP5 soit depuis une page HTML via les
 * deux scripts LSD_Mouse_Events2.js et LSD_Stuff2.js
 *
 * PHP version 5
 *
 * LICENSE: Ce script vous est gracieusement offert par The Liberated Seven Dwarfs et est libre de tout droit
 * d'utilisation priv�e et non commerciale sous la restriction de conserver le pr�sent ent�te et de respecter
 * la licence Creative Commons : By-Nc-Sa.
 *
 * @category Web utilities
 * @package the_LSD_Php5_Counter2
 * @author The Liberated Seven Dwarfs
 * @copyright 2008-2009 Advanced Software Solutions Inc.
 * @license Creative-Commons_By-Nc-Sa - http://creativecommons.org/licenses/by-nc-sa/2.0/fr/
 * @link http://theliberated7dwarfs.as2.com
 * @version 2.0.0.a - February 8, 2009
 * @filesource documented_LSD_Php5_Counter2.php
 */
new LSD_Ajax_Counter2();
class LSD_Ajax_Counter2{
const over_Root="../../../";
const script_Path="scripts/php/LSD_Php5_Counter2/";
private $ajax_Load;
private $rough_Response;
private $ajax_Root;
private $ajax_Debug;
private $ajax_Config;
private $ajax_Mode;
private $ajax_Pad;
private $ajax_Read;
private $ajax_Hidden;
private $ajax_Session;
private $ajax_New;
private $my_Ajax_Counter2;
function __construct(){
$this->ajax_Load="";
$this->rough_Response="";
$this->ajax_Root="";
$this->ajax_Debug="";
$this->ajax_Config="";
$this->ajax_Mode="";
$this->ajax_Pad="";
$this->ajax_Read="";
$this->ajax_Hidden="";
$this->ajax_Session="";
$this->my_Ajax_Counter2="";
if(!empty($_REQUEST['LSD_Load'])){
$this->ajax_Load=$_REQUEST['LSD_Load'];
}
if(!empty($_REQUEST['LSD_Rough'])){
$this->rough_Response=$_REQUEST['LSD_Rough'];
}
if(!empty($_REQUEST['LSD_Root'])){
$this->ajax_Root=$_REQUEST['LSD_Root'];
}
if(!empty($_REQUEST['LSD_Debug'])){
$this->ajax_Debug=$_REQUEST['LSD_Debug'];
}
if(!empty($_REQUEST['LSD_Config'])){
$this->ajax_Config=$_REQUEST['LSD_Config'];
}
if(!empty($_REQUEST['LSD_Mode'])){
$this->ajax_Mode=$_REQUEST['LSD_Mode'];
}
if(isset($_REQUEST['LSD_Pad'])){
$this->ajax_Pad=$_REQUEST['LSD_Pad'];
if(is_string($this->ajax_Pad)){
$this->ajax_Pad=($this->ajax_Pad!="0")?$this->ajax_Pad:"0";
}
}
if(!empty($_REQUEST['LSD_Read'])){
$this->ajax_Read=$_REQUEST['LSD_Read'];
}
if(!empty($_REQUEST['LSD_Session'])){
$this->ajax_Session=$_REQUEST['LSD_Session'];
}
if(!empty($_REQUEST['LSD_Hidden'])){
$this->ajax_Hidden=$_REQUEST['LSD_Hidden'];
}
$this->start_LSD_Counter2();
}
private function start_LSD_Counter2(){
if((strtolower($this->ajax_Load)=='ajax')){
session_start();
$this->config_Name='default_Counter2';
$this->my_Ajax_Counter2=new LSD_Php5_Counter2(self::over_Root,self::script_Path,$this->config_Name);
$this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Ajax_Load(true,$this->ajax_Root);
$this->check_Params();
$this->my_Ajax_Counter2->get_LSD_Php5_Counter2_Hit_Result();
}else{
$this->ajax_Load='';
}
}
private function check_Params(){
if((strtolower($this->ajax_Debug)=='debug')){
$this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Debug_Mode(true);
}
if((!empty($this->ajax_Config))&&($this->ajax_Config!='')){
$this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Config_Name($this->ajax_Config);
}
if((strtolower($this->rough_Response)=='rough')){
$this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Rough_Response(true);
}
if((strtolower($this->ajax_Read)=='true')){
$this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Read_Only(true);
}
if((strtolower($this->ajax_Hidden)=='true')){
$this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Hidden_Result(true);
}
if((!empty($this->ajax_Session))&&(is_string($this->ajax_Session))&&($this->ajax_Session!="")){
$this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Session_Id($this->ajax_Session);
}
if((strtoupper($this->ajax_Mode)==='T')||(strtoupper($this->ajax_Mode)==='G')){
$this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Response_Mode($this->ajax_Mode);
}
if((isset($this->ajax_Pad))&&($this->ajax_Pad!='')&&($this->ajax_Pad!=null)){
$this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Pad_Number($this->ajax_Pad);
}
}
}
class LSD_Php5_Counter2{
const bi="<img src=\"";
const mi="\"";
const ei="/>";
const h="height=\"";
const w="width=\"";
const p="px\"";
const bt="<span style=\"";
const mt="\">";
const et="</span>";
const s=";";
const ff=" font-family:";
const fs=" font-size:";
const cp="config/";
const ce=".inc";
const fp="hits/";
private $debug;
private $read_Only;
private $over_Root;
private $config_Root;
private $ajax;
private $ajax_Root;
private $msg;
private $msg_Nb;
private $script_Path;
private $config_Path;
private $config_Name;
private $changed_Config;
private $new_Config;
private $file_Path;
private $file_Name;
private $changed_Name;
private $new_Name;
private $session_Id;
private $new_Id;
private $changed_Id;
private $init;
private $cheat;
private $hidden;
private $changed_Hidden;
private $mode;
private $changed_Mode;
private $default_Mode;
private $new_Mode;
private $pad;
private $changed_Pad;
private $default_Pad;
private $new_Pad;
private $color;
private $b_Color;
private $f_Fam;
private $size;
private $t_0;
private $t_1;
private $t_2;
private $t_3;
private $i_Path;
private $i_Name;
private $i_Ext;
private $i_H;
private $i_W;
private $i_Border;
private $i_Lft;
private $i_Lft_H;
private $i_Lft_W;
private $i_Rgt1;
private $i_Rgt1_H;
private $i_Rgt1_W;
private $i_Rgt2;
private $i_Rgt2_H;
private $i_Rgt2_W;
private $c;
private $b;
private $t;
private $i;
private $hit_File;
private $n_Hit;
private $n_Str;
private $LSD_Php5_Counter2;
private $n_Digit;
private $rough_Response;
function __construct($over_Root,$script_Path,$config_Name){
$this->init_Attributes();
$this->initial_Load($over_Root,$script_Path,$config_Name);
}
private function init_Attributes(){
$this->debug=false;
$this->read_Only=false;
$this->over_Root='';
$this->config_Root='';
$this->ajax=false;
$this->ajax_Root='';
$this->msg='None';
$this->msg_Nb=0;
$this->script_Path='';
$this->config_Path='';
$this->config_Name='';
$this->changed_Config=false;
$this->new_Config='';
$this->file_Path='';
$this->file_Name='';
$this->changed_Name=false;
$this->new_Name='';
$this->session_Id='';
$this->new_Id='';
$this->changed_Id=false;
$this->init=0;
$this->cheat=0;
$this->hidden=false;
$this->changed_Hidden=false;
$this->mode='';
$this->changed_Mode=false;
$this->default_Mode='';
$this->new_Mode='';
$this->pad='';
$this->changed_Pad=false;
$this->default_Pad='';
$this->new_Pad='';
$this->color='';
$this->b_Color='';
$this->f_Fam='';
$this->size='';
$this->t_0='';
$this->t_1='';
$this->t_2='';
$this->t_3='';
$this->i_Path='';
$this->i_Name='';
$this->i_Ext='';
$this->i_H='';
$this->i_W='';
$this->i_Border='';
$this->i_Lft='';
$this->i_Lft_H='';
$this->i_Lft_W='';
$this->i_Rgt1='';
$this->i_Rgt1_H='';
$this->i_Rgt1_W='';
$this->i_Rgt2='';
$this->i_Rgt2_H='';
$this->i_Rgt2_W='';
$this->c='';
$this->b='';
$this->t='';
$this->i=0;
$this->hit_File='';
$this->n_Hit=0;
$this->n_Str='';
$this->LSD_Php5_Counter2='';
$this->n_Digit='';
$this->rough_Response=false;
}
private function initial_Load($over_Root,$script_Path,$config_Name){
if(!isset($script_Path)){
$this->create_Message(1);
}else{
if(!file_exists($over_Root.$script_Path)){
$this->script_Path=$script_Path;
$this->create_Message(2);
$this->script_Path=null;
}else{
$this->script_Path=$script_Path;
}
}
if(!isset($config_Name)){
$this->create_Message(3);
}else{
$this->config_Name=$config_Name;
}
$this->config_Path=$script_Path.self::cp;
if(!isset($over_Root)){
$this->over_Root='';
}else{
$this->over_Root=$over_Root;
}
if(!file_exists($this->over_Root.$this->config_Path)){
$this->create_Message(4);
}
if(($this->msg=="None")&&(!file_exists($this->over_Root.$this->config_Path.$this->config_Name.self::ce))){
$this->create_Message(5);
}
if($this->msg=="None"){
require ($this->over_Root.$this->config_Path.$this->config_Name.self::ce);
}
}
private function create_Message($msg_Nb){
switch($msg_Nb){
case 0:
$this->msg=htmlentities("None");
break;
case 1:
$this->msg=htmlentities("Erreur #01:La variable du chemin du fichier script n'est pas initialis�e avant l'appel du script!");
break;
case 2:
$this->msg=htmlentities("Erreur #02:Le chemin du fichier script ".$this->over_Root.$this->script_Path." est introuvable!");
break;
case 3:
$this->msg=htmlentities("Erreur #03:La variable du nom de fichier de configuration n'est pas initialis�e avant l'appel du script!");
break;
case 4:
$this->msg=htmlentities("Erreur #04:Le r�pertoire ".$this->over_Root.$this->config_Path." est introuvable!");
break;
case 5:
$this->msg=htmlentities("Erreur #05:Le fichier de configuration ".$this->over_Root.$this->config_Path.$this->config_Name.self::ce." est introuvable!");
break;
case 6:
$this->msg=htmlentities("Erreur #06:L'attribut \$this->session_Id n'est pas initialis�!");
break;
case 7:
$this->msg=htmlentities("Erreur #07:L'attribut \$this->file_Path n'est pas initialis�!");
break;
case 8:
$this->msg=htmlentities("Erreur #08:L'attribut \$this->file_Name n'est pas initialis�!");
break;
case 9:
$this->msg=htmlentities("Erreur #09:L'attribut \$this->file_Ext n'est pas initialis�!");
break;
case 10:
$this->msg=htmlentities("Erreur #10:Le r�pertoire ".$this->over_Root.$this->file_Path." est introuvable!");
break;
case 11:
$this->msg=htmlentities("Erreur #11:L'attribut \$this->init n'est pas initialis�!");
break;
case 12:
$this->msg=htmlentities("Erreur #12:L'attribut \$this->cheat n'est pas initialis�!");
break;
case 13:
$this->msg=htmlentities("Erreur #13:L'attribut \$this->hidden n'est pas initialis�!");
break;
case 14:
$this->msg=htmlentities("Erreur #14:L'attribut \$this->mode n'est pas initialis�!");
break;
case 15:
$this->msg=htmlentities("Erreur #15:L'attribut \$this->pad n'est pas initialis�!");
break;
case 16:
$this->msg=htmlentities("Erreur #16:L'attribut \$this->color n'est pas initialis�!");
break;
case 17:
$this->msg=htmlentities("Erreur #17:L'attribut \$this->b_Color n'est pas initialis�!");
break;
case 18:
$this->msg=htmlentities("Erreur #18:L'attribut \$this->size n'est pas initialis�!");
break;
case 19:
$this->msg=htmlentities("Erreur #19:L'attribut \$this->f_Fam n'est pas initialis�!");
break;
case 20:
$this->msg=htmlentities("Erreur #20:L'attribut \$this->t_0 n'est pas initialis�!");
break;
case 21:
$this->msg=htmlentities("Erreur #21:L'attribut \$this->t_1 n'est pas initialis�!");
break;
case 22:
$this->msg=htmlentities("Erreur #22:L'attribut \$this->t_2 n'est pas initialis�!");
break;
case 23:
$this->msg=htmlentities("Erreur #23:L'attribut \$this->t_3 n'est pas initialis�!");
break;
case 24:
$this->msg=htmlentities("Erreur #24:L'attribut \$this->i_Path n'est pas initialis�!");
break;
case 25:
$this->msg=htmlentities("Erreur #25:Le r�pertoire ".$this->over_Root.$this->i_Path." est introuvable!");
break;
case 26:
$this->msg=htmlentities("Erreur #26:L'attribut \$this->i_Name n'est pas initialis�!");
break;
case 27:
$this->msg=htmlentities("Erreur #27:L'attribut \$this->i_Ext n'est pas initialis�!");
break;
case 28:
$this->msg=htmlentities("Erreur #28:L'attribut \$this->i_Border n'est pas initialis�!");
break;
case 29:
$this->msg=htmlentities("Erreur #29:L'attribut \$this->i_Lft n'est pas initialis�!");
break;
case 30:
$this->msg=htmlentities("Erreur #30:L'image ".$this->over_Root.$this->i_Path.$this->i_Name.$this->i_Lft.'.'.$this->i_Ext." est introuvable!");
break;
case 31:
$this->msg=htmlentities("Erreur #31:L'attribut \$this->i_Lft_H n'est pas initialis�!");
break;
case 32:
$this->msg=htmlentities("Erreur #32:L'attribut \$this->i_Lft_W n'est pas initialis�!");
break;
case 33:
$this->msg=htmlentities("Erreur #33:L'attribut \$this->i_Rgt1 n'est pas initialis�!");
break;
case 34:
$this->msg=htmlentities("Erreur #34:L'image ".$this->over_Root.$this->i_Path.$this->i_Name.$this->i_Rgt1.'.'.$this->i_Ext." est introuvable!");
break;
case 35:
$this->msg=htmlentities("Erreur #35:L'attribut \$this->i_Rgt1_H n'est pas initialis�!");
break;
case 36:
$this->msg=htmlentities("Erreur #36:L'attribut \$this->i_Rgt1_W n'est pas initialis�!");
break;
case 37:
$this->msg=htmlentities("Erreur #37:L'attribut \$this->i_Rgt2 n'est pas initialis�!");
break;
case 38:
$this->msg=htmlentities("Erreur #38:L'image ".$this->over_Root.$this->i_Path.$this->i_Name.$this->i_Rgt2.'.'.$this->i_Ext." est introuvable!");
break;
case 39:
$this->msg=htmlentities("Erreur #39:L'attribut \$this->i_Rgt2_H n'est pas initialis�!");
break;
case 40:
$this->msg=htmlentities("Erreur #40:L'attribut \$this->i_Rgt2_W n'est pas initialis�!");
break;
case 41:
$this->msg=htmlentities("Erreur #41:L'image ".$this->over_Root.$this->i_Path.$this->i_Name.$this->i.'.'.$this->i_Ext." est introuvable!");
break;
case 42:
$this->msg=htmlentities("Erreur #42:L'attribut \$this->i_H n'est pas initialis�!");
break;
case 43:
$this->msg=htmlentities("Erreur #43:L'attribut \$this->i_W n'est pas initialis�!");
break;
case 44:
$this->msg=htmlentities("Erreur #44:L'attribut \$this->mode n'est pas correctement initialis�!");
break;
case 45:
$this->msg=htmlentities("Erreur #45:Le param�tre \$this->mode n'est pas correctement initialis�!");
break;
case 46:
$this->msg=htmlentities("Erreur #46:Le chemin pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Ajax_Load() est invalide!");
break;
case 47:
$this->msg=htmlentities("Erreur #47:Le chemin pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Ajax_Load() en mode AJAX est invalide!");
break;
case 48:
$this->msg=htmlentities("Erreur #48:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Ajax_Load() est invalide!");
break;
case 49:
$this->msg=htmlentities("Erreur #49:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Ajax_Load() en mode AJAX est invalide!");
break;
case 50:
$this->msg=htmlentities("Erreur #50:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Config_Name() est invalide!");
break;
case 51:
$this->msg=htmlentities("Erreur #51:Le fichier de configuration pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Config_Name() en mode AJAX est introuvable!");
break;
case 52:
$this->msg=htmlentities("Erreur #52:La fichier de configuration pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Debug_Mode() est introuvable!!");
break;
case 53:
$this->msg=htmlentities("Erreur #53:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Debug_Mode() en mode AJAX est invalide!");
break;
case 54:
$this->msg=htmlentities("Erreur #54:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Hidden_Result() est invalide!");
break;
case 55:
$this->msg=htmlentities("Erreur #55:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Hidden_Result() en mode AJAX est invalide!");
break;
case 56:
$this->msg=htmlentities("Erreur #56:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Rough_Response() est invalide!");
break;
case 57:
$this->msg=htmlentities("Erreur #57:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Rough_Response() en mode AJAX est invalide!");
break;
case 58:
$this->msg=htmlentities("Erreur #58:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Read_Only() est invalide!");
break;
case 59:
$this->msg=htmlentities("Erreur #59:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Read_Only() en mode AJAX est invalide!");
break;
case 60:
$this->msg=htmlentities("Erreur #60:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Session_Id() est invalide!");
break;
case 61:
$this->msg=htmlentities("Erreur #61:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Session_Id() en mode AJAX est invalide!");
break;
case 62:
$this->msg=htmlentities("Erreur #62:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Response_Mode() est invalide!");
break;
case 63:
$this->msg=htmlentities("Erreur #63:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Response_Mode() en mode AJAX est invalide!");
break;
case 64:
$this->msg=htmlentities("Erreur #64:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Pad_Number() est invalide!");
break;
case 65:
$this->msg=htmlentities("Erreur #65:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Pad_Number() en mode AJAX est invalide!");
break;
case 66:
$this->msg=htmlentities("Erreur #66:Le fichier des visites avec le nom pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Hit_Name() est introuvable!");
break;
case 67:
$this->msg=htmlentities("Erreur #67:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Hit_Name() est invalide!");
break;
case 68:
$this->msg=htmlentities("Erreur #68:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Hit_Name() en mode AJAX est invalide!");
break;
default:
$this->msg=htmlentities("None");
break;
}
}
private function test_Config(){
if((!isset($this->session_Id))||(empty($this->session_Id))){
$this->create_Message(6);
return;
}
if(!isset($this->file_Path)){
$this->create_Message(7);
return;
}
if((!isset($this->file_Name))||(empty($this->file_Name))){
$this->create_Message(8);
return;
}
if((!isset($this->file_Ext))||(empty($this->file_Ext))){
$this->create_Message(9);
return;
}
if(!file_exists($this->over_Root.$this->file_Path)){
$this->create_Message(10);
return;
}
if((!isset($this->init))&&(empty($this->init))){
$this->create_Message(11);
return;
}
if((!isset($this->cheat))&&(empty($this->cheat))){
$this->create_Message(12);
return;
}
if((!isset($this->hidden))&&(empty($this->hidden))){
$this->create_Message(13);
return;
}
if((!isset($this->mode))||(empty($this->mode))){
$this->create_Message(14);
return;
}
if((!isset($this->pad))&&(empty($this->pad))){
$this->create_Message(15);
return;
}
if(strtolower($this->mode)=="t"){
if((!isset($this->color))||(empty($this->color))){
$this->create_Message(16);
return;
}
if((!isset($this->b_Color))||(empty($this->b_Color))){
$this->create_Message(17);
return;
}
if((!isset($this->size))||(empty($this->size))){
$this->create_Message(18);
return;
}
if((!isset($this->f_Fam))||(empty($this->f_Fam))){
$this->create_Message(19);
return;
}
if(!isset($this->t_0)){
$this->create_Message(20);
return;
}
if(!isset($this->t_1)){
$this->create_Message(21);
return;
}
if(!isset($this->t_2)){
$this->create_Message(22);
return;
}
if(!isset($this->t_3)){
$this->create_Message(23);
return;
}
}elseif(strtolower($this->mode)=="g"){
if(!isset($this->i_Path)){
$this->create_Message(24);
return;
}
if(!file_exists($this->over_Root.$this->i_Path)){
$this->create_Message(25);
return;
}
if((!isset($this->i_Name))||(empty($this->i_Name))){
$this->create_Message(26);
return;
}
if((!isset($this->i_Ext))||(empty($this->i_Ext))){
$this->create_Message(27);
return;
}
if((!isset($this->i_Border))&&(empty($this->i_Border))){
$this->create_Message(28);
return;
}
if($this->i_Border==true){
if(!isset($this->i_Lft)){
$this->create_Message(29);
return;
}
if(!file_exists($this->over_Root.$this->i_Path.$this->i_Name.$this->i_Lft.'.'.$this->i_Ext)){
$this->create_Message(30);
return;
}
if(!isset($this->i_Lft_H)){
$this->create_Message(31);
return;
}
if(!isset($this->i_Lft_W)){
$this->create_Message(32);
return;
}
if(!isset($this->i_Rgt1)){
$this->create_Message(33);
return;
}
if(!file_exists($this->over_Root.$this->i_Path.$this->i_Name.$this->i_Rgt1.'.'.$this->i_Ext)){
$this->create_Message(34);
return;
}
if(!isset($this->i_Rgt1_H)){
$this->create_Message(35);
return;
}
if(!isset($this->i_Rgt1_W)){
$this->create_Message(36);
return;
}
if(!isset($this->i_Rgt2)){
$this->create_Message(37);
return;
}
if($this->i_Rgt2!=$this->i_Rgt1){
if(!file_exists($this->over_Root.$this->i_Path.$this->i_Name.$this->i_Rgt2.'.'.$this->i_Ext)){
$this->create_Message(38);
return;
}
if(!isset($this->i_Rgt2_H)){
$this->create_Message(39);
return;
}
if(!isset($this->i_Rgt2_W)){
$this->create_Message(40);
return;
}
}
}
for($this->i=0;$this->i<10;$this->i++){
if(!file_exists($this->over_Root.$this->i_Path.$this->i_Name.$this->i.'.'.$this->i_Ext)){
$this->create_Message(41);
return;
}
}
if(!isset($this->i_H)){
$this->create_Message(42);
return;
}
if(!isset($this->i_W)){
$this->create_Message(43);
return;
}
}else{
$this->create_Message(44);
return;
}
}
private function create_Config(){
$this->session_Id="Default_LSD_Php5_Counter2";
if(!file_exists($this->over_Root.$this->script_Path.self::fp)){
mkdir($this->over_Root.$this->script_Path.self::fp,0777);
}
$this->file_Path=$this->script_Path.self::fp;
$this->file_Name="Default_LSD_Php5_Counter2";
$this->file_Ext="nbr";
$this->init="0";
$this->cheat="0";
$this->hidden=false;
$this->mode="T";
$this->pad="1";
$this->color="inherit";
$this->b_Color="none";
$this->f_Fam="inherit";
$this->size="inherit";
}
private function reset_Config(){
$this->create_Message(0);
$this->new_File=0;
$this->n_Digit=null;
if($this->ajax){
$this->over_Root=$this->config_Root;
}
$this->ajax=false;
$this->rough_Response=false;
if($this->changed_Mode){
$this->mode=$this->default_Mode;
$this->changed_Mode=false;
$this->default_Mode="";
}
if($this->changed_Pad){
$this->pad=$this->default_Pad;
$this->changed_Pad=false;
$this->default_Pad="";
}
$this->changed_Config=false;
$this->changed_Name=false;
$this->changed_Id=false;
$this->LSD_Php5_Counter2=null;
}
private function test_Changes(){
if($this->changed_Config===true){
$this->config_Name=$this->new_Config;
}
if($this->changed_Id===true){
$this->session_Id=$this->new_Id;
}
if($this->changed_Hidden===true){
$this->hidden=$this->changed_Hidden;
}
if($this->changed_Mode===true){
$this->mode=$this->new_Mode;
}
if($this->changed_Pad===true){
$this->pad=$this->new_Pad;
}
if($this->changed_Name===true){
$this->file_Name=$this->new_Name;
}
}
private function create_Counter(){
$this->compute_Hit();
if(isset($this->read_Only)&&$this->read_Only==true){
$this->hidden=false;
}
if($this->hidden==false){
if(($this->mode=="G")||($this->mode=="g")){
$this->draw_Counter();
}elseif(($this->mode=="T")||($this->mode=="t")){
$this->write_Counter();
}else{
$this->create_Message(45);
}
}
}
private function compute_Hit(){
if(!file_exists($this->over_Root.$this->file_Path.$this->file_Name.'.'.$this->file_Ext)){
if((!isset($this->read_Only))||($this->read_Only==false)){
$this->hit_File=fopen($this->over_Root.$this->file_Path.$this->file_Name.'.'.$this->file_Ext,"a");
if((isset($this->init))&&(strval($this->init)>0)){
$this->n_Hit=strval($this->init);
}else{
$this->n_Hit=0;
}
}else{
$this->n_Hit=0;
}
}else{
$this->hit_File=fopen($this->over_Root.$this->file_Path.$this->file_Name.'.'.$this->file_Ext,"r+");
$this->n_Hit=fgets($this->hit_File,15);
}
if((!isset($this->read_Only))||($this->read_Only==false)){
if((isset($this->cheat))&&(strval($this->cheat)>0)){
$this->n_Hit=$this->n_Hit+strval($this->cheat);
}
if(!isset($_SESSION[$this->session_Id])){
$_SESSION[$this->session_Id]=$this->session_Id;
$this->n_Hit=$this->n_Hit+1;
fseek($this->hit_File,0);
fwrite($this->hit_File,$this->n_Hit);
}
}
if(file_exists($this->hit_File)){
fclose($this->hit_File);
}
$this->n_Str=strval($this->n_Hit);
if(strlen($this->n_Str)<strval($this->pad)){
$this->n_Str=STR_PAD($this->n_Hit,$this->pad,'0',STR_PAD_LEFT);
}
}
private function draw_Counter(){
if($this->ajax==true){
$this->over_Root=$this->ajax_Root;
}
if($this->i_Border==true){
$this->LSD_Php5_Counter2=self::bi.$this->over_Root.$this->i_Path.$this->i_Name.$this->i_Lft.".".$this->i_Ext.self::mi.self::h.$this->i_Lft_H.self::p.self::w.$this->i_Lft_W.self::p.self::ei;
}
$this->n_Digit="";
for($this->i=0;$this->i<strlen($this->n_Str);$this->i++){
$this->n_Digit.=self::bi.$this->over_Root.$this->i_Path.$this->i_Name.substr($this->n_Str,$this->i,1).".".$this->i_Ext.self::mi.self::h.$this->i_H.self::p.self::w.$this->i_W.self::p.self::ei;
}
$this->LSD_Php5_Counter2.=$this->n_Digit;
if($this->i_Border==true){
if($this->n_Hit<2){
$this->LSD_Php5_Counter2.=self::bi.$this->over_Root.$this->i_Path.$this->i_Name.$this->i_Rgt1.".".$this->i_Ext.self::mi.self::h.$this->i_Rgt1_H.self::p.self::w.$this->i_Rgt1_W.self::p.self::ei;
}else{
$this->LSD_Php5_Counter2.=self::bi.$this->over_Root.$this->i_Path.$this->i_Name.$this->i_Rgt2.".".$this->i_Ext.self::mi.self::h.$this->i_Rgt2_H.self::p.self::w.$this->i_Rgt2_W.self::p.self::ei;
}
}
}
private function write_Counter(){
if($this->color=="inherit"){
$this->c="color:";
}else{
$this->c="color:#";
}
if($this->b_Color=="none"){
$this->b=" background:";
}else{
$this->b=" background:#";
}
if($this->size=="inherit"){
$this->t=";";
}else{
$this->t="px;";
}
if($this->pad>0){
$this->LSD_Php5_Counter2=self::bt.$this->c.$this->color.self::s.$this->b.$this->b_Color.self::s.self::ff.$this->f_Fam.self::s.self::fs.$this->size.$this->t.self::mt.$this->n_Str.self::et;
}else{
if($this->n_Hit==0){
$this->LSD_Php5_Counter2=self::bt.$this->c.$this->color.self::s.$this->b.$this->b_Color.self::s.self::ff.$this->f_Fam.self::s.self::fs.$this->size.$this->t.self::mt.$this->t_0.self::et;
}elseif($this->n_Hit==1){
$this->LSD_Php5_Counter2=self::bt.$this->c.$this->color.self::s.$this->b.$this->b_Color.self::s.self::ff.$this->f_Fam.self::s.self::fs.$this->size.$this->t.self::mt.$this->t_1.$this->n_Hit.$this->t_2.self::et;
}else{
$this->LSD_Php5_Counter2=self::bt.$this->c.$this->color.self::s.$this->b.$this->b_Color.self::s.self::ff.$this->f_Fam.self::s.self::fs.$this->size.$this->t.self::mt.$this->t_1.$this->n_Hit.$this->t_3.self::et;
}
}
if($this->rough_Response===true){
$this->send_Rough();
}
}
private function send_Rough(){
$this->LSD_Php5_Counter2=strip_tags($this->LSD_Php5_Counter2);
$this->LSD_Php5_Counter2=html_entity_decode($this->LSD_Php5_Counter2,0,'UTF-8');
}
public function get_LSD_Php5_Counter2_Error_Status(){
return $this->msg;
}
public function get_LSD_Php5_Counter2_Ajax_Load(){
if($this->ajax){
return ($this->ajax_Root.$this->i_Path);
}else{
return ("F");
}
}
public function set_LSD_Php5_Counter2_Ajax_Load(){
if(func_num_args()>0){
$ajax=func_get_arg(0);
if(is_bool($ajax)){
$this->ajax=$ajax;
if(func_num_args()>1){
$this->config_Root=$this->over_Root;
$this->ajax_Root=func_get_arg(1);
if(!file_exists($this->ajax_Root.$this->i_Path)){
if($this->ajax!=true){
$this->create_Message(46);
}else{
$this->create_Message(47);
}
}
}
}
}elseif(($ajax=="")||($ajax==null)||(empty($ajax))){
if($this->ajax!=true){
$this->create_Message(48);
}else{
$this->create_Message(49);
}
$this->ajax=false;
$this->ajax_Root=null;
}
}
public function get_LSD_Php5_Counter2_Config_Name(){
if($this->changed_Config){
return ($this->over_Root.$this->config_Path.$this->new_Config.self::ce);
}else{
return ($this->over_Root.$this->config_Path.$this->config_Name.self::ce);
}
}
public function set_LSD_Php5_Counter2_Config_Name(){
if(func_num_args()>0){
$new_Config=func_get_arg(0);
if((is_string($new_Config))&&($new_Config!="")){
if(file_exists($this->over_Root.$this->config_Path.$new_Config.self::ce)){
$this->config_Name=$new_Config;
$this->new_Config=$new_Config;
$this->changed_Config=true;
$this->create_Message(0);
}else{
if($this->ajax!=true){
$this->create_Message(50);
}else{
$this->create_Message(51);
}
$this->changed_Config=false;
}
}
}
}
public function get_LSD_Php5_Counter2_Debug_Mode(){
if($this->debug){
return ("T");
}else{
return ("F");
}
}
public function set_LSD_Php5_Counter2_Debug_Mode(){
if(func_num_args()>0){
$debug=func_get_arg(0);
if(is_bool($debug)){
$this->debug=$debug;
}elseif(($debug=="")||($debug==null)||(empty($debug))){
if($this->ajax!=true){
$this->create_Message(52);
}else{
$this->create_Message(53);
}
}
}
}
public function get_LSD_Php5_Counter2_Hidden_Result(){
if($this->changed_Hidden){
return ("T");
}else{
return ("F");
}
}
public function set_LSD_Php5_Counter2_Hidden_Result(){
if(func_num_args()>0){
$changed_Hidden=func_get_arg(0);
if(is_bool($changed_Hidden)){
$this->changed_Hidden=$changed_Hidden;
}elseif(($changed_Hidden=="")||($changed_Hidden==null)||(empty($changed_Hidden))){
if($this->ajax!=true){
$this->create_Message(54);
}else{
$this->create_Message(55);
}
$this->changed_Hidden=false;
}
}
}
public function get_LSD_Php5_Counter2_Rough_Response(){
if($this->rough_Response){
return ("T");
}else{
return ("F");
}
}
public function set_LSD_Php5_Counter2_Rough_Response(){
if(func_num_args()>0){
$rough_Response=func_get_arg(0);
if(is_bool($rough_Response)){
$this->rough_Response=$rough_Response;
}elseif(($rough_Response=="")||($rough_Response==null)||(empty($rough_Response))){
if($this->ajax!=true){
$this->create_Message(56);
}else{
$this->create_Message(57);
}
$this->rough_Response=false;
}
}
}
public function get_LSD_Php5_Counter2_Read_Only(){
if($this->read_Only){
return ("T");
}else{
return ("F");
}
}
public function set_LSD_Php5_Counter2_Read_Only(){
if(func_num_args()>0){
$read_Only=func_get_arg(0);
if(is_bool($read_Only)){
$this->read_Only=$read_Only;
}elseif(($read_Only=="")||($read_Only==null)||(empty($read_Only))){
if($this->ajax!=true){
$this->create_Message(58);
}else{
$this->create_Message(59);
}
$this->read_Only=false;
}
}
}
public function get_LSD_Php5_Counter2_Session_Id(){
if($this->changed_Id==true){
return ($this->new_Id);
}else{
return ($this->session_Id);
}
}
public function set_LSD_Php5_Counter2_Session_Id(){
if(func_num_args()>0){
$new_Id=func_get_arg(0);
if((!empty($new_Id))&&(is_string($new_Id))&&($new_Id!="")){
$this->changed_Id=true;
$this->new_Id=$new_Id;
}else{
if($this->ajax!=true){
$this->create_Message(60);
}else{
$this->create_Message(61);
}
$this->changed_Id=false;
}
}
}
public function get_LSD_Php5_Counter2_Response_Mode(){
if($this->changed_Mode==true){
if(strtolower($this->new_Mode)==="t"){
return ("T");
}else{
return ("G");
}
}else{
return ($this->mode);
}
}
public function set_LSD_Php5_Counter2_Response_Mode(){
if(func_num_args()>0){
$changed_Mode=func_get_arg(0);
if((!empty($changed_Mode))&&(is_string($changed_Mode))&&($changed_Mode!="")&&((strtolower($changed_Mode)==="t")||(strtolower($changed_Mode)==="g"))){
$this->changed_Mode=true;
$this->default_Mode=$this->mode;
$this->new_Mode=$changed_Mode;
}else{
if($this->ajax!=true){
$this->create_Message(62);
}else{
$this->create_Message(63);
}
$this->changed_Mode=false;
}
}
}
public function get_LSD_Php5_Counter2_Pad_Number(){
if($this->changed_Pad==true){
return $this->new_Pad;
}else{
return ($this->pad);
}
}
public function set_LSD_Php5_Counter2_Pad_Number(){
if(func_num_args()>0){
$changed_Pad=func_get_arg(0);
if((isset($changed_Pad))&&(is_string($changed_Pad))&&($changed_Pad!="")){
$this->changed_Pad=true;
$this->default_Pad=$this->pad;
$this->new_Pad=strval($changed_Pad);
}else{
if($this->ajax!=true){
$this->create_Message(64);
}else{
$this->create_Message(65);
}
$this->changed_Pad=false;
}
}
}
public function get_LSD_Php5_Counter2_Hit_Name(){
if($this->changed_Name){
return ($this->new_Name);
}else{
return ($this->file_Name);
}
}
public function set_LSD_Php5_Counter2_Hit_Name(){
if(func_num_args()>0){
$changed_Name=func_get_arg(0);
if(is_string($changed_Name)){
if(file_exists($this->over_Root.$this->file_Path.$changed_Name.'.'.$this->file_Ext)){
$this->new_Name=$changed_Name;
$this->changed_Name=true;
}else{
$this->create_Message(66);
}
}elseif(($changed_Name=="")||($changed_Name==null)||(empty($changed_Name))){
if($this->ajax!=true){
$this->create_Message(67);
}else{
$this->create_Message(68);
}
$this->changed_Name=false;
}
}
}
public function get_LSD_Php5_Counter2_Hit_Result(){
if($this->msg=="None"){
if($this->changed_Config){
require ($this->over_Root.$this->config_Path.$this->config_Name.self::ce);
}
$this->test_Config();
if($this->msg=="None"){
$this->test_Changes();
$this->create_Counter();
}
}
if($this->msg!="None"){
if($this->debug==true){
$this->LSD_Php5_Counter2=$this->msg;
}else{
$this->create_Config();
$this->create_Counter();
}
}
if(($this->msg!="None")&&($this->debug==true)){
$this->LSD_Php5_Counter2=$this->msg;
if($this->rough_Response==true){
$this->send_Rough();
}
}
printf("%s",$this->LSD_Php5_Counter2);
$this->reset_Config();
}
}
?>
