<?php
/**
 * Fichier de configuration document� du compteur de visites LSD_Php5_Counter2.php
 *
 * PHP version 5
 *
 * LICENSE: Ce script vous est gracieusement offert par The Liberated Seven Dwarfs et est libre de tout droit
 * d'utilisation priv�e et non commerciale sous la restriction de conserver le pr�sent ent�te et de respecter
 * la licence Creative Commons : By-Nc-Sa.
 *
 * @category Web utilities
 * @package the_LSD_Php5_Counter2
 * @author The Liberated Seven Dwarfs
 * @copyright 2008-2009 Advanced Software Solutions Inc.
 * @license Creative-Commons_By-Nc-Sa - http://creativecommons.org/licenses/by-nc-sa/2.0/fr/
 * @link http://theliberated7dwarfs.as2.com
 * @version 2.0.0.a - February 8, 2009
 * @filesource default_Counter2.inc
 */

/**
 * Quatri�me et derni�re partie du script LSD_Php5_Counter2.php - Les param�tres de configuration du compteur.
 */

/**
 * Attributs communs au deux modes (texte et graphique).
 */

/**
 * L'attribut $this->session_Id d�fini la variable de session du compteur.
 * Cet attribut est utilis� pour ne pas comptabiliser plusieurs fois la m�me page � la suite de rafraichissements
 * de la page.
 * @var string
 */
$this->session_Id="default_Counter2";

/**
 * L'attribut $this->file_Path indique le chemin absolu du r�pertoire qui contient le fichier des visites par rapport
 * � la racine du site.
 * Dans la configuration par d�faut, ce r�pertoire est le sous-r�pertoire "hits/" du r�pertoire contenant le script.
 * Exemple de modification de cet attribut : $this->file_Path="my_Data/my_Counters/";
 * REMARQUE : Ne pas supprimer l'attribut $this->script_Path. plac� avant la valeur de l'attribut $this->file_Path et
 * ne pas oublier le signe slash (/) � la fin du nom du r�pertoire.
 * @var string
 */
$this->file_Path=$this->script_Path."hits/";

/**
 * L'attribut $this->f_Name indique le nom du fichier des visites. Dans la configuration par d�faut, ce nom est
 * "my_LSD_Php5_Counter2".
 * Exemple de modification de cet attribut : $this->file_Name="compteur_rouge"
 * @var string
 */
$this->file_Name="default_Counter2";

/**
 * L'attribut $this->file_Ext indique le nom de l'extension du fichier des visites.
 * Exemple de modification de cet attribut : $this->file_Ext="txt";
 * @var string
 */
$this->file_Ext="nbr";

/**
 * L'attribut $this->init indique le nombre de visites � ins�rer dans le fichier lors de la cr�ation du fichier de
 * d�compte des visites.
 * Exemple de modification de cet attribut : $this->init="100";
 * @var int
 */
$this->init="0";

/**
 * L'attribut $this->cheat permet de "tricher" l'incr�mentation du nombre des visites � chaque visite!
 * Exemple de modification de cet attribut : $this->cheat="9"; augmentera le nombre de visites de 10 � chaque visite.
 * @var int
 */
$this->cheat="0";

/**
 * L'attribut $this->hidden indique si le compteur est visible ou cach�.
 * Valeurs admises : "true" pour cach� et "false" pour visible.
 * @var boolean
 */
$this->hidden=false;

/**
 * L'attribut $this->mode indique si le compteur est en mode texte ou en mode graphique.
 * Valeurs admises : T (ou t) pour le mode texte et G (ou g) pour le mode graphique.
 * @var char
 */
$this->mode="G";

/**
 * L'attribut $this->pad indique le nombre de chiffres � afficher par d�faut dans le compteur.
 * Ne pas d�passer 15 qui est la valeur maximale lue par le compteur !
 * REMARQUE : Si cet attribut a une valeur sup�rieure � 0, le compteur en mode texte n'affichera pas les attributs
 * $this->t_0, t_1, t_2 et t_3.
 *  * @var integer
 */
$this->pad="5";

/**
 * Attributs sp�cifiques du mode texte.
 */

/**
 * L'attribut $this->color d�fini la couleur du mode texte.
 * Exemple de modification de cet attribut : $this->t_color="0033FF"; pour obtenir un texte �crit en bleu.
 * D�finir $this->t_color="inherit"; pour utiliser la couleur de la fonte utilis�e dans la page.
 * @var string
 */
$this->color="inherit";

/**
 * L'attribut $this->b_Color d�fini la couleur de fond du mode texte.
 * Exemple de modification de cet attribut : $this->b_color="FFFF99"; pour obtenir un fond jaune clair.
 * D�finir $this->b_color="none"; pour obtenir un fond transparent.
 * @var string
 */
$this->b_Color="none";

/**
 * L'attribut $this->f_Fam d�fini la famille de fontes du mode texte.
 * Exemple $this->f_Fam="Script"; ou encore $this->f_Fam="Verdana, Arial, Helvetica, sans-serif";
 * D�finir $this->f_Fam="inherit" pour utiliser la fonte utilis�e par d�faut dans la page.
 * @var string
 */
$this->f_Fam="inherit";

/**
 * L'attribut $this->size d�fini la taille du mode texte exprim�e en pixel.
 * Exemple $this->size="30"; pour �crire un texte de 30 pixels.
 * D�finir $this->size="inherit"; pour utiliser la taille de la fonte utilis�e par d�faut dans la page.
 * @var string
 */
$this->size="inherit";

/**
 * L'attribut $this->t_0 d�fini le texte plac� avant le compteur en mode texte s'il n'y a pas eu de visite.
 * @var string
 */
$this->t_0="Il n'y a pas encore eu de visite.";

/**
 * L'attribut $this->t_1 d�fini le texte plac� avant le compteur en mode texte.
 * @var string
 */
$this->t_1="Vous &ecirc;tes le ";

/**
 * L'attribut $this->t_2 d�fini le texte plac� apr�s le compteur en mode texte si le nombre de hit est 1.
 * @var string
 */
$this->t_2="er visiteur.";

/**
 * L'attribut $this->t_3 d�fini le texte plac� apr�s le compteur en mode texte si le nombre de hit est
 * sup�rieur � 1.
 * @var string
 */
$this->t_3="&egrave;me visiteur.";

/**
 * Attributs sp�cifiques du mode graphique.
 */

/**
 * L'attribut $this->i_Path indique le chemin absolu du r�pertoire qui contient les images du compteur par rapport
 * � la racine du site.
 * Dans la configuration par d�faut, ce r�pertoire est plac� juste en dessous du r�pertoire qui contient le script.
 * Exemple de modification de cet attribut : $this->i_Path = "data/my_images/png/";
 * REMARQUE : Ne pas supprimer l'attribut $this->script_Path. plac� avant la valeur de l'attribut $this->i_Path et
 * ne pas oublier le signe slash (/) � la fin du nom du r�pertoire.
 * @var string
 */
$this->i_Path=$this->script_Path."pictures/default/";

/**
 * L'attribut $this->i_Name d�fini le nom � ajouter devant le nom des images pour obtenir les noms complets des
 * fichiers d'images.
 * Exemple $this->i_Name="Img"; donnera (my_Img0, my_Img1, ..., my_Img9)
 * @var string
 */
$this->i_Name="default_";

/**
 * L'attribut this->i_Ext d�fini l'extension du nom de fin d'image.
 * D�finir $this->i_Ext="jpg"; si les images sont au format "jpeg" ou $this->i_Ext="bmp"; pour des bitmaps.
 * RTEMARQUES : Toutes les images d'un m�me compteur doivent avoir le m�me format et donc avoir la m�me extension,
 * et donc �tre du m�me type.
 * @var string
 */
$this->i_Ext="gif";

/**
 * L'attribut $this->i_H d�fini la hauteur des images des chiffres exprim�e en pixel.
 * @var integer
 */
$this->i_H="30";

/**
 * L'attribut $this->i_W d�fini la largeur des images des chiffres exprim�e en pixel.
 * @var integer
 */
$this->i_W="20";

/**
 * L'attribut $this->i_Border indique s'il faut entourer le compteur avec des images de d�part et de fin
 * de compteur.
 * Valeurs admises : "false" pour non et "true" pour oui.
 * @var boolean
 */
$this->i_Border=true;

/**
 * L'attribut $this->i_Lft d�fini le nom de l'image de gauche du compteur.
 * @var string
 */
$this->i_Lft="Left";

/**
 * L'attribut $this->i_Lft_H d�fini la hauteur de l'image de gauche exprim�e en pixel.
 * @var integer
 */
$this->i_Lft_H="30";

/**
 * L'attribut $this->i_Lft_W d�fini la largeur de l'image de gauche exprim�e en pixel.
 * @var integer
 */
$this->i_Lft_W="15";

/**
 * L'attribut $this->i_Rgt1 d�fini le nom de l'image de droite n� 1 du compteur si le nombre de hits est
 * inf�rieur � 2.
 * @var string
 */
$this->i_Rgt1="Right1";

/**
 * L'attribut $this->i_Rgt1_H d�fini la hauteur de l'image de droite n� 1 exprim�e en pixel.
 * @var integer
 */
$this->i_Rgt1_H="30";

/**
 * L'attribut $this->i_Rgt1_W d�fini la largeur de l'image de droite n� 1 exprim�e en pixel.
 * @var integer
 */
$this->i_Rgt1_W="90";

/**
 * L'attribut $this->i_Rgt2 d�fini le nom de l'image de droite n� 2 du compteur si le nombre de hits est
 * sup�rieur � 1.
 * @var string
 */
$this->i_Rgt2="Right2";

/**
 * L'attribut $this->i_Rgt2_H d�fini la hauteur de l'image de droite n� 2 exprim�e en pixel.
 * @var integer
 */
$this->i_Rgt2_H="30";

/**
 * L'attribut $this->i_Rgt2_W d�fini la largeur de l'image de droite n� 2 exprim�e en pixel.
 * @var integer
 */
$this->i_Rgt2_W="90";

/**
 * NOTE :
 * ======
 *
 * La modification de ces attributs n'est utile que dans le cas de la personnalisation de la pr�sentation, ou dans le
 * cadre de l'utilisation de plusieurs compteurs utilisant chacun, soit un fichier de d�compte de diff�rent, soit un
 * affichage diff�rent.
 *
 * Exemple : L'utilisation d'un compteur g�n�ral visible dans la page d'accueil de votre site web, et l'utilisation
 * d'un compteur cach� dans chacune des pages de votre site web, chacun de ces compteurs ayant son propre fichier de
 * d�compte de visites. Cela est particuli�rement utile pour mesurer la fr�quentation des diff�rentes pages d'un site.
 *
 * Apr�s la modification de ces attributs, n'h�sitez pas � utiliser la page documented_LSD_Php5_Counter2_Test_Page.php
 * pour v�rifier le r�sultat de vos modifications.
 *
 * REMARQUE : D'une part, les modifications ne doivent exclusivement �tre effectu�es qu'� l'int�rieur des signes " " sous
 * peine de provoquer un message d'erreur lors du contr�le des attributs, et d'autre part, ces modifications doivent
 * �tre r�alistes sous peine de provoquer des affichages incoh�rents.
 */
?>