<?php
/**
 * Fichier de configuration du compteur de visites LSD_Php5_Counter2.php
 *
 * PHP version 5
 *
 * LICENSE: Ce script vous est gracieusement offert par The Liberated Seven Dwarfs et est libre de tout droit
 * d'utilisation priv�e et non commerciale sous la restriction de conserver le pr�sent ent�te et de respecter
 * la licence Creative Commons : By-Nc-Sa.
 *
 * @category Web utilities
 * @package the_LSD_Php5_Counter2
 * @author The Liberated Seven Dwarfs
 * @copyright 2008-2009 Advanced Software Solutions Inc.
 * @license Creative-Commons_By-Nc-Sa - http://creativecommons.org/licenses/by-nc-sa/2.0/fr/
 * @link http://theliberated7dwarfs.as2.com
 * @version 2.0.0.a - February 8, 2009
 * @filesource white_Counter2.inc
 */
$this->session_Id="default_Counter2";
$this->file_Path=$this->script_Path."hits/";
$this->file_Name="default_Counter2";
$this->file_Ext="nbr";
$this->init="0";
$this->cheat="0";
$this->hidden=false;
$this->mode="G";
$this->pad="5";
$this->color="inherit";
$this->b_Color="none";
$this->f_Fam="inherit";
$this->size="inherit";
$this->t_0="Il n'y a pas encore eu de visite.";
$this->t_1="Vous &ecirc;tes le ";
$this->t_2="er visiteur.";
$this->t_3="&egrave;me visiteur.";
$this->i_Path=$this->script_Path."pictures/white/";
$this->i_Name="white";
$this->i_Ext="gif";
$this->i_H="30";
$this->i_W="20";
$this->i_Border=true;
$this->i_Lft="start";
$this->i_Lft_H="30";
$this->i_Lft_W="15";
$this->i_Rgt1="hit";
$this->i_Rgt1_H="30";
$this->i_Rgt1_W="90";
$this->i_Rgt2="hits";
$this->i_Rgt2_H="30";
$this->i_Rgt2_W="90";
?>