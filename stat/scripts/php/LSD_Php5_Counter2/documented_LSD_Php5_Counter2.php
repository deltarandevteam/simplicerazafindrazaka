<?php

/**
 * Compteur de visites (hit) param�trable utilisable soit depuis une page PHP5 soit depuis une page HTML via les
 * deux scripts LSD_Mouse_Events2.js et LSD_Stuff2.js
 *
 * PHP version 5
 *
 * LICENSE: Ce script vous est gracieusement offert par The Liberated Seven Dwarfs et est libre de tout droit
 * d'utilisation priv�e et non commerciale sous la restriction de conserver le pr�sent ent�te et de respecter
 * la licence Creative Commons : By-Nc-Sa.
 *
 * @category Web utilities
 * @package the_LSD_Php5_Counter2
 * @author The Liberated Seven Dwarfs
 * @copyright 2008-2009 Advanced Software Solutions Inc.
 * @license Creative-Commons_By-Nc-Sa - http://creativecommons.org/licenses/by-nc-sa/2.0/fr/
 * @link http://theliberated7dwarfs.as2.com
 * @version 2.0.0.a - February 8, 2009
 * @filesource documented_LSD_Php5_Counter2.php
 */

/**
 * Premi�re partie du script : Instencier l'objet compteur en mode Ajax
 */
new LSD_Ajax_Counter2();

/**
 * Seconde partie du script LSD_Counter2.php : Objet qui instencie l'objet compteur pour le mode Ajax via les
 * deux javascript LSD_Mouse_Events2.js et LSD_Ajax_Stuff2.js
 * Lors du chargement du script en mode Ajax via le script LSD_Mouse_Events.js, il faut passer les param�tres
 * suivants � l'objet compteur, les 3 premiers param�tres sont obligatoires et doivent �tre pass�s dans l'ordre
 * d�fini ci-dessous, tous les autres param�tres sont optionnels et sans ordre particulier � respecter :
 *
 * 1 - $ajax_Load, obligatoire, [doit �tre le mot "AJAX" pour activer ce mode]
 * 2 - $rough_Response, obligatoire, [doit �tre le mot "ROUGH" pour activer ce mode]
 * 3 - $ajax_Root, obligatoire m�me vide, [est le chemin relatif pour remonter � la racine du site. Exemple "../"]
 *
 * 4 - $ajax_Config, est le nom du fichier de configuration, sans chemin ni extention [optionnel mais recommand� car
 *     si le fichier de configuration n'est pas sp�cifi�, le compteur se comportera avec sa configuration par d�faut
 *     ou affichera un message d'erreur si le mode d'aide � la mise au point est activ�]
 * 5 - $ajax_Debug, doit �tre le mot "DEBUG" pour activer ce mode [optionnel]
 * 6 - $ajax_Mode, doit �tre la lettre "T" ou la lettre "G" [optionnel]
 * 7 - $ajax_Pad, n'a aucun effet si $ajax_Mode n'est pas initialis� en m�me temps [optionnel]
 * 8 - $ajax_Read, doit �tre le mot "TRUE" pour activer ce mode [optionnel]
 * 9 - $ajax_Hidden, doit �tre le mot "TRUE" pour activer ce mode (annul� si $ajax_Read a la valeur true)[optionnel]
 * 10 - $ajax_Session, doit �tre une cha�ne de caract�re non vide pour changer la variable de session [optionnel]
 */

/**
 * Objet qui intencie l'objet LSD_Php5_Counter2 en mode Ajax et qui affiche le r�sultat du compteur en tenant compte
 * des param�tres pass�s par la page HTML appelante via les deux javascripts LSD_Mouse_Events2.js et LSD_Ajax_Stuff2.js.
 */
class LSD_Ajax_Counter2
   {
      
      /**
       * constante over_Root : indique le chemin absolu du script par rapport � la racine du site
       */
      const over_Root="../../../";
      
      /**
       * constante script_Path :  indique le nom du chemin du script
       */
      const script_Path="scripts/php/LSD_Php5_Counter2/";
      
      /**
       * $ajax_Load : variable d'initialisation du mode de lancement Ajax de l'objet compteur.
       * @var string
       */
      private $ajax_Load;
      
      /**
       * $rough_Response : variable d'initialisation de la r�ponse sans balise HTML lorsque le mode de lancement Ajax est initialis�.
       * @var string
       */
      private $rough_Response;
      
      /**
       * $ajax_Root : variable du chemin � appliquer aux images lorsque le mode Ajax de l'objet compteur Ajax est initialis�.
       * @var string
       */
      private $ajax_Root;
      
      /**
       * $ajax_Debug : variable d'initialisation du mode de mise au point lorsque le mode Ajax de l'objet compteur est initialis�.
       * @var string
       */
      private $ajax_Debug;
      
      /**
       * $ajax_Config : variable du nom du fichier de configuration lorsque le mode Ajax de l'objet compteur est initialis�.
       * @var string
       */
      private $ajax_Config;
      
      /**
       * $ajax_Mode1 : variable du mode d'affichage du compteur lorsque le mode Ajax de l'objet compteur est initialis�.
       * @var string
       */
      private $ajax_Mode;
      
      /**
       * $ajax_Pad : variable du nombre de chiffres du compteur lorsque le mode Ajax de l'objet compteur est initialis�.
       * @var string
       */
      private $ajax_Pad;
      
      /**
       * ajax_Read : variable d'initialisation du mode lecture seule
       * @var string
       */
      private $ajax_Read;
      
      /**
       * ajax_Hidden : variable d'initialisation du mode cach�
       * @var string
       */
      private $ajax_Hidden;
      
      /**
       * ajax_Session : variable d'initialisation d'une nouvelle variable de session
       * @var string
       */
      private $ajax_Session;
      
      /**
       * ajax_New : variable d'initialisation d'un nouveau chemin du fichier des visites
       * @var string
       */
      private $ajax_New;
      
      /**
       * my_Ajax_Counter2 : variable d'initialisation d'une nouvelle extention du fichier des visites
       * @var string
       */
      private $my_Ajax_Counter2;
      
      /**
       * M�thode qui initialise les diff�rents attributs de l'objet LSD_Ajax_Counter2.
       *
       */
      function __construct()
         {
            /**
             * $this->ajax_Load : attribut d'initialisation du mode de lancement Ajax de l'objet compteur.
             * @var string
             */
            $this->ajax_Load="";
            /**
             * $this->rough_Response : attribut d'initialisation de la r�ponse sans balise HTML lorsque le mode de lancement Ajax est initialis�.
             * @var string
             */
            $this->rough_Response="";
            /**
             * $this->ajax_Root : attribut du chemin � appliquer aux images lorsque le mode Ajax de l'objet compteur Ajax est initialis�.
             * @var string
             */
            $this->ajax_Root="";
            /**
             * $this->ajax_Debug : attribut d'initialisation du mode de mise au point lorsque le mode Ajax de l'objet compteur est initialis�.
             * @var string
             */
            $this->ajax_Debug="";
            /**
             * $this->ajax_Config : attribut du nom du fichier de configuration lorsque le mode Ajax de l'objet compteur est initialis�.
             * @var string
             */
            $this->ajax_Config="";
            /**
             * $this->ajax_Mode1 : attribut du mode d'affichage du compteur lorsque le mode Ajax de l'objet compteur est initialis�.
             * @var string
             */
            $this->ajax_Mode="";
            /**
             * $this->ajax_Pad : attribut du nombre de chiffres du compteur lorsque le mode Ajax de l'objet compteur est initialis�.
             * @var string
             */
            $this->ajax_Pad="";
            /**
             * ajax_Read : attribut d'initialisation du mode lecture seule
             * @var string
             */
            $this->ajax_Read="";
            /**
             * ajax_Hidden : attribut d'initialisation du mode cach�
             * @var string
             */
            $this->ajax_Hidden="";
            /**
             * ajax_Session : attribut d'initialisation d'une nouvelle variable de session
             * @var string
             */
            $this->ajax_Session="";
            /**
             * my_Ajax_Counter2 : attribut qui re�oit le r�sultat de l'objet compteur.
             * @var string
             */
            $this->my_Ajax_Counter2="";
            /**
             * Si le script est lanc� avec le param�tre LSD_Load et que l'argument n'est pas vide
             */
            if(!empty($_REQUEST['LSD_Load']))
               {
                  /**
                   * alors, l'attribut $this->ajax_Mode re�oit l'argument pass� dans LSD_Load
                   */
                  $this->ajax_Load=$_REQUEST['LSD_Load'];
               }
            /**
             * Si le script est lanc� avec le param�tre LSD_Rough et que l'argument n'est pas vide
             */
            if(!empty($_REQUEST['LSD_Rough']))
               {
                  /**
                   * alors, l'attribut $this->rough_Response re�oit l'argument pass� dans LSD_Rough
                   */
                  $this->rough_Response=$_REQUEST['LSD_Rough'];
               }
            /**
             * Si le script est lanc� avec le param�tre LSD_Root et que l'argument n'est pas vide
             */
            if(!empty($_REQUEST['LSD_Root']))
               {
                  /**
                   * alors l'attribut $this->ajax_Root re�oit l'argument pass� dans LSD_Root
                   */
                  $this->ajax_Root=$_REQUEST['LSD_Root'];
               }
            /**
             * Si le script est lanc� avec le param�tre LSD_Debug et que l'argument n'est pas vide
             */
            if(!empty($_REQUEST['LSD_Debug']))
               {
                  /**
                   * alors l'attribut $this->ajax_Debug re�oit l'argument pass� dans LSD_Debug
                   */
                  $this->ajax_Debug=$_REQUEST['LSD_Debug'];
               }
            /**
             * Si le script est lanc� avec le param�tre LSD_Config et que l'argument n'est pas vide
             */
            if(!empty($_REQUEST['LSD_Config']))
               {
                  /**
                   * alors l'attribut $this->ajax_Config re�oit l'argument pass� dans LSD_Config
                   */
                  $this->ajax_Config=$_REQUEST['LSD_Config'];
               }
            /**
             * Si le script est lanc� avec le param�tre LSD_Mode et que l'argument n'est pas vide
             */
            if(!empty($_REQUEST['LSD_Mode']))
               {
                  /**
                   * alors, l'attribut $this->ajax_Mode re�oit l'argument pass� dans LSD_Mode
                   */
                  $this->ajax_Mode=$_REQUEST['LSD_Mode'];
               }
            /**
             * Si le script est lanc� avec le param�tre LSD_Pad et que l'argument n'est pas vide
             */
            if(isset($_REQUEST['LSD_Pad']))
               {
                  /**
                   * alors, l'attribut $this->ajax_Pad re�oit l'argument pass� dans LSD_Pad
                   */
                  $this->ajax_Pad=$_REQUEST['LSD_Pad'];
                  /**
                   * si l'attribut $this->ajax_Pad est une cha�ne de caract�res
                   */
                  if(is_string($this->ajax_Pad))
                     {
                        /**
                         * alors, l'attribut $this->ajax_Pad prend soit une valeur diff�rente de 0, soit la valeur "0"
                         */
                        $this->ajax_Pad=($this->ajax_Pad!="0")?$this->ajax_Pad:"0";
                     }
               }
            /**
             * Si le script est lanc� avec le param�tre LSD_Read et que l'argument n'est pas vide
             */
            if(!empty($_REQUEST['LSD_Read']))
               {
                  /**
                   * alors l'attribut $this->ajax_Read re�oit l'argument pass� dans LSD_Read
                   */
                  $this->ajax_Read=$_REQUEST['LSD_Read'];
               }
            /**
             * Si le script est lanc� avec le param�tre LSD_Session et que l'argument n'est pas vide
             */
            if(!empty($_REQUEST['LSD_Session']))
               {
                  /**
                   * alors l'attribut $this->ajax_Session re�oit l'argument pass� dans LSD_Session
                   */
                  $this->ajax_Session=$_REQUEST['LSD_Session'];
               }
            /**
             * Si le script est lanc� avec le param�tre LSD_Hidden et que l'argument n'est pas vide
             */
            if(!empty($_REQUEST['LSD_Hidden']))
               {
                  /**
                   * alors l'attribut $this->ajax_Hidden re�oit l'argument pass� dans LSD_Hidden
                   */
                  $this->ajax_Hidden=$_REQUEST['LSD_Hidden'];
               }
            /**
             * appeler la m�thode start_LSD_Counter2()
             */
            $this->start_LSD_Counter2();
         }
      
      /**
       * M�thode qui instencie l'objet compteur en mode Ajax
       */
      private function start_LSD_Counter2()
         {
            /**
             * Si l'attribut $this->ajax_Load est le mot "ajax"
             */
            if((strtolower($this->ajax_Load)=='ajax'))
               {
                  /**
                   * alors, lancer une session
                   */
                  session_start();
                  /**
                   * indiquer le nom du fichier de configuration
                   */
                  $this->config_Name='default_Counter2';
                  /**
                   * instencier l'objet compteur
                   */
                  $this->my_Ajax_Counter2=new LSD_Php5_Counter2(self::over_Root,self::script_Path,$this->config_Name);
                  /**
                   * activer le mode ajax
                   */
                  $this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Ajax_Load(true,$this->ajax_Root);
                  /**
                   * puis tester les param�tres pass�s � l'objet
                   */
                  $this->check_Params();
                  /**
                   * puis afficher le compteur � l'aide de la m�thode get_LSD_Php5_Counter2_Hit_Result() :
                   */
                  $this->my_Ajax_Counter2->get_LSD_Php5_Counter2_Hit_Result();
               }
            /**
             * sinon (donc l'attribut $this->ajax_Mode n'est pas le mot "ajax"),
             */
            else
               {
                  /**
                   * alors forcer la valeur de l'attribut $this->ajax_Load � vide ("")
                   */
                  $this->ajax_Load='';
               }
         }
      
      /**
       * M�thode qui teste les param�tres pass�s � l'objet LSD_Ajax_Counter2.
       */
      private function check_Params()
         {
            /**
             * si l'attribut $this->ajax_Debug est le mot "debug"
             */
            if((strtolower($this->ajax_Debug)=='debug'))
               {
                  /**
                   * alors activer le mode debug � l'aide de la m�thode set_LSD_Php5_Counter2_Debug_Mode()
                   */
                  $this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Debug_Mode(true);
               }
            /**
             * si l'attribut $this->ajax_Config n'est pas vide
             */
            if((!empty($this->ajax_Config))&&($this->ajax_Config!=''))
               {
                  /**
                   * alors, changer le fichier de configuration � l'aide de la m�thode set_LSD_Php5_Counter2_Config_Name()
                   */
                  $this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Config_Name($this->ajax_Config);
               }
            /**
             * si la param�tre $this->rough_Response est le mot "rough"
             */
            if((strtolower($this->rough_Response)=='rough'))
               {
                  /**
                   * alors activer le mode d'affichage sans balise HTML � l'aide de la m�thode set_LSD_Php5_Counter2_Rough_Response()
                   */
                  $this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Rough_Response(true);
               }
            /**
             * si la param�tre $this->ajax_Read est le mot "true"
             */
            if((strtolower($this->ajax_Read)=='true'))
               {
                  /**
                   * alors activer le mode lecture seule � l'aide de la m�thode set_LSD_Php5_Counter2_Read_Only()
                   */
                  $this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Read_Only(true);
               }
            /**
             * si la param�tre $this->ajax_Hidden est le mot "true"
             */
            if((strtolower($this->ajax_Hidden)=='true'))
               {
                  /**
                   * alors activer le mode cach� � l'aide de la m�thode set_LSD_Php5_Counter2_Hidden_Result()
                   */
                  $this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Hidden_Result(true);
               }
            /**
             * si l'attribut $this->ajax_Session est une cha�ne de caract�res non vide et non nulle
             */
            if((!empty($this->ajax_Session))&&(is_string($this->ajax_Session))&&($this->ajax_Session!=""))
               {
                  /**
                   * alors changer l'attribut de session � l'aide de la m�thode set_LSD_Php5_Counter2_Session_Id()
                   */
                  $this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Session_Id($this->ajax_Session);
               }
            /**
             * si l'attribut $this->ajax_Mode est la lettre "T" ou la lettre "G"
             */
            if((strtoupper($this->ajax_Mode)==='T')||(strtoupper($this->ajax_Mode)==='G'))
               {
                  /**
                   * alors changer le mode d'affichage � l'aide de la m�thode set_LSD_Php5_Counter2_Response_Mode()
                   */
                  $this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Response_Mode($this->ajax_Mode);
               }
            /**
             * si l'attribut $this->ajax_Pad n'est pas vide et n'est pas null
             */
            if((isset($this->ajax_Pad))&&($this->ajax_Pad!='')&&($this->ajax_Pad!=null))
               {
                  /**
                   * alors changer le nombre de digit du compteur � l'aide de la m�thode set_LSD_Php5_Counter2_Pad_Number()
                   */
                  $this->my_Ajax_Counter2->set_LSD_Php5_Counter2_Pad_Number($this->ajax_Pad);
               }
         }
   }

/**
 * Troisi�me partie du script LSD_Php5_Counter2.php - L'objet compteur.
 */
/**
 * Objet PHP5 qui affiche un compteur de visites (hit) param�trable en mode texte ou en mode graphique.
 * @return - affiche soit un compteur, soit un message d'erreur si la variable "$debug" a �t� initialis�e � "true" avant l'appel du compteur.
 */
class LSD_Php5_Counter2
   {
      
      /**
       * bi : constante de l'objet compteur utilis�e pour construire le d�but de la balise HTML d'une image
       */
      const bi="<img src=\"";
      
      /**
       * mi : constante de l'objet compteur utilis�e pour construire le milieu de la balise HTML d'une image
       */
      const mi="\"";
      
      /**
       * ei : constante de l'objet compteur utilis�e pour construire la fin de la balise HTML d'une image
       */
      const ei="/>";
      
      /**
       * h : constante de l'objet compteur utilis�e pour construire le d�but de la balise HTML de la hauteur d'une image
       */
      const h="height=\"";
      
      /**
       * w : constante de l'objet compteur utilis�e pour construire le d�but de la balise HTML de la largeur d'une image
       */
      const w="width=\"";
      
      /**
       * p : constante de l'objet compteur utilis�e pour construire la fin de la balise HTML de la dimention d'une image
       */
      const p="px\"";
      
      /**
       * bt : constante de l'objet compteur utilis�e pour construire le d�but de la balise HTML d'un texte
       */
      const bt="<span style=\"";
      
      /**
       * mt : constante de l'objet compteur utilis�e pour construire le milieu de la balise HTML d'un texte
       */
      const mt="\">";
      
      /**
       * et : constante de l'objet compteur utilis�e pour construire la fin de la balise HTML d'un texte
       */
      const et="</span>";
      
      /**
       * s : constante de l'objet compteur utilis�e pour �crire le signe ;
       */
      const s=";";
      
      /**
       * ff : constante de l'objet compteur utilis�e pour construire le d�but de la balise HTML de la famille de fonte d'un texte
       */
      const ff=" font-family:";
      
      /**
       * fs : constante de l'objet compteur utilis�e pour construire le d�but de la balise HTML de la taille d'un texte
       */
      const fs=" font-size:";
      
      /**
       * cp : constante de l'objet compteur utilis�e pour construire le chemin des fichiers de configuration
       */
      const cp="config/";
      
      /**
       * ce : constante de l'objet compteur utilis�e pour l'extention du nom des fichiers de configuration
       */
      const ce=".inc";
      
      /**
       * fp : constante de l'objet compteur utilis�e pour le chemin des fichiers de vistes
       */
      const fp="hits/";
      
      /**
       * $debug : variable du mode de mise au point
       * @var boolean
       */
      private $debug;
      
      /**
       * $read_Only : variable de lecture seule du fichier des visites
       * @var boolean
       */
      private $read_Only;
      
      /**
       * $over_Root : variable de surcharge des chemins absolus
       * @var string
       */
      private $over_Root;
      
      /**
       * $config_Root : variable de sauvegarde du chemin � rajouter au chemin des images lorsque le mode ajax est activ�
       * @var string
       */
      private $config_Root;
      
      /**
       * $ajax : variable d'activation de l'appel du script par un Javasript pour retourner le compteur en mode Ajax
       * @var boolean
       */
      private $ajax;
      
      /**
       * $ajax_Root : chemin � rajouter au chemin des images lorsque le mode ajax est activ�
       * @var string
       */
      private $ajax_Root;
      
      /**
       * $msg : variable du message
       * @var string
       */
      private $msg;
      
      /**
       * $msg_Nb : variable du num�ro du message
       * @var string
       */
      private $msg_Nb;
      
      /**
       * $script_Path : variable du chemin du script
       * @var string
       */
      private $script_Path;
      
      /**
       * $config_Path : chemin du fichier de configuration
       * @var string
       */
      private $config_Path;
      
      /**
       * $config_Name : variable du nom du fichier de configuration
       * @var string
       */
      private $config_Name;
      
      /**
       * $changed_Config : variable de changement du nom du fichier de configuration
       * @var string
       */
      private $changed_Config;
      
      /**
       * $new_Config : nouveau nom du fichier de configuration
       * @var string
       */
      private $new_Config;
      
      /**
       * $file_Path : variable du chemin du fichier des visites
       * @var string
       */
      private $file_Path;
      
      /**
       * $file_Name : variable du nom du fichier des visites
       * @var string
       */
      private $file_Name;
      
      /**
       * $changed_Name : variable de changement du nom du nouveau fichier des visites
       * @var string
       */
      private $changed_Name;
      
      /**
       * $cnew_Name : nom du nouveau fichier des visites
       * @var string
       */
      private $new_Name;
      
      /**
       * $session_Id : variable de la variable de session
       * @var string
       */
      private $session_Id;
      
      /**
       * $new_Id : nouveau nom de variable de session
       * @var string
       */
      private $new_Id;
      
      /**
       * $changed_Id : variable de changement de la valeur de la variable de session
       * @var boolean
       */
      private $changed_Id;
      
      /**
       * $init : variable d'initialisation du compteur
       * @var string
       */
      private $init;
      
      /**
       * $cheat : variable de triche de comptabilisation des visites
       * @var string
       */
      private $cheat;
      
      /**
       * $hidden : variable d'affichage du compteur
       * @var boolean
       */
      private $hidden;
      
      /**
       * $changed_Hidden : variable de changement d'affichage du compteur
       * @var boolean
       */
      private $changed_Hidden;
      
      /**
       * $mode : variable du mode d'affichage du compteur (texte ou graphique)
       * @var char
       */
      private $mode;
      
      /**
       * $changed_Mode : variable de changement du mode d'affichage du compteur (texte ou graphique)
       * @var char
       */
      private $changed_Mode;
      
      /**
       * $default_Mode : variable initiale du nom du mode d'affichage
       * @var string
       */
      private $default_Mode;
      
      /**
       * $new_Mode : nouveau mode d'affichage du compteur
       * @var char
       */
      private $new_Mode;
      
      /**
       * $pad : variable du nombre de chiffres du compteur (texte ou graphique)
       * @var string
       */
      private $pad;
      
      /**
       * $changed_Pad : variable de changement du nombre de chiffres du compteur (texte ou graphique)
       * @var string
       */
      private $changed_Pad;
      
      /**
       * $default_Pad : variable initiale du nombre de digit du compteur
       * @var string
       */
      private $default_Pad;
      
      /**
       * $new_Pad : nouveau nombre de chiffres du compteur (texte ou graphique)
       * @var string
       */
      private $new_Pad;
      
      /**
       * $color : variable de la couleur d'avant plan du texte du compteur texte
       * @var string
       */
      private $color;
      
      /**
       * $b_Color : variable de la couleur de fond du texte du compteur texte
       * @var string
       */
      private $b_Color;
      
      /**
       * $f_Fam : variable de la f_Fame du texte du compteur texte
       * @var string
       */
      private $f_Fam;
      
      /**
       * $size : variable de la taille du texte du compteur texte
       * @var string
       */
      private $size;
      
      /**
       * $t_0 : variable du texte affich� s'il n'y a pas eu de visite (compteur en mode texte)
       * @var string
       */
      private $t_0;
      
      /**
       * $t_1 : variable du texte affich� avant le nombre de visites (compteur en mode texte)
       * @var string
       */
      private $t_1;
      
      /**
       * $t_2 : variable du texte affich� apr�s le nombre de visites s'il n'y a eu qu'une seule visite (compteur en mode texte)
       * @var string
       */
      private $t_2;
      
      /**
       * $t_3 : variable du texte affich� apr�s le nombre de visites s'il n'y a eu plus d'une visite (compteur en mode texte)
       * @var string
       */
      private $t_3;
      
      /**
       * $i_Path : variable du chemin des images du compteur graphique
       * @var string
       */
      private $i_Path;
      
      /**
       * $i_Name : variable du nom des images du compteur graphique
       * @var string
       */
      private $i_Name;
      
      /**
       * $i_Ext : variable de l'extention des images du compteur graphique
       * @var string
       */
      private $i_Ext;
      
      /**
       * $i_H : variable de la hauteur des images des chiffres du compteur graphique
       * @var string
       */
      private $i_H;
      
      /**
       * $i_W : variable de la largeur des images des chiffres du compteur graphique
       * @var string
       */
      private $i_W;
      
      /**
       * $i_border : variable d'affichage des images de bord du compteur graphique
       * @var boolean
       */
      private $i_Border;
      
      /**
       * $i_Lft : variable du nom de l'image du bord gauche du compteur graphique
       * @var string
       */
      private $i_Lft;
      
      /**
       * $i_Lft_H : variable de la hauteur de l'image du bord gauche du compteur graphique
       * @var string
       */
      private $i_Lft_H;
      
      /**
       * $i_Lft_H : variable de la largeur de l'image du bord gauche du compteur graphique
       * @var string
       */
      private $i_Lft_W;
      
      /**
       * $i_Rgt1 : variable du nom de l'image du bord droit n�1 du compteur graphique affich�e lorsqu'il y a eu moins de 2 visites
       * @var string
       */
      private $i_Rgt1;
      
      /**
       * $i_Rgt1_H : variable de la hauteur de l'image du bord droit n�1 du compteur graphique
       * @var string
       */
      private $i_Rgt1_H;
      
      /**
       * $i_Rgt1_W : variable de la largeur de l'image du bord droit n�1 du compteur graphique
       * @var string
       */
      private $i_Rgt1_W;
      
      /**
       * $i_Rgt2 : variable du nom de l'image du bord droit n�2 du compteur graphique affich�e lorsqu'il y a eu plus d'une visite
       * @var string
       */
      private $i_Rgt2;
      
      /**
       * $i_Rgt2_H : variable de la hauteur de l'image du bord droit n�2 du compteur graphique
       * @var string
       */
      private $i_Rgt2_H;
      
      /**
       * $i_Rgt2_W : variable de la largeur de l'image du bord droit n�2 du compteur graphique
       * @var string
       */
      private $i_Rgt2_W;
      
      /**
       * $c : cha�ne de caract�res de balise HTML de la couleur d'avant plan du texte
       * @var string
       */
      private $c;
      
      /**
       * $b : cha�ne de caract�res de balise HTML de la couleur de fond du texte
       * @var string
       */
      private $b;
      
      /**
       * $t : cha�ne de caract�res de fin balise HTML de la taille du texte
       * @var string
       */
      private $t;
      
      /**
       * $i : Indice de boucle
       * @var integer
       */
      private $i;
      
      /**
       * $hit_File : fichier des visites
       * @var string
       */
      private $hit_File;
      
      /**
       * $n_Hit : nombre de visites
       * @var integer
       */
      private $n_Hit;
      
      /**
       * $n_Str : transformation du nombre de visites en cha�ne de caract�res
       * @var string
       */
      private $n_Str;
      
      /**
       * $LSD_Php5_Counter2 : cha�ne HTML qui repr�sente le compteur
       * @var string
       */
      private $LSD_Php5_Counter2;
      
      /**
       * $n_Digit : cha�ne HTML de concat�nation des nombres
       * @var string
       */
      private $n_Digit;
      
      /**
       * $rough_Response : variable d'activation du mode texte sans balise HTML lorsque le mode ajax est activ�
       * @var boolean
       */
      private $rough_Response;
      
      /**
       * M�thode constructeur de l'objet compteur.
       *
       * @param string $over_Root
       * @param string $script_Path
       * @param string $config_Name
       * @param boolean $read_Only
       */
      function __construct($over_Root,$script_Path,$config_Name)
         {
            /**
             * Appeler la m�thode qui initialise tous les attributs de l'objet compteur
             */
            $this->init_Attributes();
            /**
             * puis, appeler la m�thode $this->initial_Load($this->over_Root,$this->script_Path,$this->config_Name)
             */
            $this->initial_Load($over_Root,$script_Path,$config_Name);
         }

      /**
       * M�thode qui initialise tous les attributs de l'objet compteur avant son utilisation
       */
      private function init_Attributes()
         {
            $this->debug=false;
            $this->read_Only=false;
            $this->over_Root='';
            $this->config_Root='';
            $this->ajax=false;
            $this->ajax_Root='';
            $this->msg='None';
            $this->msg_Nb=0;
            $this->script_Path='';
            $this->config_Path='';
            $this->config_Name='';
            $this->changed_Config=false;
            $this->new_Config='';
            $this->file_Path='';
            $this->file_Name='';
            $this->changed_Name=false;
            $this->new_Name='';
            $this->session_Id='';
            $this->new_Id='';
            $this->changed_Id=false;
            $this->init=0;
            $this->cheat=0;
            $this->hidden=false;
            $this->changed_Hidden=false;
            $this->mode='';
            $this->changed_Mode=false;
            $this->default_Mode='';
            $this->new_Mode='';
            $this->pad='';
            $this->changed_Pad=false;
            $this->default_Pad='';
            $this->new_Pad='';
            $this->color='';
            $this->b_Color='';
            $this->f_Fam='';
            $this->size='';
            $this->t_0='';
            $this->t_1='';
            $this->t_2='';
            $this->t_3='';
            $this->i_Path='';
            $this->i_Name='';
            $this->i_Ext='';
            $this->i_H='';
            $this->i_W='';
            $this->i_Border='';
            $this->i_Lft='';
            $this->i_Lft_H='';
            $this->i_Lft_W='';
            $this->i_Rgt1='';
            $this->i_Rgt1_H='';
            $this->i_Rgt1_W='';
            $this->i_Rgt2='';
            $this->i_Rgt2_H='';
            $this->i_Rgt2_W='';
            $this->c='';
            $this->b='';
            $this->t='';
            $this->i=0;
            $this->hit_File='';
            $this->n_Hit=0;
            $this->n_Str='';
            $this->LSD_Php5_Counter2='';
            $this->n_Digit='';
            $this->rough_Response=false;
         }
      
      /**
       * M�thode qui teste l'existance du fichier de configuration et qui le charge s'il est trouv�
       */
      private function initial_Load($over_Root,$script_Path,$config_Name)
         {
            /**
             * Si la variable $script_Path n'est pas initialis�e,
             */
            if(!isset($script_Path))
               {
                  /**
                   * alors, cr�er le message suivant.
                   */
                  $this->create_Message(1);
               }
            /**
             * sinon
             */
            else
               {
                  /**
                   * alors, si le r�pertoire n'existe pas
                   */
                  if(!file_exists($over_Root.$script_Path))
                     {
                        /**
                         * alors, affecter la valeur de la variable $script_Path � l'attribut $this->script_Path
                         */
                        $this->script_Path=$script_Path;
                        /**
                         * puis, cr�er le message suivant,
                         */
                        $this->create_Message(2);
                        /**
                         * puis passer la valeur de l'attribut $this->script-Path � null.
                         */
                        $this->script_Path=null;
                     }
                  /**
                   * sinon (donc le r�pertoire existe)
                   */
                  else
                     {
                        /**
                         * L'attribut $this->script_Path est initialis� avec la valeur de la variable $script_Path pass�e en param�tre,
                         */
                        $this->script_Path=$script_Path;
                     }
               }
            /**
             * Si la variable $config_Name n'est pas initialis�e,
             */
            if(!isset($config_Name))
               {
                  /**
                   * alors, cr�er le message suivant.
                   */
                  $this->create_Message(3);
               }
            /**
             * sinon
             */
            else
               {
                  /**
                   * L'attribut $this->c_Name est initialis� avec la valeur de la variable $config_Name pass�e en param�tre,
                   */
                  $this->config_Name=$config_Name;
               }
            /**
             * Initialisation du sous-r�pertoire contenant le(s) fichier(s) de configuration.
             */
            $this->config_Path=$script_Path.self::cp;
            /**
             * Si l'attribut $over_Root n'a pas �t� initialis� dans la page qui app�le le script,
             */
            if(!isset($over_Root))
               {
                  /**
                   * alors, initialiser cet attribut a vide ('').
                   */
                  $this->over_Root='';
               }
            /**
             * sinon
             */
            else
               {
                  /**
                   * alors l'attribut $over_Root prend la valeur pass�e en param�tre.
                   */
                  $this->over_Root=$over_Root;
               }
            /**
             * Si le r�pertoire $this->script_Path.self::cp n'existe pas
             */
            if(!file_exists($this->over_Root.$this->config_Path))
               {
                  /**
                   * alors, cr�er le message suivant :
                   */
                  $this->create_Message(4);
               }
            /**
             * Si l'attribut $this->msg a toujours la valeur "None" et que le fichier de configuration n'est pas trouv�,
             */
            if(($this->msg=="None")&&(!file_exists($this->over_Root.$this->config_Path.$this->config_Name.self::ce)))
               {
                  /**
                   * alors, cr�er le message suivant.
                   */
                  $this->create_Message(5);
               }
            /**
             * si il n'y a pas d'erreur,
             */
            if($this->msg=="None")
               {
                  /**
                   * alors, charger le fichier de configuration.
                   */
                  require ($this->over_Root.$this->config_Path.$this->config_Name.self::ce);
               }
         }
      
      /**
       * M�thode qui assigne les messages d'erreur.
       */
      private function create_Message($msg_Nb)
         {
            switch($msg_Nb)
               {
                  case 0:
                     $this->msg=htmlentities("None");
                     break;
                  case 1:
                     $this->msg=htmlentities("Erreur #01:La variable du chemin du fichier script n'est pas initialis�e avant l'appel du script!");
                     break;
                  case 2:
                     $this->msg=htmlentities("Erreur #02:Le chemin du fichier script ".$this->over_Root.$this->script_Path." est introuvable!");
                     break;
                  case 3:
                     $this->msg=htmlentities("Erreur #03:La variable du nom de fichier de configuration n'est pas initialis�e avant l'appel du script!");
                     break;
                  case 4:
                     $this->msg=htmlentities("Erreur #04:Le r�pertoire ".$this->over_Root.$this->config_Path." est introuvable!");
                     break;
                  case 5:
                     $this->msg=htmlentities("Erreur #05:Le fichier de configuration ".$this->over_Root.$this->config_Path.$this->config_Name.self::ce." est introuvable!");
                     break;
                  case 6:
                     $this->msg=htmlentities("Erreur #06:L'attribut \$this->session_Id n'est pas initialis�!");
                     break;
                  case 7:
                     $this->msg=htmlentities("Erreur #07:L'attribut \$this->file_Path n'est pas initialis�!");
                     break;
                  case 8:
                     $this->msg=htmlentities("Erreur #08:L'attribut \$this->file_Name n'est pas initialis�!");
                     break;
                  case 9:
                     $this->msg=htmlentities("Erreur #09:L'attribut \$this->file_Ext n'est pas initialis�!");
                     break;
                  case 10:
                     $this->msg=htmlentities("Erreur #10:Le r�pertoire ".$this->over_Root.$this->file_Path." est introuvable!");
                     break;
                  case 11:
                     $this->msg=htmlentities("Erreur #11:L'attribut \$this->init n'est pas initialis�!");
                     break;
                  case 12:
                     $this->msg=htmlentities("Erreur #12:L'attribut \$this->cheat n'est pas initialis�!");
                     break;
                  case 13:
                     $this->msg=htmlentities("Erreur #13:L'attribut \$this->hidden n'est pas initialis�!");
                     break;
                  case 14:
                     $this->msg=htmlentities("Erreur #14:L'attribut \$this->mode n'est pas initialis�!");
                     break;
                  case 15:
                     $this->msg=htmlentities("Erreur #15:L'attribut \$this->pad n'est pas initialis�!");
                     break;
                  case 16:
                     $this->msg=htmlentities("Erreur #16:L'attribut \$this->color n'est pas initialis�!");
                     break;
                  case 17:
                     $this->msg=htmlentities("Erreur #17:L'attribut \$this->b_Color n'est pas initialis�!");
                     break;
                  case 18:
                     $this->msg=htmlentities("Erreur #18:L'attribut \$this->size n'est pas initialis�!");
                     break;
                  case 19:
                     $this->msg=htmlentities("Erreur #19:L'attribut \$this->f_Fam n'est pas initialis�!");
                     break;
                  case 20:
                     $this->msg=htmlentities("Erreur #20:L'attribut \$this->t_0 n'est pas initialis�!");
                     break;
                  case 21:
                     $this->msg=htmlentities("Erreur #21:L'attribut \$this->t_1 n'est pas initialis�!");
                     break;
                  case 22:
                     $this->msg=htmlentities("Erreur #22:L'attribut \$this->t_2 n'est pas initialis�!");
                     break;
                  case 23:
                     $this->msg=htmlentities("Erreur #23:L'attribut \$this->t_3 n'est pas initialis�!");
                     break;
                  case 24:
                     $this->msg=htmlentities("Erreur #24:L'attribut \$this->i_Path n'est pas initialis�!");
                     break;
                  case 25:
                     $this->msg=htmlentities("Erreur #25:Le r�pertoire ".$this->over_Root.$this->i_Path." est introuvable!");
                     break;
                  case 26:
                     $this->msg=htmlentities("Erreur #26:L'attribut \$this->i_Name n'est pas initialis�!");
                     break;
                  case 27:
                     $this->msg=htmlentities("Erreur #27:L'attribut \$this->i_Ext n'est pas initialis�!");
                     break;
                  case 28:
                     $this->msg=htmlentities("Erreur #28:L'attribut \$this->i_Border n'est pas initialis�!");
                     break;
                  case 29:
                     $this->msg=htmlentities("Erreur #29:L'attribut \$this->i_Lft n'est pas initialis�!");
                     break;
                  case 30:
                     $this->msg=htmlentities("Erreur #30:L'image ".$this->over_Root.$this->i_Path.$this->i_Name.$this->i_Lft.'.'.$this->i_Ext." est introuvable!");
                     break;
                  case 31:
                     $this->msg=htmlentities("Erreur #31:L'attribut \$this->i_Lft_H n'est pas initialis�!");
                     break;
                  case 32:
                     $this->msg=htmlentities("Erreur #32:L'attribut \$this->i_Lft_W n'est pas initialis�!");
                     break;
                  case 33:
                     $this->msg=htmlentities("Erreur #33:L'attribut \$this->i_Rgt1 n'est pas initialis�!");
                     break;
                  case 34:
                     $this->msg=htmlentities("Erreur #34:L'image ".$this->over_Root.$this->i_Path.$this->i_Name.$this->i_Rgt1.'.'.$this->i_Ext." est introuvable!");
                     break;
                  case 35:
                     $this->msg=htmlentities("Erreur #35:L'attribut \$this->i_Rgt1_H n'est pas initialis�!");
                     break;
                  case 36:
                     $this->msg=htmlentities("Erreur #36:L'attribut \$this->i_Rgt1_W n'est pas initialis�!");
                     break;
                  case 37:
                     $this->msg=htmlentities("Erreur #37:L'attribut \$this->i_Rgt2 n'est pas initialis�!");
                     break;
                  case 38:
                     $this->msg=htmlentities("Erreur #38:L'image ".$this->over_Root.$this->i_Path.$this->i_Name.$this->i_Rgt2.'.'.$this->i_Ext." est introuvable!");
                     break;
                  case 39:
                     $this->msg=htmlentities("Erreur #39:L'attribut \$this->i_Rgt2_H n'est pas initialis�!");
                     break;
                  case 40:
                     $this->msg=htmlentities("Erreur #40:L'attribut \$this->i_Rgt2_W n'est pas initialis�!");
                     break;
                  case 41:
                     $this->msg=htmlentities("Erreur #41:L'image ".$this->over_Root.$this->i_Path.$this->i_Name.$this->i.'.'.$this->i_Ext." est introuvable!");
                     break;
                  case 42:
                     $this->msg=htmlentities("Erreur #42:L'attribut \$this->i_H n'est pas initialis�!");
                     break;
                  case 43:
                     $this->msg=htmlentities("Erreur #43:L'attribut \$this->i_W n'est pas initialis�!");
                     break;
                  case 44:
                     $this->msg=htmlentities("Erreur #44:L'attribut \$this->mode n'est pas correctement initialis�!");
                     break;
                  case 45:
                     $this->msg=htmlentities("Erreur #45:Le param�tre \$this->mode n'est pas correctement initialis�!");
                     break;
                  case 46:
                     $this->msg=htmlentities("Erreur #46:Le chemin pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Ajax_Load() est invalide!");
                     break;
                  case 47:
                     $this->msg=htmlentities("Erreur #47:Le chemin pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Ajax_Load() en mode AJAX est invalide!");
                     break;
                  case 48:
                     $this->msg=htmlentities("Erreur #48:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Ajax_Load() est invalide!");
                     break;
                  case 49:
                     $this->msg=htmlentities("Erreur #49:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Ajax_Load() en mode AJAX est invalide!");
                     break;
                  case 50:
                     $this->msg=htmlentities("Erreur #50:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Config_Name() est invalide!");
                     break;
                  case 51:
                     $this->msg=htmlentities("Erreur #51:Le fichier de configuration pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Config_Name() en mode AJAX est introuvable!");
                     break;
                  case 52:
                     $this->msg=htmlentities("Erreur #52:La fichier de configuration pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Debug_Mode() est introuvable!!");
                     break;
                  case 53:
                     $this->msg=htmlentities("Erreur #53:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Debug_Mode() en mode AJAX est invalide!");
                     break;
                  case 54:
                     $this->msg=htmlentities("Erreur #54:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Hidden_Result() est invalide!");
                     break;
                  case 55:
                     $this->msg=htmlentities("Erreur #55:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Hidden_Result() en mode AJAX est invalide!");
                     break;
                  case 56:
                     $this->msg=htmlentities("Erreur #56:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Rough_Response() est invalide!");
                     break;
                  case 57:
                     $this->msg=htmlentities("Erreur #57:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Rough_Response() en mode AJAX est invalide!");
                     break;
                  case 58:
                     $this->msg=htmlentities("Erreur #58:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Read_Only() est invalide!");
                     break;
                  case 59:
                     $this->msg=htmlentities("Erreur #59:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Read_Only() en mode AJAX est invalide!");
                     break;
                  case 60:
                     $this->msg=htmlentities("Erreur #60:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Session_Id() est invalide!");
                     break;
                  case 61:
                     $this->msg=htmlentities("Erreur #61:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Session_Id() en mode AJAX est invalide!");
                     break;
                  case 62:
                     $this->msg=htmlentities("Erreur #62:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Response_Mode() est invalide!");
                     break;
                  case 63:
                     $this->msg=htmlentities("Erreur #63:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Response_Mode() en mode AJAX est invalide!");
                     break;
                  case 64:
                     $this->msg=htmlentities("Erreur #64:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Pad_Number() est invalide!");
                     break;
                  case 65:
                     $this->msg=htmlentities("Erreur #65:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Pad_Number() en mode AJAX est invalide!");
                     break;
                  case 66:
                     $this->msg=htmlentities("Erreur #66:Le fichier des visites avec le nom pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Hit_Name() est introuvable!");
                     break;
                  case 67:
                     $this->msg=htmlentities("Erreur #67:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Hit_Name() est invalide!");
                     break;
                  case 68:
                     $this->msg=htmlentities("Erreur #68:La variable pass�e en param�tre � la m�thode set_LSD_Php5_Counter2_Hit_Name() en mode AJAX est invalide!");
                     break;
                  default:
                     $this->msg=htmlentities("None");
                     break;
               }
         }
      
      /**
       * M�thode qui teste les attributs d�finis par le fichier de configuration s�lectionn�.
       */
      private function test_Config()
         {
            /**
             * Si l'attribut de session n'est pas initialis�e ou est vide,
             */
            if((!isset($this->session_Id))||(empty($this->session_Id)))
               {
                  /**
                   * alors, cr�er le message suivant :
                   */
                  $this->create_Message(6);
                  /**
                   * puis sortir de la m�thode test_Config().
                   */
                  return;
               }
            /**
             * Si l'attribut de chemin du fichier des visites n'est pas initialis�,
             */
            if(!isset($this->file_Path))
               {
                  /**
                   * alors, cr�er le message suivant :
                   */
                  $this->create_Message(7);
                  /**
                   * puis sortir de la m�thode test_Config().
                   */
                  return;
               }
            /**
             * Si l'attribut de nom du fichier des visites n'est pas initialis� ou est vide,
             */
            if((!isset($this->file_Name))||(empty($this->file_Name)))
               {
                  /**
                   * alors, cr�er le message suivant :
                   */
                  $this->create_Message(8);
                  /**
                   * puis sortir de la m�thode test_Config().
                   */
                  return;
               }
            /**
             * Si l'attribut de l'extention du nom du fichier des visites n'est pas initialis� ou est vide,
             */
            if((!isset($this->file_Ext))||(empty($this->file_Ext)))
               {
                  /**
                   * alors, cr�er le message suivant :
                   */
                  $this->create_Message(9);
                  /**
                   * puis sortir de la m�thode test_Config().
                   */
                  return;
               }
            /**
             * Si le r�pertoire du fichier de configuration n'existe pas
             */
            if(!file_exists($this->over_Root.$this->file_Path))
               {
                  /**
                   * alors, cr�er le message suivant :
                   */
                  $this->create_Message(10);
                  /**
                   * puis sortir de la m�thode test_Config().
                   */
                  return;
               }
            /**
             * Si l'attribut d'initialisation du compteur n'est pas initialis� ou est vide,
             */
            if((!isset($this->init))&&(empty($this->init)))
               {
                  /**
                   * alors, cr�er le message suivant :
                   */
                  $this->create_Message(11);
                  /**
                   * puis sortir de la m�thode test_Config().
                   */
                  return;
               }
            /**
             * Si l'attribut de triche du compteur n'est pas initialis� ou est vide,
             */
            if((!isset($this->cheat))&&(empty($this->cheat)))
               {
                  /**
                   * alors, cr�er le message suivant :
                   */
                  $this->create_Message(12);
                  /**
                   * puis sortir de la m�thode test_Config().
                   */
                  return;
               }
            /**
             * Si l'attribut d'affichage du compteur n'est pas initialis� ou est vide,
             */
            if((!isset($this->hidden))&&(empty($this->hidden)))
               {
                  /**
                   * alors, cr�er le message suivant :
                   */
                  $this->create_Message(13);
                  /**
                   * puis sortir de la m�thode test_Config().
                   */
                  return;
               }
            /**
             * Si l'attribut du mode d'affichage du compteur n'est pas initialis� ou est vide,
             */
            if((!isset($this->mode))||(empty($this->mode)))
               {
                  /**
                   * alors, cr�er le message suivant :
                   */
                  $this->create_Message(14);
                  /**
                   * puis sortir de la m�thode test_Config().
                   */
                  return;
               }
            /**
             * Si l'attribut d'affichage du nombre de chiffres du compteur graphique n'est pas initialis�,
             */
            if((!isset($this->pad))&&(empty($this->pad)))
               {
                  /**
                   * alors, cr�er le message suivant :
                   */
                  $this->create_Message(15);
                  /**
                   * puis sortir de la m�thode test_Config().
                   */
                  return;
               }
            /**
             * Si l'attribut $this->mode a la valeur "t" ou "T" (le compteur est en mode texte)
             */
            if(strtolower($this->mode)=="t")
               {
                  /**
                   * Si l'attribut de couleur du texte du compteur n'est pas initialis� ou est vide,
                   */
                  if((!isset($this->color))||(empty($this->color)))
                     {
                        /**
                         * alors, cr�er le message suivant :
                         */
                        $this->create_Message(16);
                        /**
                         * puis sortir de la m�thode test_Config().
                         */
                        return;
                     }
                  /**
                   * Si l'attribut de couleur de fond du texte du compteur n'est pas initialis� ou est vide,
                   */
                  if((!isset($this->b_Color))||(empty($this->b_Color)))
                     {
                        /**
                         * alors, cr�er le message suivant :
                         */
                        $this->create_Message(17);
                        /**
                         * puis sortir de la m�thode test_Config().
                         */
                        return;
                     }
                  /**
                   * Si l'attribut de taille du texte du compteur n'est pas initialis� ou est vide,
                   */
                  if((!isset($this->size))||(empty($this->size)))
                     {
                        /**
                         * alors, cr�er le message suivant :
                         */
                        $this->create_Message(18);
                        /**
                         * puis sortir de la m�thode test_Config().
                         */
                        return;
                     }
                  /**
                   * Si l'attribut de police du texte du compteur n'est pas initialis� ou est vide,
                   */
                  if((!isset($this->f_Fam))||(empty($this->f_Fam)))
                     {
                        /**
                         * alors, cr�er le message suivant :
                         */
                        $this->create_Message(19);
                        /**
                         * puis sortir de la m�thode test_Config().
                         */
                        return;
                     }
                  /**
                   * Si l'attribut du texte � afficher au lieu du compteur s'il n'y pas eu de visite n'est pas initialis�,
                   */
                  if(!isset($this->t_0))
                     {
                        /**
                         * alors, cr�er le message suivant :
                         */
                        $this->create_Message(20);
                        /**
                         * puis sortir de la m�thode test_Config().
                         */
                        return;
                     }
                  /**
                   * Si l'attribut du texte � afficher avant le nombre de visites n'est pas initialis�,
                   */
                  if(!isset($this->t_1))
                     {
                        /**
                         * alors, cr�er le message suivant :
                         */
                        $this->create_Message(21);
                        /**
                         * puis sortir de la m�thode test_Config().
                         */
                        return;
                     }
                  /**
                   * Si l'attribut du texte � afficher apr�s une seule visite n'est pas initialis�,
                   */
                  if(!isset($this->t_2))
                     {
                        /**
                         * alors, cr�er le message suivant :
                         */
                        $this->create_Message(22);
                        /**
                         * puis sortir de la m�thode test_Config().
                         */
                        return;
                     }
                  /**
                   * Si l'attribut du texte � afficher apr�s plusieurs visites n'est pas initialis�,
                   */
                  if(!isset($this->t_3))
                     {
                        /**
                         * alors, cr�er le message suivant :
                         */
                        $this->create_Message(23);
                        /**
                         * puis sortir de la m�thode test_Config().
                         */
                        return;
                     }
               }
            /**
             * sinon, si l'attribut $this->mode a la valeur "g" ou "G" (le compteur est en mode graphique)
             */
            elseif(strtolower($this->mode)=="g")
               {
                  /**
                   * alors, si l'attribut de chemin des images n'est pas initialis�,
                   */
                  if(!isset($this->i_Path))
                     {
                        /**
                         * alors, cr�er le message suivant :
                         */
                        $this->create_Message(24);
                        /**
                         * puis sortir de la m�thode test_Config().
                         */
                        return;
                     }
                  /**
                   * alors, si le r�pertoire des images n'est ps trouv�,
                   */
                  if(!file_exists($this->over_Root.$this->i_Path))
                     {
                        /**
                         * alors, cr�er le message suivant :
                         */
                        $this->create_Message(25);
                        /**
                         * puis sortir de la m�thode test_Config().
                         */
                        return;
                     }
                  /**
                   * alors, si l'attribut du nom g�n�rique des images n'est pas initialis� ou est vide,
                   */
                  if((!isset($this->i_Name))||(empty($this->i_Name)))
                     {
                        /**
                         * alors, cr�er le message suivant :
                         */
                        $this->create_Message(26);
                        /**
                         * puis sortir de la m�thode test_Config().
                         */
                        return;
                     }
                  /**
                   * alors, si l'attribut de l'extention du nom des fichiers images n'est pas initialis� ou est vide,
                   */
                  if((!isset($this->i_Ext))||(empty($this->i_Ext)))
                     {
                        /**
                         * alors, cr�er le message suivant :
                         */
                        $this->create_Message(27);
                        /**
                         * puis sortir de la m�thode test_Config().
                         */
                        return;
                     }
                  /**
                   * alors, si l'attribut d'affichage des bords du compteur graphique n'est pas initialis�,
                   */
                  if((!isset($this->i_Border))&&(empty($this->i_Border)))
                     {
                        /**
                         * alors, cr�er le message suivant :
                         */
                        $this->create_Message(28);
                        /**
                         * puis sortir de la m�thode test_Config().
                         */
                        return;
                     }
                  /**
                   * alors, si l'attribut $this->i_Border a la valeur "true",
                   */
                  if($this->i_Border==true)
                     {
                        /**
                         * alors, si l'attribut du nom de l'image de gauche du compteur graphique n'est pas initialis�,
                         */
                        if(!isset($this->i_Lft))
                           {
                              /**
                               * alors, cr�er le message suivant :
                               */
                              $this->create_Message(29);
                              /**
                               * puis sortir de la m�thode test_Config().
                               */
                              return;
                           }
                        /**
                         * alors, si l'image de gauche du compteur graphique n'est pas trouv�e,
                         */
                        if(!file_exists($this->over_Root.$this->i_Path.$this->i_Name.$this->i_Lft.'.'.$this->i_Ext))
                           {
                              /**
                               * alors, cr�er le message suivant
                               */
                              $this->create_Message(30);
                              /**
                               * puis sortir de la m�thode test_Config().
                               */
                              return;
                           }
                        /**
                         * alors, si l'attribut de la hauteur de l'image de gauche du compteur graphique n'est pas initialis�,
                         */
                        if(!isset($this->i_Lft_H))
                           {
                              /**
                               * alors, cr�er le message suivant :
                               */
                              $this->create_Message(31);
                              /**
                               * puis sortir de la m�thode test_Config().
                               */
                              return;
                           }
                        /**
                         * alors, si l'attribut de la largeur de l'image de gauche du compteur graphique n'est pas initialis�,
                         */
                        if(!isset($this->i_Lft_W))
                           {
                              /**
                               * alors, cr�er le message suivant :
                               */
                              $this->create_Message(32);
                              /**
                               * puis sortir de la m�thode test_Config().
                               */
                              return;
                           }
                        /**
                         * alors, si l'attribut de nom de l'image de droite n� 1 du compteur graphique n'est pas initialis�,
                         */
                        if(!isset($this->i_Rgt1))
                           {
                              /**
                               * alors, cr�er le message suivant :
                               */
                              $this->create_Message(33);
                              /**
                               * puis sortir de la m�thode test_Config().
                               */
                              return;
                           }
                        /**
                         * alors, si l'image de droite n� 1 du compteur graphique n'est pas trouv�e,
                         */
                        if(!file_exists($this->over_Root.$this->i_Path.$this->i_Name.$this->i_Rgt1.'.'.$this->i_Ext))
                           {
                              /**
                               * alors, cr�er le message suivant
                               */
                              $this->create_Message(34);
                              /**
                               * puis sortir de la m�thode test_Config().
                               */
                              return;
                           }
                        /**
                         * alors, si l'attribut de la hauteur de l'image de droite n� 1 du compteur graphique n'est pas initialis�,
                         */
                        if(!isset($this->i_Rgt1_H))
                           {
                              /**
                               * alors, cr�er le message suivant :
                               */
                              $this->create_Message(35);
                              /**
                               * puis sortir de la m�thode test_Config().
                               */
                              return;
                           }
                        /**
                         * alors, si l'attribut de la largeur de l'image de droite n� 1 du compteur graphique n'est pas initialis�,
                         */
                        if(!isset($this->i_Rgt1_W))
                           {
                              /**
                               * alors, cr�er le message suivant :
                               */
                              $this->create_Message(36);
                              /**
                               * puis sortir de la m�thode test_Config().
                               */
                              return;
                           }
                        /**
                         * alors, si l'attribut de nom de l'image de droite n� 2 du compteur graphique n'est pas initialis�,
                         */
                        if(!isset($this->i_Rgt2))
                           {
                              /**
                               * alors, cr�er le message suivant :
                               */
                              $this->create_Message(37);
                              /**
                               * puis sortir de la m�thode test_Config().
                               */
                              return;
                           }
                        /**
                         * alors, si l'attribut de nom de l'image de droite n� 2 est diff�rent de celui de l'image de droite n� 1
                         */
                        if($this->i_Rgt2!=$this->i_Rgt1)
                           {
                              /**
                               * alors, si l'image de droite n� 2 du compteur graphique n'est pas trouv�e,
                               */
                              if(!file_exists($this->over_Root.$this->i_Path.$this->i_Name.$this->i_Rgt2.'.'.$this->i_Ext))
                                 {
                                    /**
                                     * alors, cr�er le message suivant
                                     */
                                    $this->create_Message(38);
                                    /**
                                     * puis sortir de la m�thode test_Config().
                                     */
                                    return;
                                 }
                              /**
                               * alors, si l'attribut de la hauteur de l'image de droite n� 2 du compteur graphique n'est pas initialis�,
                               */
                              if(!isset($this->i_Rgt2_H))
                                 {
                                    /**
                                     * alors, cr�er le message suivant :
                                     */
                                    $this->create_Message(39);
                                    /**
                                     * puis sortir de la m�thode test_Config().
                                     */
                                    return;
                                 }
                              /**
                               * alors, si l'attribut de la largeur de l'image de droite n� 2 du compteur graphique n'est pas initialis�,
                               */
                              if(!isset($this->i_Rgt2_W))
                                 {
                                    /**
                                     * alors, cr�er le message suivant :
                                     */
                                    $this->create_Message(40);
                                    /**
                                     * puis sortir de la m�thode test_Config().
                                     */
                                    return;
                                 }
                           }
                     }
                  /**
                   * alors, faire une boucle pour tester l'existence des 10 images des chiffres du compteur.
                   */
                  for($this->i=0;$this->i<10;$this->i++)
                     {
                        /**
                         * si une image n'exsite pas,
                         */
                        if(!file_exists($this->over_Root.$this->i_Path.$this->i_Name.$this->i.'.'.$this->i_Ext))
                           {
                              /**
                               * alors, cr�er le message suivant :
                               */
                              $this->create_Message(41);
                              /**
                               * puis sortir de la m�thode test_Config().
                               */
                              return;
                           }
                     }
                  /**
                   * alors, si l'attribut de la hauteur des chiffres du compteur graphique n'est pas initialis�,
                   */
                  if(!isset($this->i_H))
                     {
                        /**
                         * alors, cr�er le message suivant :
                         */
                        $this->create_Message(42);
                        /**
                         * puis sortir de la m�thode test_Config().
                         */
                        return;
                     }
                  /**
                   * alors, si l'attribut de la largeur des chiffres du compteur graphique n'est pas initialis�,
                   */
                  if(!isset($this->i_W))
                     {
                        /**
                         * alors, cr�er le message suivant :
                         */
                        $this->create_Message(43);
                        /**
                         * puis sortir de la m�thode test_Config().
                         */
                        return;
                     }
               }
            /**
             * sinon (l'attribut $this->mode a une autre valeur que "t", "T", "g" ou "G")
             */
            else
               {
                  /**
                   * alors cr�er le message suivant :
                   */
                  $this->create_Message(44);
                  /**
                   * puis sortir de la m�thode test_Config().
                   */
                  return;
               }
         }
      
      /**
       * M�thode qui d�fini les param�tres de la configuration d'un objet compteur minimal en mode texte qui est affich� en cas d'erreur 
       * si le mode mise au point n'est pas actif.
       */
      private function create_Config()
         {
            /**
             * Attribut qui d�fini la variable de session du compteur.
             */
            $this->session_Id="Default_LSD_Php5_Counter2";
            /**
             * Dans la configuration par d�faut, le r�pertoire qui contient le fichier des visites est un sous-r�pertoire nomm� "hits/" 
             * situ� en dessous du r�pertoire du script.
             * Si ce r�pertoire n'existe pas, le r�pertoire est cr��.
             */
            if(!file_exists($this->over_Root.$this->script_Path.self::fp))
               {
                  /**
                   * cr�er le sous-r�pertoire.
                   */
                  mkdir($this->over_Root.$this->script_Path.self::fp,0777);
               }
            /**
             *Attribut qui indique le chemin du r�pertoire qui contient le fichier des visites par rapport � la page qui appele le script.
             */
            $this->file_Path=$this->script_Path.self::fp;
            /**
             * Attribut qui indique le nom du fichier des visites. Dans la configuration par d�faut, ce nom est "Default_LSD_Php5_Counter2".
             */
            $this->file_Name="Default_LSD_Php5_Counter2";
            /**
             * Attribut qui indique le nom de l'extention du fichier des visites. Dans la configuration par d�faut, cette extention est "nbr".
             */
            $this->file_Ext="nbr";
            /**
             * Attribut qui indique le nombre de visites � ins�rer dans le fichier lors de la cr�ation du fichier des visites.
             */
            $this->init="0";
            /**
             * Attribut de "triche" d'incr�mentation du nombre des visites � chaque visite!
             */
            $this->cheat="0";
            /**
             * Attribut qui indique si le compteur est visible ou cach�.
             */
            $this->hidden=false;
            /**
             * Attribut qui indique si le compteur est en mode texte ou en mode graphique.
             */
            $this->mode="T";
            /**
             * Attribut qui indique le nombre de chiffres � afficher par d�faut dans le compteur.
             */
            $this->pad="1";
            /**
             * Attribut qui d�fini la couleur du mode texte. Dans la configuration par d�faut, c'est la couleur de la f_Fame utilis�e dans la page.
             */
            $this->color="inherit";
            /**
             * Attribut qui d�fini la couleur de fond du mode texte. Dans la configuration par d�faut, c'est un fond transparent.
             */
            $this->b_Color="none";
            /**
             * Attribut qui d�fini la famille de fontes du mode texte. Dans la configuration par d�faut, c'est la f_Fame utilis�e par d�faut dans la page.
             */
            $this->f_Fam="inherit";
            /**
             * Attribut qui d�fini la taille du mode texte exprim�e en px. Dans la configuration par d�faut, c'est la taille de la f_Fame utilis�e par d�faut dans la page.
             */
            $this->size="inherit";
         }
      
      /**
       * M�thode qui r�initialise les diff�rents attributs pour un nouvel affichage du compteur en mode PHP
       */
      private function reset_Config()
         {
            /**
             * puis r�initialiser l'attribut $this->msg � la valeur "None" pour d'�ventuelles autres it�rations de la m�thode Get_LSD_Php5_Counter2_Result()
             */
            $this->create_Message(0);
            /**
             * et r�initialiser l'attribut $this->n_File � 0.
             */
            $this->new_File=0;
            /**
             * et r�initialiser l'attribut $this->n_Digit (la cha�ne des nombres) � la valeur null.
             */
            $this->n_Digit=null;
            /**
             * si l'attribut $this->ajax a la valeur true
             */
            if($this->ajax)
               {
                  /**
                   * alors passer la valeur de l'attribut $this->config_Root � l'attribut $this->over_Root
                   */
                  $this->over_Root=$this->config_Root;
               }
            /**
             * et r�initialiser la valeur de l'attribut $this->ajax � "false".
             */
            $this->ajax=false;
            /**
             * et r�initialiser la valeur de l'attribut $this->rough_Response � "false".
             */
            $this->rough_Response=false;
            /**
             * si l'attribut $this->changed_mode a la valeur "true".
             */
            if($this->changed_Mode)
               {
                  /**
                   * alors, passer le type d'affichage en mode graphique
                   */
                  $this->mode=$this->default_Mode;
                  /**
                   * puis passer la valeur de l'attribut $this->changed_Mode � la valeur false
                   */
                  $this->changed_Mode=false;
                  /**
                   * et r�initialiser la valeur de l'attribut $this->default_Mode � ""
                   */
                  $this->default_Mode="";
               }
            /**
             * si l'attribut $this->changed_Pad a la valeur "true".
             */
            if($this->changed_Pad)
               {
                  /**
                   * alors r�initialiser l'attribut $this->pad � sa valeur initiale.
                   */
                  $this->pad=$this->default_Pad;
                  /**
                   * puis r�initialiser l'attribut $this->changed_pad avec la valeur "false".
                   */
                  $this->changed_Pad=false;
                  /**
                   * et r�initialiser la valeur de l'attribut $this->default_Pad � ""
                   */
                  $this->default_Pad="";
               }
            /**
             * et r�initialiser l'attribut $this->changed_Config � la valeur "false".
             */
            $this->changed_Config=false;
            /**
             * et r�initialiser l'attribut $this->changed_Name � la valeur "false".
             */
            $this->changed_Name=false;
            /**
             * et r�initialiser l'attribut $this->changed_Id � la valeur "false".
             */
            $this->changed_Id=false;
            /**
             * et r�initialiser l'attribut $this->LSD_Php5_Counter2 (la cha�ne du compteur) � la valeur null.
             */
            $this->LSD_Php5_Counter2=null;
         }
      
      /**
       * M�thode qui teste les changements apport�s � la configuration par les diff�rentes commandes set_LSD_Php5_Counter2_XYZ()
       */
      private function test_Changes()
         {
            /**
             * si la variable $changed_Config a la valeur true
             */
            if($this->changed_Config===true)
               {
                  /**
                   * alors affecter la valeur de l'attribut $this->new_Config � l'attribut $this->Config_Name
                   */
                  $this->config_Name=$this->new_Config;
               }
            /**
             * si la variable de session a �t� chang�e
             */
            if($this->changed_Id===true)
               {
                  /**
                   * alors attribuer � l'attribut $this->session_Id la valeur de la variable $n_Id
                   */
                  $this->session_Id=$this->new_Id;
               }
            /**
             * si la variable $changed_Hidden a la valeur true
             */
            if($this->changed_Hidden===true)
               {
                  /**
                   * alors affecter la valeur de l'attribut $this->changed_Hidden � l'attribut $this->hidden
                   */
                  $this->hidden=$this->changed_Hidden;
               }
            /**
             * si la variable $changed_Mode a la valeur true
             */
            if($this->changed_Mode===true)
               {
                  /**
                   * alors affecter la valeur de l'attribut $this->new_Mode � l'attribut $this->Mode
                   */
                  $this->mode=$this->new_Mode;
               }
            /**
             * si la variable $changed_Pad a la valeur true
             */
            if($this->changed_Pad===true)
               {
                  /**
                   * alors affecter la valeur de l'attribut $this->new_Pad � l'attribut $this->Pad
                   */
                  $this->pad=$this->new_Pad;
               }
            /**
             * si la variable $changed_Name a la valeur true
             */
            if($this->changed_Name===true)
               {
                  /**
                   * alors affecter la valeur de l'attribut $this->new_Name � l'attribut $this->file_Name
                   */
                  $this->file_Name=$this->new_Name;
               }
         }
      
      /**
       * M�thode qui cr�e le compteur en appelant la m�thode de calcul des visite puis la m�thode du mode texte ou du mode graphique
       */
      private function create_Counter()
         {
            /**
             * Appeler la m�thode de calcul du nombre de visites
             */
            $this->compute_Hit();
            /**
             * Si l'attribut $this->read_Only est initialis�e et a la valeur "true" (pour le mode lecture),
             */
            if(isset($this->read_Only)&&$this->read_Only==true)
               {
                  /**
                   * alors, initialiser le mode d'affichage � visible.
                   */
                  $this->hidden=false;
               }
            /**
             * Si la valeur de l'attribut $this->hidden est � "false" (pour le mode compteur affich�),
             */
            if($this->hidden==false)
               {
                  /**
                   * alors, si la valeur de l'attribut $this->mode est G ou g (pour un compteuren mode graphique),
                   */
                  if(($this->mode=="G")||($this->mode=="g"))
                     {
                        /**
                         * alors, appeler la m�thode $this->draw_Counter2().
                         */
                        $this->draw_Counter();
                     }
                  /**
                   * sinon (donc le compteur est en mode texte),
                   */
                  elseif(($this->mode=="T")||($this->mode=="t"))
                     {
                        /**
                         * alors appeler la m�thode $this->write_Counter2().
                         */
                        $this->write_Counter();
                     }
                  /**
                   * sinon
                   */
                  else
                     {
                        /**
                         * cr�er le message suivant.
                         */
                        $this->create_Message(45);
                     }
               }
         }
      
      /**
       * M�thode qui lit le fichier des visites et calcule le nombre de visites
       */
      private function compute_Hit()
         {
            /**
             * Si le fichier des visites n'existe pas,
             */
            if(!file_exists($this->over_Root.$this->file_Path.$this->file_Name.'.'.$this->file_Ext))
               {
                  /**
                   * alors, si l'attribut $this->read_Only n'est pas initialis� ou a la valeur "false" (pour le mode �criture),
                   */
                  if((!isset($this->read_Only))||($this->read_Only==false))
                     {
                        /**
                         * alors, cr�er le fichier des visites.
                         */
                        $this->hit_File=fopen($this->over_Root.$this->file_Path.$this->file_Name.'.'.$this->file_Ext,"a");
                        
                        /**
                         * si l'attribut de d�part $this->init est initialis�e et est supp�rieur � 0,
                         */
                        if((isset($this->init))&&(strval($this->init)>0))
                           {
                              /**
                               * alors, le nombre de hits a la valeur de $this->init.
                               */
                              $this->n_Hit=strval($this->init);
                           }
                        /**
                         * sinon (donc si l'attribut $this->init n'est pas initialis� ou est n'est pas supp�rieur � 0),
                         */
                        else
                           {
                              /**
                               * alors, le nombre de hits est 0
                               */
                              $this->n_Hit=0;
                           }
                     }
                  /**
                   * sinon (donc le compteur est en mode lecture)
                   */
                  else
                     {
                        /**
                         * alors, le nombre de hits est 0
                         */
                        $this->n_Hit=0;
                     }
               }
            /**
             * sinon (donc le fichier des visites existe),
             */
            else
               {
                  /**
                   * alors, ouvrir le fichier des visites,
                   */
                  $this->hit_File=fopen($this->over_Root.$this->file_Path.$this->file_Name.'.'.$this->file_Ext,"r+");
                  
                  /**
                   * puis r�cup�rer le nombre de hits sur 15 chiffres (valeur maximale et raisonnable d'un nombre de visites!!!)
                   */
                  $this->n_Hit=fgets($this->hit_File,15);
               }
            /**
             * Si l'attribut $this->read_Only n'est pas initialis� ou a la valeur "false" (pour le mode ecriture),
             */
            if((!isset($this->read_Only))||($this->read_Only==false))
               {
                  /**
                   * alors, si l'attribut de triche $this->cheat est initialis�e et est supp�rieur � 0,
                   */
                  if((isset($this->cheat))&&(strval($this->cheat)>0))
                     {
                        /**
                         * alors, ajouter la valeur de triche $this->cheat � la valeur du nombre de hits strval($this->cheat).
                         */
                        $this->n_Hit=$this->n_Hit+strval($this->cheat);
                     }
                  /**
                   * et si l'attribut de session $_SESSION[$this->s_Id] n'est pas initialis�,
                   */
                  if(!isset($_SESSION[$this->session_Id]))
                     {
                        /**
                         * alors, cr�er l'attribut de session $_SESSION[$this->s_Id] avec la valeur de $this->s_Id,
                         */
                        $_SESSION[$this->session_Id]=$this->session_Id;
                        /**
                         * puis ajouter 1 au nombre de hits,
                         */
                        $this->n_Hit=$this->n_Hit+1;
                        /**
                         * puis se placer au d�but du fichier,
                         */
                        fseek($this->hit_File,0);
                        /**
                         * puis �crire le nouveau nombre de hits dans le fichier.
                         */
                        fwrite($this->hit_File,$this->n_Hit);
                     }
               }
            /**
             * Si le fichier des visites existe,
             */
            if(file_exists($this->hit_File))
               {
                  /**
                   * alors, fermer le fichier.
                   */
                  fclose($this->hit_File);
               }
            /**
             * Transformer la valeur de l'attribut du nombre de hits $this->n_Hit en une cha�ne de caract�res $this->n_Str.
             */
            $this->n_Str=strval($this->n_Hit);
            /**
             * Si la longueur de la cha�ne de caract�res $this->n_Str est inf�rieure � la valeur de l'attribut $this->pad,
             */
            if(strlen($this->n_Str)<strval($this->pad))
               {
                  /**
                   * alors, remplir la cha�ne de caract�res $this->n_Str par la gauche avec des 0.
                   */
                  $this->n_Str=STR_PAD($this->n_Hit,$this->pad,'0',STR_PAD_LEFT);
               }
         }
      
      /**
       * M�thode qui cr�e la cha�ne de caract�res HTML correspondant au compteur en mode graphique
       */
      private function draw_Counter()
         {
            /**
             * si l'attribut $this->ajax a la valeur "true",
             */
            if($this->ajax==true)
               {
                  /**
                   * alors passer la valeur de l'attribut $this->ajax_Root � l'attribut $this->over_Root
                   */
                  $this->over_Root=$this->ajax_Root;
               
               }
            /**
             * si la valeur de l'attribut $this->i_Border est � "true" (pour afficher les images de bordure),
             */
            if($this->i_Border==true)
               {
                  /**
                   * alors, d�finir l'image de gauche du compteur dans le d�but de la cha�ne de caract�res $this->LSD_Php5_Counter2.
                   */
                  $this->LSD_Php5_Counter2=self::bi.$this->over_Root.$this->i_Path.$this->i_Name.$this->i_Lft.".".$this->i_Ext.self::mi.self::h.$this->i_Lft_H.self::p.self::w.$this->i_Lft_W.self::p.self::ei;
               }
            /**
             * puis initialiser l'attribut du nombre de chiffres � afficher dans le compteur $this->n_Digit � vide (''),
             */
            $this->n_Digit="";
            /**
             * puis boucler sur le nombre de caract�res du compteur strlen($this->n_Str),
             */
            for($this->i=0;$this->i<strlen($this->n_Str);$this->i++)
               {
                  /**
                   * Concat�ner les cha�nes de caract�res des chiffres du compteur dans l'attribut $this->n_Digit
                   */
                  $this->n_Digit.=self::bi.$this->over_Root.$this->i_Path.$this->i_Name.substr($this->n_Str,$this->i,1).".".$this->i_Ext.self::mi.self::h.$this->i_H.self::p.self::w.$this->i_W.self::p.self::ei;
               }
            /**
             * puis concat�ner la cha�ne de caract�res de l'image du compteur $this->LSD_Php5_Counter2 avec l'attribut $this->n_Digit.
             */
            $this->LSD_Php5_Counter2.=$this->n_Digit;
            /**
             * puis si la valeur de l'attribut $this->i_Border est � "true" (pour afficher les images de bordure),
             */
            if($this->i_Border==true)
               {
                  /**
                   * alors, si le nombre de hits $this->n_Hit est inf�rieur � 2,
                   */
                  if($this->n_Hit<2)
                     {
                        /**
                         * alors, concat�ner la cha�ne de caract�res $this->LSD_Php5_Counter2 avec celle de l'attribut de l'image de droite n�1
                         */
                        $this->LSD_Php5_Counter2.=self::bi.$this->over_Root.$this->i_Path.$this->i_Name.$this->i_Rgt1.".".$this->i_Ext.self::mi.self::h.$this->i_Rgt1_H.self::p.self::w.$this->i_Rgt1_W.self::p.self::ei;
                     }
                  /**
                   * sinon (donc le nombre de hits $this->n_Hit est �gal ou supp�rieur � 2)
                   */
                  else
                     {
                        /**
                         * alors, concat�ner la cha�ne de caract�res $this->LSD_Php5_Counter2 avec celle de l'attribut de l'image de droite n�2
                         */
                        $this->LSD_Php5_Counter2.=self::bi.$this->over_Root.$this->i_Path.$this->i_Name.$this->i_Rgt2.".".$this->i_Ext.self::mi.self::h.$this->i_Rgt2_H.self::p.self::w.$this->i_Rgt2_W.self::p.self::ei;
                     }
               }
         }
      
      /**
       * M�thode qui cr�e la cha�ne de caract�res HTML correspondant au compteur en mode texte
       */
      private function write_Counter()
         {
            /**
             * Initialisation des attributs servant � �crire trois balises HTML diff�rentes du compteur en mode texte en fonction du contexte
             */
            if($this->color=="inherit")
               {
                  $this->c="color:";
               }
            else
               {
                  $this->c="color:#";
               }
            
            if($this->b_Color=="none")
               {
                  $this->b=" background:";
               }
            else
               {
                  $this->b=" background:#";
               }
            
            if($this->size=="inherit")
               {
                  $this->t=";";
               }
            else
               {
                  $this->t="px;";
               }
            /**
             * si l'attribut $this->pad est supp�rieur � 0
             */
            if($this->pad>0)
               {
                  /**
                   * alors, concat�ner la cha�ne de caract�res $this->LSD_Php5_Counter2 avec l'attribut $this->n_Str.
                   */
                  $this->LSD_Php5_Counter2=self::bt.$this->c.$this->color.self::s.$this->b.$this->b_Color.self::s.self::ff.$this->f_Fam.self::s.self::fs.$this->size.$this->t.self::mt.$this->n_Str.self::et;
               }
            /**
             * sinon (donc l'attribut $this->pad n'est pas supp�rieur 0)
             */
            else
               {
                  /**
                   * alors, si l'attribut $this->n_Hit � la valeur 0 (il n'y a pas encore eu de hit)
                   */
                  if($this->n_Hit==0)
                     {
                        /**
                         * alors, cr�er la cha�ne de caract�res $this->LSD_Php5_Counter2 et la concat�ner avec l'attribut $this->t_0.
                         */
                        $this->LSD_Php5_Counter2=self::bt.$this->c.$this->color.self::s.$this->b.$this->b_Color.self::s.self::ff.$this->f_Fam.self::s.self::fs.$this->size.$this->t.self::mt.$this->t_0.self::et;
                     }
                  /**
                   * sinon, si l'attribut $this->n_Hit � la valeur 1
                   */
                  elseif($this->n_Hit==1)
                     {
                        /**
                         * alors, cr�er la cha�ne de caract�res $this->LSD_Php5_Counter2 et la concat�ner avec l'attribut $this->t_1 et avec l'attribut $this->t_2.
                         */
                        $this->LSD_Php5_Counter2=self::bt.$this->c.$this->color.self::s.$this->b.$this->b_Color.self::s.self::ff.$this->f_Fam.self::s.self::fs.$this->size.$this->t.self::mt.$this->t_1.$this->n_Hit.$this->t_2.self::et;
                     }
                  /**
                   * sinon (donc l'attribut $this->n_Hit est supp�rieur � 1)
                   */
                  else
                     {
                        /**
                         * alors, cr�er la cha�ne de caract�res $this->LSD_Php5_Counter2 et la concat�ner avec l'attribut $this->t_1 et avec l'attribut $this->t_3.
                         */
                        $this->LSD_Php5_Counter2=self::bt.$this->c.$this->color.self::s.$this->b.$this->b_Color.self::s.self::ff.$this->f_Fam.self::s.self::fs.$this->size.$this->t.self::mt.$this->t_1.$this->n_Hit.$this->t_3.self::et;
                     }
               }
            /**
             * si le mode texte sans balise est activ�
             */
            if($this->rough_Response===true)
               {
                  /**
                   * alors, appeler la m�thode send_Rough()
                   */
                  $this->send_Rough();
               }
         }
      
      /**
       * M�thode qui supprime les balises HTML et qui convertit les accents et caract�res sp�ciaux
       */
      private function send_Rough()
         {
            /**
             * supprime les balises HTML
             */
            $this->LSD_Php5_Counter2=strip_tags($this->LSD_Php5_Counter2);
            /**
             * et convertit les accents et autres caract�res sp�ciaux.
             */
            $this->LSD_Php5_Counter2=html_entity_decode($this->LSD_Php5_Counter2,0,'UTF-8');
         }
      
      /**
       * M�thode qui renvoie soit le mot "None", soit un message d'erreur explicite.
       *
       * @return string-($msg)
       */
      public function get_LSD_Php5_Counter2_Error_Status()
         {
            /**
             * renvoie soit le mot "None", soit un message d'erreur.
             */
            return $this->msg;
         }
      
      /**
       * M�thode qui renvoie l'�tat du mode Ajax
       * @return char-(T/F)
       */
      public function get_LSD_Php5_Counter2_Ajax_Load()
         {
            /**
             * si le mode Ajax est activ�
             */
            if($this->ajax)
               {
                  /**
                   * alors, renvoie le chemin des images
                   */
                  return ($this->ajax_Root.$this->i_Path);
               }
            /**
             * sinon (donc le mode Ajax n'est pas activ�)
             */
            else
               {
                  /**
                   * alors, renvoie la lettre F
                   */
                  return ("F");
               }
         }
      
      /**
       * M�thode qui permet d'activer le mode de chargement Ajax, ce qui a pour effet de changer le chemin relatif des images.
       *
       * @param $ajax boolean
       * @param $ajax_Root string
       * @example set_LSD_Php5_Counter2_Ajax_Load(true,'../')
       */
      public function set_LSD_Php5_Counter2_Ajax_Load()
         {
            /**
             * si au moins un argument est pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Ajax_Mode(),
             */
            if(func_num_args()>0)
               {
                  /**
                   * alors, affecter le premier argument � la variable $ajax.
                   */
                  $ajax=func_get_arg(0);
                  /**
                   * si la variable $ajax est un bool�en,
                   */
                  if(is_bool($ajax))
                     {
                        /**
                         * alors, changer la valeur de l'attribut $this->ajax par la valeur pass�e en param�tre.
                         */
                        $this->ajax=$ajax;
                        /**
                         * si il y a un deuxi�me argument pass� en param�tre
                         */
                        if(func_num_args()>1)
                           {
                              /**
                               * alors sauvegarder la valeur de l'attribut $this->over_Root dans l'attribut $this->config_Root
                               */
                              $this->config_Root=$this->over_Root;
                              /**
                               * puis changer la valeur de l'attribut $this->ajax_Root par la valeur du second param�tre
                               */
                              $this->ajax_Root=func_get_arg(1);
                              /**
                               * si le chemin des images n'existe pas
                               */
                              if(!file_exists($this->ajax_Root.$this->i_Path))
                                 {
                                    /**
                                     * si le mode Ajax n'est pas activ�
                                     */
                                    if($this->ajax!=true)
                                       {
                                          /**
                                           * alors, cr�er le message suivant.
                                           */
                                          $this->create_Message(46);
                                       }
                                    /**
                                     * sinon (donc le mode Ajax est activ�)
                                     */
                                    else
                                       {
                                          /**
                                           * alors, cr�er le message suivant.
                                           */
                                          $this->create_Message(47);
                                       }
                                 }
                           }
                     }
               }
            /**
             * sinon, si la variable $ajax est vide ou nulle ou n'a pas �t� initialis�e
             */
            elseif(($ajax=="")||($ajax==null)||(empty($ajax)))
               {
                  /**
                   * si le mode Ajax n'est pas activ�
                   */
                  if($this->ajax!=true)
                     {
                        /**
                         * alors, cr�er le message suivant.
                         */
                        $this->create_Message(48);
                     }
                  /**
                   * sinon (donc le mode Ajax est activ�)
                   */
                  else
                     {
                        /**
                         * alors, cr�er le message suivant.
                         */
                        $this->create_Message(49);
                     }
                  /**
                   * puis, attribuer la valeur false � l'attribut $this->ajax.
                   */
                  $this->ajax=false;
                  /**
                   * et attribuer la valeur null � l'attribut $this->ajax_Root.
                   */
                  $this->ajax_Root=null;
               }
         }
      
      /**
       * M�thode qui renvoie le chemin et le nom complet du fichier de configuration
       * @return string-($over_Root.$config_Path.$config_Name.self::ce)
       */
      public function get_LSD_Php5_Counter2_Config_Name()
         {
            /**
             * si l'attribut $this->changed_Config a la valeur true
             */
            if($this->changed_Config)
               {
                  /**
                   * alors, renvoie le nouveau nom complet du fichier de configuration
                   */
                  return ($this->over_Root.$this->config_Path.$this->new_Config.self::ce);
               }
            /**
             * sinon (donc le nom du fichier de configuration n'a pas �t� chang�)
             */
            else
               {
                  /**
                   * alors, renvoie le nom du fichier de configuration actif.
                   */
                  return ($this->over_Root.$this->config_Path.$this->config_Name.self::ce);
               }
         }
      
      /**
       * M�thode qui permet de changer le nom du fichier de configuration (le chemin et l'extention �tant fixes).
       * @param $new_Config string
       */
      public function set_LSD_Php5_Counter2_Config_Name()
         {
            /**
             * si au moins un argument est pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Config_Name(),
             */
            if(func_num_args()>0)
               {
                  /**
                   * alors, affecter le premier argument � la variable $new_Config.
                   */
                  $new_Config=func_get_arg(0);
                  /**
                   * si la variable $new_Config est une cha�ne de caract�res non vide,
                   */
                  if((is_string($new_Config))&&($new_Config!=""))
                     {
                        /**
                         * alors si le fichier de configuration existe
                         */
                        if(file_exists($this->over_Root.$this->config_Path.$new_Config.self::ce))
                           {
                              /**
                               * alors, changer la valeur de l'attribut $this->c_Name par la valeur pass�e en param�tre.
                               */
                              $this->config_Name=$new_Config;
                              /**
                               * puis passer la valeur de l'attribut $this->changed_Config a true,
                               */
                              $this->new_Config=$new_Config;
                              /**
                               * puis passer la valeur de l'attribut $this->changed_Config a true,
                               */
                              $this->changed_Config=true;
                              /**
                               * et passer la valeur de l'attribut $this->msg � "None".
                               */
                              $this->create_Message(0);
                           }
                        /**
                         * sinon
                         */
                        else
                           {
                              /**
                               * si le mode Ajax n'est pas activ�
                               */
                              if($this->ajax!=true)
                                 {
                                    /**
                                     * alors, cr�er le message suivant.
                                     */
                                    $this->create_Message(50);
                                 }
                              /**
                               * sinon (donc le Pad Ajax est activ�)
                               */
                              else
                                 {
                                    /**
                                     * alors, cr�er le message suivant.
                                     */
                                    $this->create_Message(51);
                                 }
                              /**
                               * puis passer la valeur de l'attribut $this->changed_Config a false,
                               */
                              $this->changed_Config=false;
                           }
                     }
               }
         }
      
      /**
       * M�thode qui renvoie l'�tat du mode d'aide � la mise au point
       * @return char-(T/F)
       */
      public function get_LSD_Php5_Counter2_Debug_Mode()
         {
            /**
             * si le mode d'aide � la mise au point est activ�
             */
            if($this->debug)
               {
                  /**
                   * renvoie la lettre T
                   */
                  return ("T");
               }
            /**
             * sinon (donc la mode d'aide � la mise au point n'est pas activ�)
             */
            else
               {
                  /**
                   * renvoie la lettre F
                   */
                  return ("F");
               }
         }
      
      /**
       * M�thode qui permet de changer le mode d'aide � la mise au point.
       *
       * @param $debug boolean
       */
      public function set_LSD_Php5_Counter2_Debug_Mode()
         {
            /**
             * si au moins un argument est pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Debug_Mode(),
             */
            if(func_num_args()>0)
               {
                  /**
                   * alors, affecter le premier argument � la variable $debug.
                   */
                  $debug=func_get_arg(0);
                  /**
                   * si la variable $debug est un bool�en,
                   */
                  if(is_bool($debug))
                     {
                        /**
                         * alors, changer la valeur de l'attribut $this->debug par la valeur pass�e en param�tre.
                         */
                        $this->debug=$debug;
                     }
                  /**
                   * sinon, si la variable $debug n'est pas vide ou n'a pas �t� initialis�e
                   */
                  elseif(($debug=="")||($debug==null)||(empty($debug)))
                     {
                        /**
                         * si le mode Ajax n'est pas activ�
                         */
                        if($this->ajax!=true)
                           {
                              /**
                               * alors, cr�er le message suivant.
                               */
                              $this->create_Message(52);
                           }
                        /**
                         * sinon (donc le Pad Ajax est activ�)
                         */
                        else
                           {
                              /**
                               * alors, cr�er le message suivant.
                               */
                              $this->create_Message(53);
                           }
                     }
               }
         }
      
      /**
       * M�thode qui renvoie l'�tat d'affichage du compteur (mode cach�)
       * @return char-(T/F)
       */
      public function get_LSD_Php5_Counter2_Hidden_Result()
         {
            /**
             * si le mode cach� est activ�
             */
            if($this->changed_Hidden)
               {
                  /**
                   * alors, renvoie la lettre T
                   */
                  return ("T");
               }
            /**
             * sinon (donc le mode cach� n'est pas activ�)
             */
            else
               {
                  /**
                   * alors renvoie la lettre F
                   */
                  return ("F");
               }
         }
      
      /**
       * M�thode qui permet d'activer le mode cach�.
       *
       * @param $changed_Hidden boolean
       */
      public function set_LSD_Php5_Counter2_Hidden_Result()
         {
            /**
             * si au moins un argument est pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Hidden_Result(),
             */
            if(func_num_args()>0)
               {
                  /**
                   * alors, affecter le premier argument � la variable $hidden.
                   */
                  $changed_Hidden=func_get_arg(0);
                  /**
                   * si la variable $changed_Hidden est un bool�en,
                   */
                  if(is_bool($changed_Hidden))
                     {
                        /**
                         * alors, changer la valeur de l'attribut $this->hidden par la valeur pass�e en param�tre.
                         */
                        $this->changed_Hidden=$changed_Hidden;
                     }
                  /**
                   * sinon, si la variable $changed_Hidden est vide ou n'a pas �t� initialis�e
                   */
                  elseif(($changed_Hidden=="")||($changed_Hidden==null)||(empty($changed_Hidden)))
                     {
                        /**
                         * si le mode Ajax n'est pas activ�
                         */
                        if($this->ajax!=true)
                           {
                              /**
                               * alors, cr�er le message suivant.
                               */
                              $this->create_Message(54);
                           }
                        /**
                         * sinon (donc le mode Ajax est activ�)
                         */
                        else
                           {
                              /**
                               * alors, cr�er le message suivant.
                               */
                              $this->create_Message(55);
                           }
                        /**
                         * puis, attribuer la valeur false � l'attribut $this->changed_Hidden.
                         */
                        $this->changed_Hidden=false;
                     }
               }
         }
      
      /**
       * M�thode qui renvoie l'�tat du mode d'affichage sans balise HTML.
       * @return char-(T/F)
       */
      public function get_LSD_Php5_Counter2_Rough_Response()
         {
            /**
             * si le mode d'affichage sans balise HTML est activ�
             */
            if($this->rough_Response)
               {
                  /**
                   * alors, renvoie la lettre T
                   */
                  return ("T");
               }
            /**
             * sinon (donc le mode d'affichage sans balise HTML n'est pas activ�)
             */
            else
               {
                  /**
                   * alors, renvoie la lettre F
                   */
                  return ("F");
               }
         }
      
      /**
       * M�thode qui permet d'activer le mode d'affichage sans balise HTML.
       *
       * @param $rough_Response boolean
       */
      public function set_LSD_Php5_Counter2_Rough_Response()
         {
            /**
             * si au moins un argument est pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Rough_Response(),
             */
            if(func_num_args()>0)
               {
                  /**
                   * alors, affecter le premier argument � la variable $rough_Response.
                   */
                  $rough_Response=func_get_arg(0);
                  
                  /**
                   * si la variable $ajax_Raw est un bool�en,
                   */
                  if(is_bool($rough_Response))
                     {
                        /**
                         * alors, changer la valeur de l'attribut $this->rough_Response par la valeur pass�e en param�tre.
                         */
                        $this->rough_Response=$rough_Response;
                     }
                  /**
                   * sinon, si la variable $rough_Response est vide ou n'a pas �t� initialis�e
                   */
                  elseif(($rough_Response=="")||($rough_Response==null)||(empty($rough_Response)))
                     {
                        /**
                         * si le mode Ajax n'est pas activ�
                         */
                        if($this->ajax!=true)
                           {
                              /**
                               * alors, cr�er le message suivant.
                               */
                              $this->create_Message(56);
                           }
                        /**
                         * sinon (donc le Pad Ajax est activ�)
                         */
                        else
                           {
                              /**
                               * alors, cr�er le message suivant.
                               */
                              $this->create_Message(57);
                           }
                        /**
                         * puis, attribuer la valeur false � l'attribut $this->rough_Response.
                         */
                        $this->rough_Response=false;
                     }
               }
         }
      
      /**
       * M�thode qui renvoie l'�tat du mode lecture seule.
       * @return char-(T/F)
       */
      public function get_LSD_Php5_Counter2_Read_Only()
         {
            /**
             * si le mode lecture seule est activ�.
             */
            if($this->read_Only)
               {
                  /**
                   * alors, renvoie la lettre T
                   */
                  return ("T");
               }
            /**
             * sinon (donc le mode lecture seule n'est pas activ�)
             */
            else
               {
                  /**
                   * alors, renvoie la lettre F
                   */
                  return ("F");
               }
         }
      
      /**
       * M�thode qui permet de changer le mode lecture seule.
       *
       * @param $read_Only boolean
       */
      public function set_LSD_Php5_Counter2_Read_Only()
         {
            /**
             * si au moins un argument est pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Read_Only(),
             */
            if(func_num_args()>0)
               {
                  /**
                   * alors, affecter l'argument � la variable $read_Only.
                   */
                  $read_Only=func_get_arg(0);
                  
                  /**
                   * si la variable $read_Only est un bool�en,
                   */
                  if(is_bool($read_Only))
                     {
                        /**
                         * alors, changer la valeur de l'attribut $this->read_Only par la valeur pass�e en param�tre.
                         */
                        $this->read_Only=$read_Only;
                     }
                  /**
                   * sinon, si la variable $read_Only est vide ou n'a pas �t� initialis�e
                   */
                  elseif(($read_Only=="")||($read_Only==null)||(empty($read_Only)))
                     {
                        /**
                         * si le mode Ajax n'est pas activ�
                         */
                        if($this->ajax!=true)
                           {
                              /**
                               * alors, cr�er le message suivant.
                               */
                              $this->create_Message(58);
                           }
                        /**
                         * sinon (donc le Pad Ajax est activ�)
                         */
                        else
                           {
                              /**
                               * alors, cr�er le message suivant.
                               */
                              $this->create_Message(59);
                           }
                        /**
                         * alors, initialiser l'attribut $this->read_Only avec la valeur false.
                         */
                        $this->read_Only=false;
                     }
               }
         }
      
      /**
       * M�thode qui renvoie le nom de la variable de session
       * @return string-($session_Id)
       */
      public function get_LSD_Php5_Counter2_Session_Id()
         {
            /**
             * si la variable de session a �t� chang�e
             */
            if($this->changed_Id==true)
               {
                  /**
                   * renvoie la valeur de l'attribut $this->new_Id
                   */
                  return ($this->new_Id);
               }
            /**
             * sinon
             */
            else
               {
                  /**
                   * renvoie la valeur de l'attribut $this->session_Id.
                   */
                  return ($this->session_Id);
               }
         }
      
      /**
       * M�thode qui permet de changer le nom de la variable de session.
       * @param $n_Id string
       */
      public function set_LSD_Php5_Counter2_Session_Id()
         {
            /**
             * si au moins un argument est pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Session_Id(),
             */
            if(func_num_args()>0)
               {
                  /**
                   * alors, affecter l'argument � la variable $n_Id.
                   */
                  $new_Id=func_get_arg(0);
                  
                  /**
                   * si la variable $new_Id est une cha�ne de caract�res non vide,
                   */
                  if((!empty($new_Id))&&(is_string($new_Id))&&($new_Id!=""))
                     {
                        /**
                         * alors, passer la valeur de l'attribut $this->changed_Id � la valeur true.
                         */
                        $this->changed_Id=true;
                        /**
                         * et affecter la valeur de la variable $new_Id � l'attribut $this->new_Id.
                         */
                        $this->new_Id=$new_Id;
                     }
                  /**
                   * sinon (donc la variable $new_Id est vide ou n'a pas �t� initialis�e)
                   */
                  else
                     {
                        /**
                         * si le mode Ajax n'est pas activ�
                         */
                        if($this->ajax!=true)
                           {
                              /**
                               * alors, cr�er le message suivant.
                               */
                              $this->create_Message(60);
                           }
                        /**
                         * sinon (donc le Pad Ajax est activ�)
                         */
                        else
                           {
                              /**
                               * alors, cr�er le message suivant.
                               */
                              $this->create_Message(61);
                           }
                        /**
                         * alors, passer la valeur de l'attribut $this->changed_Id � false.
                         */
                        $this->changed_Id=false;
                     }
               }
         }
      
      /**
       * M�thode qui renvoie le mode d'affichage du compteur (texte ou graphique)
       * @return char-(T/G)
       */
      public function get_LSD_Php5_Counter2_Response_Mode()
         {
            /**
             * si l'attribut $this->changed_Mode a la valeur true
             */
            if($this->changed_Mode==true)
               {
                  /**
                   * alors, si l'attribut $this->mode est la lettre "t"
                   */
                  if(strtolower($this->new_Mode)==="t")
                     {
                        /**
                         * alors, renvoyer la lettre "T"
                         */
                        return ("T");
                     }
                  /**
                   * sinon
                   */
                  else
                     {
                        /**
                         * alors, renvoyer la lettre "G"
                         */
                        return ("G");
                     }
               }
            /**
             * sinon (donc le mode d'affichage n'a pas �t� chang�)
             */
            else
               {
                  /**
                   * alors, renvoyer la valeur de l'attribut $this->mode.
                   */
                  return ($this->mode);
               }
         }
      
      /**
       * M�thode qui permet de changer le mode d'affichage du compteur.
       * @param $changed_Mode char
       */
      public function set_LSD_Php5_Counter2_Response_Mode()
         {
            /**
             * si au moins un argument est pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Response_Mode(),
             */
            if(func_num_args()>0)
               {
                  /**
                   * alors, affecter l'argument � la variable $changed_Mode.
                   */
                  $changed_Mode=func_get_arg(0);
                  /**
                   * si la variable $changed_Mode est une cha�ne de caract�res non vide,
                   */
                  if((!empty($changed_Mode))&&(is_string($changed_Mode))&&($changed_Mode!="")&&((strtolower($changed_Mode)==="t")||(strtolower($changed_Mode)==="g")))
                     {
                        /**
                         * alors, passer la valeur de l'attribut $this->changed_Mode � la valeur true.
                         */
                        $this->changed_Mode=true;
                        /**
                         * et affecter la valeur de l'attribut $this->mode � l'attribut $this->default_Mode
                         */
                        $this->default_Mode=$this->mode;
                        /**
                         * puis affecter la valeur de la variable $new_Mode � l'attribut $this->new_Mode.
                         */
                        $this->new_Mode=$changed_Mode;
                     }
                  /**
                   * sinon (la variable $new_Mode est vide ou n'a pas �t� initialis�e)
                   */
                  else
                     {
                        /**
                         * si le mode Ajax n'est pas activ�
                         */
                        if($this->ajax!=true)
                           {
                              /**
                               * alors, cr�er le message suivant.
                               */
                              $this->create_Message(62);
                           }
                        /**
                         * sinon (donc le mode Ajax est activ�)
                         */
                        else
                           {
                              /**
                               * alors, cr�er le message suivant.
                               */
                              $this->create_Message(63);
                           }
                        /**
                         * puis, passer la valeur de l'attribut $this->changed_Mode � false.
                         */
                        $this->changed_Mode=false;
                     }
               }
         }
      
      /**
       * M�thode qui renvoie le nombre de digit du compteur (texte ou graphique)
       * @return string-($pad)
       */
      public function get_LSD_Php5_Counter2_Pad_Number()
         {
            /**
             * si l'attribut $this->changed_Pad a la valeur true
             */
            if($this->changed_Pad==true)
               {
                  /**
                   * alors, renvoyer la valeur de l'attribut $this->new_Pad
                   */
                  return $this->new_Pad;
               }
            /**
             * sinon (donc le nombre de digit du compteur n'a pas �t� chang�)
             */
            else
               {
                  /**
                   * alors, renvoyer la valeur de l'attribut $this->pad.
                   */
                  return ($this->pad);
               }
         }
      
      /**
       * M�thode qui permet de changer le nombre de chiffres du compteur.
       * @param $changed_Pad char
       */
      public function set_LSD_Php5_Counter2_Pad_Number()
         {
            /**
             * si au moins un argument est pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Pad_Number(),
             */
            if(func_num_args()>0)
               {
                  /**
                   * alors, affecter l'argument � la variable $changed_Pad.
                   */
                  $changed_Pad=func_get_arg(0);
                  /**
                   * si la variable $changed_Pad est une cha�ne de caract�res non vide,
                   */
                  if((isset($changed_Pad))&&(is_string($changed_Pad))&&($changed_Pad!=""))
                     {
                        /**
                         * alors, passer la valeur de l'attribut $this->changed_Pad � la valeur true.
                         */
                        $this->changed_Pad=true;
                        /**
                         * et affecter la valeur de l'attribut $this->pad � l'attribut $this->default_Pad
                         */
                        $this->default_Pad=$this->pad;
                        /**
                         * puis affecter la valeur de la variable $new_Pad � l'attribut $this->new_Pad.
                         */
                        $this->new_Pad=strval($changed_Pad);
                     }
                  /**
                   * sinon (la variable $new_Pad est vide ou n'a pas �t� initialis�e)
                   */
                  else
                     {
                        /**
                         * si le mode Ajax n'est pas activ�
                         */
                        if($this->ajax!=true)
                           {
                              /**
                               * alors, cr�er le message suivant.
                               */
                              $this->create_Message(64);
                           }
                        /**
                         * sinon (donc le mode Ajax est activ�)
                         */
                        else
                           {
                              /**
                               * alors, cr�er le message suivant.
                               */
                              $this->create_Message(65);
                           }
                        /**
                         * alors, passer la valeur de l'attribut $this->changed_Pad � false.
                         */
                        $this->changed_Pad=false;
                     }
               }
         }
      
      /**
       * M�thode qui renvoie le nom du fichier des visites.
       * @return string-($file_Name)
       */
      public function get_LSD_Php5_Counter2_Hit_Name()
         {
            /**
             * si l'attribut $this->changed_Name a la valeur true
             */
            if($this->changed_Name)
               {
                  /**
                   * alors, renvoyer la valeur de l''attribut $this->new_Name
                   */
                  return ($this->new_Name);
               }
            /**
             * sinon (donc le nom du fichier des visites n'a pas �t� chang�)
             */
            else
               {
                  /**
                   * alors, renvoyer la valeur de l'attribut $this->file_Name
                   */
                  return ($this->file_Name);
               }
         }
      
      /**
       * M�thode qui permet de changer le nom du fichier des visites
       *
       * @param $changed_Name string
       */
      public function set_LSD_Php5_Counter2_Hit_Name()
         {
            /**
             * si au moins un argument est pass� en param�tre � la m�thode set_LSD_Php5_Counter2_Hit_Name(),
             */
            if(func_num_args()>0)
               {
                  /**
                   * alors, affecter le premier argument � la variable $changed_Name.
                   */
                  $changed_Name=func_get_arg(0);
                  /**
                   * si la variable $changed_Name est une cha�ne de caract�res,
                   */
                  if(is_string($changed_Name))
                     {
                        /**
                         * alors si le fichier des visites existe
                         */
                        if(file_exists($this->over_Root.$this->file_Path.$changed_Name.'.'.$this->file_Ext))
                           {
                              /**
                               * alors, changer la valeur de l'attribut $this->file_Name par la valeur pass�e en param�tre.
                               */
                              $this->new_Name=$changed_Name;
                              /**
                               * et passer la valeur de l'attribut $this->changed_Name � true
                               */
                              $this->changed_Name=true;
                           }
                        /**
                         * sinon (donc le nouveau fichier des visites est n'est pas trouv�)
                         */
                        else
                           {
                              /**
                               * alors, cr�er le message suivant
                               */
                              $this->create_Message(66);
                           }
                     }
                  /**
                   * sinon, si la variable $changed_Name est vide ou n'a pas �t� initialis�e
                   */
                  elseif(($changed_Name=="")||($changed_Name==null)||(empty($changed_Name)))
                     {
                        /**
                         * si le mode Ajax n'est pas activ�
                         */
                        if($this->ajax!=true)
                           {
                              /**
                               * alors, cr�er le message suivant.
                               */
                              $this->create_Message(67);
                           }
                        /**
                         * sinon (donc le mode Ajax est activ�)
                         */
                        else
                           {
                              /**
                               * alors, cr�er le message suivant.
                               */
                              $this->create_Message(68);
                           }
                        /**
                         * puis, attribuer la valeur false � l'attribut $this->changed_Name.
                         */
                        $this->changed_Name=false;
                     }
               }
         }
      
      /**
       * M�thode qui affiche l'objet compteur
       * @return printf("%s",$this->LSD_Php5_Counter2) string
       */
      public function get_LSD_Php5_Counter2_Hit_Result()
         {
            /**
             * Si l'attribut $this->msg a la valeur "None" (donc il n'y a pas d'erreur avant l'appel du script)
             */
            if($this->msg=="None")
               {
                  /**
                   * si le fichier de configuration a �t� chang�
                   */
                  if($this->changed_Config)
                     {
                        /**
                         * alors, charger le nouveau fichier de configuration.
                         */
                        require ($this->over_Root.$this->config_Path.$this->config_Name.self::ce);
                     }
                  /**
                   * puis v�rifier les param�tres du fichier de configuration,
                   */
                  $this->test_Config();
                  /**
                   * Si l'attribut $this->msg a toujours la valeur "None" apr�s la v�rification des param�tres du fichier de configuration,
                   */
                  if($this->msg=="None")
                     {
                        /**
                         * puis tester les changements par rapport au fichier de configuration
                         */
                        $this->test_Changes();
                        /**
                         * puis, cr�er le compteur.
                         */
                        $this->create_Counter();
                     }
               }
            /**
             * Si l'attribut $this->msg n'a plus la valeur "None" (donc il y a une d'erreur apr�s la lecture du fichier de configuration)
             */
            if($this->msg!="None")
               {
                  /**
                   * alors, si l'attribut $this->debug a la valeur "true" (le mode mise au point est activ�)
                   */
                  if($this->debug==true)
                     {
                        /**
                         * alors affecter � l'attribut $this->couter la valeur de l'attribut $this->msg (le compteur sera alors le message d'erreur),
                         */
                        $this->LSD_Php5_Counter2=$this->msg;
                     }
                  /**
                   * sinon (donc le mode mise au point n'est pas activ�)
                   */
                  else
                     {
                        /**
                         * alors utiliser la m�thode create_Config() qui cr�e les param�tres d'une configuration texte minimale par d�faut.
                         */
                        $this->create_Config();
                        /**
                         * puis utiliser la m�thode create_Counter2()
                         */
                        $this->create_Counter();
                     }
               }
            /**
             * si l'attribut $this->msg n'a plus la valeur "None" et que l'attibut $this->debug a la valeur "true" (mode mise au point activ�)
             */
            if(($this->msg!="None")&&($this->debug==true))
               {
                  /**
                   * alors le compteur est le message d'erreur.
                   */
                  $this->LSD_Php5_Counter2=$this->msg;
                  /**
                   * si l'attribut $this->rough_Response a la valeur true
                   */
                  if($this->rough_Response==true)
                     {
                        /**
                         * alors, appeler la m�thode $this->send_Rough()
                         */
                        $this->send_Rough();
                     }
               }
            /**
             * affiche l'attribut $this->LSD_Php5_Counter2.
             */
            printf("%s",$this->LSD_Php5_Counter2);
            /**
             * puis r�initialiser les attributs n�cessaires � d'�ventuelles autres it�rations de la m�thode Get_LSD_Php5_Counter2_Result()
             */
            $this->reset_Config();
         }
   }
?>
