<style type="text/css">
<!--
.titre9{
	text-decoration: underline;
	font-size:14px;
	font-weight:bold;
	}
-->
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<body>
<div class="filazana2" align="left">
<table width="900" border="0" style="font-size:14px">
  <tr>
    <td>&nbsp;</td>
    <td><p align="center">&nbsp;</p>
      <p>Amin'ny finoana sy ny fanantenana ny fitsanganan'ny tena amin'ny maty no ampandrenesanay fa ny zanaka malalanay</p>
      <table border="0" cellspacing="0" cellpadding="0" width="652">
        <tr>
          <td width="461" valign="top"><p align="center"><strong>&nbsp;</strong></p>
              <p align="center"><strong>Herisoa    Thierry ANDRIAMIAKATRA</strong><br>
                  <strong>Professeur de Math&eacute;matiques    au lyc&eacute;e Sophie Lumina, </strong><br>
                  <strong>Saint-Laurent du Maroni en    Guyane fran&ccedil;aise, </strong><br>
                  <strong>&nbsp;Am&eacute;rique du Sud.</strong></p></td>
          <td width="191" valign="top"><p align="center">&nbsp;</p>
              <p align="center"><img src="tetiarana22_clip_image002.gif" alt="" width="64" height="92"> </p></td>
        </tr>
      </table>
      <p>Dia nantsoin'ny Ray hody any Aminy tamin'ny 13 Oktobra 2013 tany Guyane fran&ccedil;aise, teo amin'ny faha-36 taonany.</p><p>&nbsp;</p>
      <p>Hoy&nbsp;: </p>
      <p><strong>Ny  reniny&nbsp;:</strong> ANDRIAMIAKATRA RAVOLOLONIRINA Suzanne (Bras Panon, La R&eacute;union).</p><p>&nbsp;</p>
      <p><strong>Ny iray  tampo aminy&nbsp;:</strong></p><p>&nbsp;</p>
      <ul>
        
<li>ANDRIAMIAKATRA Fidisoa Alain sy Nathalie ary ny zanany  Cl&eacute;ia, Elise, Maxence, Th&eacute;o (Bras Panon, La R&eacute;union).</li>
<li>Dr ANDRIAMIAKATRA Serge sy Lanto ary ny zanany  Fabien, Micka&euml;l, Tina (Toulouse).</li>
<li>ANDRIAMIAKATRA Guy Roland sy Corinne ary ny zanany  D&eacute;borah, Charles, Boris (Saint-Denis, La R&eacute;union).
</li><li>LESAGE Florent sy Dominique ary ny zanany Katia (Saint-Denis, La R&eacute;union).
RAMAROSON Paul sy ny zanany Christian (Saint-Denis, La R&eacute;union).</li>

        
      </ul><p>&nbsp;</p>
      <p><strong>Ny iray tampo amin&rsquo;ny  rainy&nbsp;: </strong> </p>
        
        
        <ul>
<li>RAKOTONDRAZAKA  ANDRIAMIAKATRA Samuel mivady sy ny zanaka aman-jafiny.</li>
<li>RANAIVOSON Lucie Berthe sy ny zanaka aman-jafiny.</li>
<li>Terak'Itpvavy RATSIORY Julie C&eacute;line.</li>
<li>Terak'Itpklahy RAFIDIMANANA Callixte sy Itpkvavy RAZAFIARISOA Victorine Ang&eacute;line.</li>
<li>Terak'Itpklahy RAZAFINDRAKOTO Th&eacute;ophile sy Itpvavy RAZAFINDRAFARA Christine Agn&egrave;s.</li>
<li>RAKOTONIAINA  Monique Yvonne sy ny zanaka aman-jafiny. </li>
<li>RAZAFIMANDIMBY Volasoa L&eacute;a sy ny zanaka aman-jafiny.</li>

        </ul>
        
        
        <p>&nbsp;</p>
        
      <p><strong>Ny iray  tampo amin&rsquo;ny reniny :</strong></p>
        
<ul>
<li>Vady navelan'Itpklahy RAMAROKOTO Emile Roger sy ny zanaka aman-jafiny (Montpellier).</li>
<li>RASOLONIAINA Rakotonizao Marcel sy Margot ary ny zanaka aman-jafiny.</li>
<li>RAKOTOMALALA C&eacute;lestin et Mavo et ny zanaka aman-jafiny</li>
<li>RAKOTOMAHARO Victor Emile sy Lucie ary ny zanaka aman-jafiny.</li>
<li>RAMAROKOTO Norosoa Lalao.</li>
<li>RAMAROKOTO Andriamihaja Emile sy Lalatiana Arlette ary ny zanany.</li>
<li>MILSON C&eacute;line sy ny zanaka aman-jafiny.</li>
<li>ANDRIAMANDROSO Adolphe sy Faratiana ary ny zanany.	</li>
</ul>        

        <p>&nbsp;</p>
        
      <p><strong>Ny  Havany :</strong></p>
        
<ul>
<li>Taranak'Itpklahy RAZAFINDRAZAKA  (Mpamoron-kira : Esory re ry Raiko).</li>
<li>Taranak'Itpklahy RAMIAKATRA sy Itpkvavy RAZAFINDRAFARA.</li>
<li>Taranak'Itpklahy RAZAKA Justin sy RAZANADRAIBE.</li>
<li>Terak'Itpklahy RAKOTOMAVO ANDRIAMIAKATRA Ignace sy Itpkvavy RATSARAIBE Justine.</li>

</ul>   <p>&nbsp;</p>

<ul>
<li>Ny zanaka ampielezana Falihavana Ampangabe Ambohitrimanjaka.</li>
<li>Terak'Itpklahy RAZANAKOTO Bernard.</li>
<li>Terak'Itpklahy RAINITSIMBA sy Itpkvavy RAMIADANA.</li>
<li>Ny fianakaviana manontolo.</li>
</ul>   <p>&nbsp;</p>  

<ul>
<li>Ny R&eacute;v&eacute;rend P&egrave;re sy ny Paroasy Trinit&eacute; Masina Mangasoavina.</li>
<li>Ny Fiangonana Notre Dame de Lourdes Ampangabe Ambohitrimanjaka.</li>
<li>Ny Mpitandrina sy ny Fiangonana Tranovato FJKM Anjanahary Maritiora.</li>
<li>Ny Mpitandrina sy ny Fiangonana FJKM Ambohipanja Fitiavana.</li>
</ul>    <p>&nbsp;</p>                   
        
<p>
	Ny razana dia efa nalevina ny Alahady 27 Oktobra lasa teo, tao amin'ny Cimeti&egrave;re de Bras-Panon, La R&eacute;union.
</p>     <p>&nbsp;</p> 
      <p>
		Ny fanompoam-pivavahana hanomezam-boninahitra an'Andriamanitra sy ho fialantsasatrin'ny fanahiny dia ho tanterahina ao amin'ny Paroasy Trinit&eacute; Masina MANGASOAVINA,ny Asabotsy 2 novambra 2013, amin'ny 10 ora maraina. <br/>
Ny fianakaviana dia hivory aorian'ny fotoam-bavaka ao Androhibe Lot II M 35 Bis (pr&egrave;s Soeurs Religieuses Trinitaires de Valence-Noviciat) ny tontolo andro.
</p><p>&nbsp;</p>
      <p>
	"Ankehitriny, ry Tompo, vao alefanao am-piadanana araka ny teninao ny mpanomponao,
fa ny masoko nahita ny famonjena avy aminao," (Lioka 2: 29 - 30).
	
     </p><p>&nbsp;</p>
     <p>
     	<strong>ITY NO SOLON'NY FAMPANDRENESANA</strong>
     </p>
      </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
	<td bgcolor="#784f3b"><a href="page21.php">Pejy teo aloha</a></td>    
	<td>&nbsp;</td>
    <td>&nbsp;</td>
	<td bgcolor="#784f3b"><a href="page23.php">Pejy manaraka</a></td>    
    <td>&nbsp;</td>
  </tr>
</table>
</div>


</body>
